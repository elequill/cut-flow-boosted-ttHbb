#define cutFlow_cxx
#include "cutFlow.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TChain.h>

using namespace std;

void cutFlow::Loop(TString prod, TString sample, TString treeName,
                 bool choose2016, bool choose2015, bool chooseElchannel, bool chooseMuchannel,
                 bool chooseBoosted, bool chooseResolved,
                 bool saveHistos, bool saveBoostedCutflow, bool saveResolvedCutflow,
                 bool saveTreeForTMVA, bool saveTreeForFit
      ) 
{
   // set 'global' variables
   fProd=prod;
   fTreeName = treeName;
   fsaveBoostedCutflow = saveBoostedCutflow and saveHistos;
   fsaveResolvedCutflow = saveResolvedCutflow and saveHistos;

   // load the trees
   TChain* myChain = new TChain(fTreeName); // ex : "nominal_Loose"
   myChain->Add("inputNtuples/"+prod+"/"+sample+"/*.root*");
   fProcess=sample.ReplaceAll("/", "_");
   cout <<fProcess <<  " : total # of events : " << myChain->GetEntries() << endl;
   ReadInputTreeLeaf(myChain);

   // load the ntuples normalisations (1st bin 
   mcProcessNormalisation( fProd );

   if (fTreeName == "nominal_Loose" and saveHistos) InitHisto();
   if (fTreeName == "nominal_Loose" and saveTreeForTMVA) InitTreeForTMVA();
   if (saveTreeForFit)  InitTreeForFit();


   //ofstream maliste("resolvedWeight_"+ fProcess+ ".txt");

   if (fChain == 0) return;

   Long64_t nentries = fChain->GetEntriesFast();



   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry< nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      if (jentry%10000 == 0) cout << fProcess << " : entry # : " << jentry << endl;
      
      // overlapp b-filtered sample and non all-had sample : remove tt+b componenent from non all-had ttbar (410000)
      if (fttbarb and mcChannelNumber == 410000 and TopHeavyFlavorFilterFlag == 5) continue;
      // note: the sample ttx_sarah contains all the inclusive ttbar and the tt+b filtered events
      if (sample == "ttb_sarah" and HF_SimpleClassification != 1 ) continue;
      if (sample == "ttc_sarah" and HF_SimpleClassification != -1 ) continue;
      if (sample == "ttlight_sarah" and HF_SimpleClassification != 0 ) continue;

      // _____ initilize the parameters _____
      isBoosted = false; isResolved = false;
      nJ= 0; nJPt250= 0; nloosetJ= 0; nloosetJPt250_woOR= 0; ntighttJ= 0; nj = 0; nb70j = 0; nb77j = 0; nb85j = 0;
      njOusidettags3211 = 0; nb70Outsidettags3211 = 0;
      njOusidettagsTight3211 = 0; nb70OutsidettagsTight3211 = 0;
      njOutOfFirtPtLoosetJ = 0; nb70jOutOfFirstPtLoosetJ = 0;
      nel = 0; nmu=0;
      sumPseudoMv2c10_b70 = 0; sumPseudoMv2c10_b77 = 0; sumPseudoMv2c10_b85 = 0;
      evtwgtb70 = 1.; evtwgtb77 = 1.; evtwgtb85 = 1.;  evtwgtbContinuous = 1.;evtwgtForCutflow = 1.;
      muchannel = false; elchannel = false;
      is2015 = false; is2016 = false;
      is3211 = false; 
      Pass_4JE2BE_tr = 0;
      Pass_4JE3BE_tr = 0;
      Pass_4JE4BI_tr = 0;
      Pass_5JE2BE_tr = 0;
      Pass_5JE3BE_tr = 0;
      Pass_5JE4BI_tr = 0;
      Pass_6JI2BE_tr = 0;
      Pass_6JI3BE_tr = 0;
      Pass_6JI4BI_tr = 0;
      lumiDatasetMinus1 = 0.;

      //  _____ assign weights to events _____
      if ( mcChannelNumber != 0 ) { // if MC, set the weights. Otherwise, do not change their initial value (1)

         if ( fmcCrossSection[mcChannelNumber] == 0 ) {
            cout << "Null cross section, you should check that" << endl;
            return;
         }

         if (fFirstBinCutflow) {
            lumiDatasetMinus1 =  fmcCrossSection[mcChannelNumber] * 1000. / fmcFirstBinCutFlow[mcChannelNumber]; // in fb-1
 
            double ratio_lumi_data_mc = fDataLumi * lumiDatasetMinus1; // MC file Lumi reweighting to data lumi
            double weight_mc_pu_jvt_lepSF = weight_mc  * weight_pileup * weight_jvt * weight_leptonSF;
            double ratio_lumi_data_mc_weight_mc_pu_jvt_lepSF = ratio_lumi_data_mc * weight_mc_pu_jvt_lepSF;

            evtwgtb70         = ratio_lumi_data_mc_weight_mc_pu_jvt_lepSF * weight_bTagSF_70;
            evtwgtb77         = ratio_lumi_data_mc_weight_mc_pu_jvt_lepSF * weight_bTagSF_77;
            evtwgtb85         = ratio_lumi_data_mc_weight_mc_pu_jvt_lepSF * weight_bTagSF_85;
            evtwgtbContinuous = ratio_lumi_data_mc_weight_mc_pu_jvt_lepSF * weight_bTagSF_Continuous;


            if ( fProd.Contains("0204230") ) {
               evtwgtb70 *= weight_ttbb_Nominal;
               evtwgtb77 *= weight_ttbb_Nominal;
               evtwgtb85 *= weight_ttbb_Nominal;
               evtwgtbContinuous *= weight_ttbb_Nominal;
            }

            evtwgtForCutflow *= evtwgtb70 ;
         } else {

            lumiDatasetMinus1 =   fmcCrossSection[mcChannelNumber] * 1000. / fmcSumWeightTree[mcChannelNumber]; // in fb-1
            evtwgtb70 *= fDataLumi * lumiDatasetMinus1 ; // MC file Lumi reweighting to data lumi
            evtwgtb77 *= fDataLumi * lumiDatasetMinus1 ; 
            evtwgtb85 *= fDataLumi * lumiDatasetMinus1 ; 
            if (mcChannelNumber != 410120) {
               evtwgtb70 *= weight_mc  * weight_pileup * weight_jvt * weight_leptonSF * weight_bTagSF_70;
               evtwgtb77 *= weight_mc  * weight_pileup * weight_jvt * weight_leptonSF * weight_bTagSF_77;
               evtwgtb85 *= weight_mc  * weight_pileup * weight_jvt * weight_leptonSF * weight_bTagSF_85;
            } else {
               evtwgtb70 *=  weight_pileup * weight_jvt * weight_leptonSF * weight_bTagSF_70;
               evtwgtb77 *=  weight_pileup * weight_jvt * weight_leptonSF * weight_bTagSF_77;
               evtwgtb85 *=  weight_pileup * weight_jvt * weight_leptonSF * weight_bTagSF_85;
            }

            evtwgtForCutflow *= evtwgtb70 ;
         }
      }

      // __ CUT boosted pre-selection __ //
      elchannel =  boosted_ejets_2016 == 1 or boosted_ejets_2015 == 1
                        or ejets_2016 == 1 or ejets_2015 == 1;

      muchannel = boosted_mujets_2016 == 1 or boosted_mujets_2015 == 1
                       or mujets_2016 == 1 or mujets_2015 == 1;

      is2016 = boosted_ejets_2016 == 1 or boosted_mujets_2016 == 1 
                    or ejets_2016 == 1 or mujets_2016 == 1;
      is2015 = boosted_ejets_2015 == 1 or boosted_mujets_2015 == 1
                    or ejets_2015 == 1 or mujets_2015 == 1;

      isBoosted = boosted_ejets_2016 == 1 or boosted_ejets_2015 == 1
              or boosted_mujets_2016 == 1 or boosted_mujets_2015 == 1;

      isResolved = ejets_2016 == 1 or ejets_2015 == 1
               or mujets_2016 == 1 or mujets_2015 == 1;

      // lepton flavour cut 
      if ( not( (elchannel and chooseElchannel) or (muchannel and chooseMuchannel) ) ) continue;

      // year cut 
      if ( not( ( is2015 and choose2015 ) or ( is2016 and choose2016 ) ) ) continue;

      // resolved boosted cut (not boosted and not resolved -> I don't give a shit)
      if ( not( ( isBoosted and chooseBoosted ) or ( isResolved and chooseResolved ) ) ) continue;

      //lepton(0);

      int nelPt25(0);
      // _____ el loop _____
      for ( size_t iel = 0; iel < el_pt->size(); iel++) {
         if ( el_pt->at(iel) / 1e3 < ptmin_lep ) continue;
         nelPt25++;
         lepton = new Lepton(new TLorentzVector());
         lepton->v4()->SetPtEtaPhiE(el_pt->at(iel)/1e3, el_eta->at(iel), el_phi->at(iel), el_e->at(iel)/1e3);
      } // for iel

      int nmuPt25(0);
      // _____ mu loop _____
      for ( size_t imu = 0; imu < mu_pt->size(); imu++) {
         if ( mu_pt->at(imu) / 1e3 < ptmin_lep ) continue;
         nmuPt25++;
         lepton = new Lepton(new TLorentzVector());
         lepton->v4()->SetPtEtaPhiE(mu_pt->at(imu)/1e3, mu_eta->at(imu), mu_phi->at(imu), mu_e->at(imu)/1e3);
      } // for imu

      // __ CHECK : exactly 1 lepton __ //
      if ( elchannel and muchannel) 
    { cout << "Error with lepton definition" << endl; cout << "el : " << nelPt25 << ", mu : " << nmuPt25 << endl; }


      // _____ large-R jet loop _____
      for ( size_t iJ = 0; iJ < ljet_pt->size(); iJ++) {
         // _____ large-R jet kinematics selection _____
         if ( ljet_pt->at(iJ) / 1e3 < ptmin_J or ljet_pt->at(iJ) / 1e3 > ptmax_J 
            or ljet_m->at(iJ) / 1e3 < massmin_J or abs(ljet_eta->at(iJ)) > etamax_J )  continue; 

         nJ++;

         TLorentzVector* tmpLV = new TLorentzVector();
         tmpLV->SetPtEtaPhiE(ljet_pt->at(iJ)/1e3, ljet_eta->at(iJ), ljet_phi->at(iJ), ljet_e->at(iJ)/1e3);
         double deltaRToLepton = tmpLV->DeltaR(*(lepton->v4()));

         // nb for 3211 cutflow
         if ( ljet_pt->at(iJ) /1e3 > ptmin_ttag_3211 )
         {
            nJPt250++; // count nb of large-R jets with pT > 250 GeV
            if (ljet_topTag_loose->at(iJ)) nloosetJPt250_woOR++; // top-tag loose (no overlap removal with electron)
         }

         // _____ skip large-R jets that contain an electron _____
         if (elchannel and deltaRToLepton < radius_J) 
         {
            delete tmpLV;
            continue;   // fat jet electron overlap removal
         }

         // nb for 3211 cutflow
         if ( ljet_pt->at(iJ) / 1e3 > ptmin_ttag_3211 )
         {
            if (ljet_topTag_loose->at(iJ)) nloosetJ++;
            if (ljet_topTag->at(iJ)) ntighttJ++;
         }

         FatJet* tmpfat = new FatJet(tmpLV);
         tmpfat->set_lepton        (lepton);
         float mone = -1.;
         // __ set the default values of substructure variables to -1 __
         tmpfat->set_sd12          (ljet_sd12->at(iJ) > 0. ? ljet_sd12->at(iJ) : mone);
         tmpfat->set_sd23          (ljet_sd23->at(iJ) > 0 ? ljet_sd23->at(iJ) : mone);
         tmpfat->set_tau21         (ljet_tau21->at(iJ) > 0 ? ljet_tau21->at(iJ) : mone);
         tmpfat->set_tau32         (ljet_tau32->at(iJ) > 0 ? ljet_tau32->at(iJ) : mone);
         tmpfat->set_tau21_wta     (ljet_tau21_wta->at(iJ) > 0 ? ljet_tau21_wta->at(iJ) : mone);
         tmpfat->set_tau32_wta     (ljet_tau32_wta->at(iJ) > 0 ? ljet_tau32_wta->at(iJ) : mone);
         tmpfat->set_D2            (ljet_D2->at(iJ) > 0 ? ljet_D2->at(iJ) : mone);
         tmpfat->set_C2            (ljet_C2->at(iJ) > 0 ? ljet_C2->at(iJ) : mone);
         tmpfat->set_topTag        (ljet_topTag->at(iJ));
         if ( !fProd.Contains("0204230") ) tmpfat->set_bosonTag      (ljet_bosonTag->at(iJ));
         tmpfat->set_topTag_loose  (ljet_topTag_loose->at(iJ));
         if ( !fProd.Contains("0204230") ) tmpfat->set_bosonTag_loose(ljet_bosonTag_loose->at(iJ));

         int defTopTag = -1;
         if ( not(ljet_topTag_loose->at(iJ)) ) defTopTag = 0;
         else if ( ljet_topTag->at(iJ) ) defTopTag = 2;
         else defTopTag = 1;
         tmpfat->set_defTopTag(defTopTag);  // 0 if not top loose, 2 if top tight, else 1


         fatJets.push_back(tmpfat);
      } // for iJ

      // _____ jet loop _____
      for ( size_t ij = 0; ij < jet_pt->size(); ij++) {

         Jet* tmpjet = new Jet(new TLorentzVector(), jet_mv2c10->at(ij));
         tmpjet->v4()->SetPtEtaPhiE(jet_pt->at(ij)/1e3, jet_eta->at(ij), jet_phi->at(ij), jet_e->at(ij)/1e3);

         bool outsideAnyTopTagLoosePt250(true), outsideAnyTopTagTightPt250(true);

         // _____ assign a jet to exactly 1 fat jet _____
         // fat jets must satisfy dR ( jet , fat jets ) < 1.0
         // take the fat jet with minimal distance anti-Kt, defined as  dR ( jet , fat jet ) / fat jet pT
         double minAntiKtJtoj(1e4);
         // save the iterator of the fat jet with minimal distance anti-Kt
         vector<FatJet*>::iterator  closestJtoj(fatJets.end());

         double dRjetFatJet;
         // _____ large-R jet loop _____
         for( fatIt = fatJets.begin(); fatIt != fatJets.end(); ++fatIt) {
            dRjetFatJet = tmpjet->v4()->DeltaR(*(*fatIt)->v4());

            // _____ jet inside the large-R jet _____
            if ( dRjetFatJet < radius_J ) {
               if ((*fatIt)->topTag_loose() and (*fatIt)->v4()->Pt() > ptmin_ttag_3211) outsideAnyTopTagLoosePt250 = false;
               if ((*fatIt)->topTag() and (*fatIt)->v4()->Pt() > ptmin_ttag_3211) outsideAnyTopTagTightPt250 = false;

               double antiKtJtoj = dRjetFatJet / (*fatIt)->v4()->Pt();
               if (antiKtJtoj < minAntiKtJtoj) {
                  minAntiKtJtoj = antiKtJtoj;
                  closestJtoj = fatIt;
               }

            }
         } // for fatIt
         // add +1 to the large-R jet that matches with the jet, b-tag (70%, 77% WP), non b-tag (70%, 77% WP)
         if ( closestJtoj != fatJets.end() ) {
          (*closestJtoj)->set_njInsideJ    ( (*closestJtoj)->njInsideJ()     + 1 );

          if ( jet_mv2c10->at(ij) > mv2c10min_b70j ) {
             (*closestJtoj)->set_nb70jInsideJ ( (*closestJtoj)->nb70jInsideJ()  + 1 );
             (*closestJtoj)->set_sumMv2c10ib70 ( (*closestJtoj)->sumMv2c10ib70()  + jet_mv2c10->at(ij) );
             //(*closestJtoj)->set_sumPseudoMv2c10ib70 ( (*closestJtoj)->sumPseudoMv2c10ib70() + pseudoBtag(jet_mv2c10->at(ij)));
             (*closestJtoj)->set_sumPseudoMv2c10ib70 ( (*closestJtoj)->sumPseudoMv2c10ib70() + jet_tagWeightBin->at(ij));
          } else { 
            (*closestJtoj)->set_nnb70jInsideJ( (*closestJtoj)->nnb70jInsideJ() + 1 ); }

          if ( jet_mv2c10->at(ij) > mv2c10min_b77j ) {
             (*closestJtoj)->set_nb77jInsideJ ( (*closestJtoj)->nb77jInsideJ()  + 1 );
             (*closestJtoj)->set_sumMv2c10ib77 ( (*closestJtoj)->sumMv2c10ib77()  + jet_mv2c10->at(ij) );
             //(*closestJtoj)->set_sumPseudoMv2c10ib77 ( (*closestJtoj)->sumPseudoMv2c10ib77() + pseudoBtag(jet_mv2c10->at(ij)));
             (*closestJtoj)->set_sumPseudoMv2c10ib77 ( (*closestJtoj)->sumPseudoMv2c10ib77() + jet_tagWeightBin->at(ij));
          } else { 
            (*closestJtoj)->set_nnb77jInsideJ( (*closestJtoj)->nnb77jInsideJ() + 1 ); }

          if ( jet_mv2c10->at(ij) > mv2c10min_b85j ) {
             (*closestJtoj)->set_nb85jInsideJ ( (*closestJtoj)->nb85jInsideJ()  + 1 );
             (*closestJtoj)->set_sumMv2c10ib85 ( (*closestJtoj)->sumMv2c10ib85()  + jet_mv2c10->at(ij) );
             //(*closestJtoj)->set_sumPseudoMv2c10ib85 ( (*closestJtoj)->sumPseudoMv2c10ib85() + pseudoBtag(jet_mv2c10->at(ij)));
             (*closestJtoj)->set_sumPseudoMv2c10ib85 ( (*closestJtoj)->sumPseudoMv2c10ib85() + jet_tagWeightBin->at(ij));
             (*closestJtoj)->add_mv2c10ib85 ( jet_mv2c10->at(ij) );
          } else { 
            (*closestJtoj)->set_nnb85jInsideJ( (*closestJtoj)->nnb85jInsideJ() + 1 ); }

         }

         c_jet.push_back(tmpjet);

         nj++; // count the nb of jets 
         if (jet_mv2c10->at(ij) > mv2c10min_b70j) {
            nb70j++;  // count nb of b-tags 70% WP
            //sumPseudoMv2c10_b70 += pseudoBtag( jet_mv2c10->at(ij) );
            sumPseudoMv2c10_b70 += jet_tagWeightBin->at(ij);
         }
         if (jet_mv2c10->at(ij) > mv2c10min_b77j) {
            nb77j++;  // count nb of b-tags 77% WP
            //sumPseudoMv2c10_b77 += pseudoBtag( jet_mv2c10->at(ij) );
            sumPseudoMv2c10_b77 += jet_tagWeightBin->at(ij);
         }
         if (jet_mv2c10->at(ij) > mv2c10min_b85j) {
            nb85j++;  // count nb of b-tags 85% WP
            //sumPseudoMv2c10_b85 += pseudoBtag( jet_mv2c10->at(ij) );
            sumPseudoMv2c10_b85 += jet_tagWeightBin->at(ij);
         }

         if ( outsideAnyTopTagLoosePt250 ) {
            njOusidettags3211++; // count nb of jets outside any top-tag loose pT > 250 GeV
            if (jet_mv2c10->at(ij) > mv2c10min_b70j) nb70Outsidettags3211++; // same but for b-tags 70% WP
         }

         if ( outsideAnyTopTagTightPt250 ) {
            njOusidettagsTight3211++;
            if (jet_mv2c10->at(ij) > mv2c10min_b70j) nb70OutsidettagsTight3211++;
         }

      } // for ij

      leptonflavour = elchannel ? 0 : 1 ;

      met = new TLorentzVector();
      met->SetPtEtaPhiE(met_met/1e3, 0., met_phi, 0.);

      is3211 = isBoosted and nJPt250 >= 1 and nloosetJ >= 1 and nj >= 3 and njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 2;

      if(isResolved) {
         if (nJets == 4 && nb70j == 2) Pass_4JE2BE_tr = 1; 
         if (nJets == 4 && nb70j == 3) Pass_4JE3BE_tr = 1;
         if (nJets == 4 && nb70j >= 4) Pass_4JE4BI_tr = 1;
         if (nJets == 5 && nb70j == 2) Pass_5JE2BE_tr = 1;
         if (nJets == 5 && nb70j == 3) Pass_5JE3BE_tr = 1;
         if (nJets == 5 && nb70j >= 4) Pass_5JE4BI_tr = 1;
         if (nJets >= 6 && nb70j == 2) Pass_6JI2BE_tr = 1;
         if (nJets >= 6 && nb70j == 3) Pass_6JI3BE_tr = 1;
         if (nJets >= 6 && nb70j >= 4) Pass_6JI4BI_tr = 1;
      }

      if (nJets >= 4 && nb70j >= 2) isResolved = true;

      // define above your Higgs-tag and top-tag per selection 

      // first : initialize them
      for ( map<TString, bool>::iterator it = fselection.begin(); it != fselection.end(); ++it) {
         fHiggsTag[it->first] = fatJets.end();
         fTopTag[it->first] = fatJets.end();
      } // for fselections

      double maxSumMv2c10ib77_2ib77iPt200(-3), maxSumMv2c10ib85_2ib85iPt200(-3), maxSumMv2c10ib77_2ib77iPt250(-3);
      double maxMass_tl1ib77ePt250(0), maxMass_tl1ib85iPt250(0),
             maxPt_tl1ib70e1inb70iPt250(0), maxMv2c10_secondib85Pt200(-1);
      for( fatIt = fatJets.begin(); fatIt != fatJets.end(); ++fatIt) {
         if ( thereIs2ib77iPt200(*fatIt ) and  (*fatIt)->sumMv2c10ib77() > maxSumMv2c10ib77_2ib77iPt200 ) {
            maxSumMv2c10ib77_2ib77iPt200 = (*fatIt)->sumMv2c10ib77();
            fHiggsTag["1iJ2ib77iPt200_1iJtl1ib77ePt250"] = fatIt;
         }
         if ( thereIs2ib85iPt200(*fatIt ) ) {

            // take the candidate with highest sum of mv2c10 over b-tag 85% WP
            if ( (*fatIt)->sumMv2c10ib85() > maxSumMv2c10ib85_2ib85iPt200 ) {
               maxSumMv2c10ib85_2ib85iPt200 = (*fatIt)->sumMv2c10ib85();
               fHiggsTag["1iJ2ib85iPt200"] = fatIt;
               fHiggsTag["1iJ2ib85iPt200_1iJtl1ib85iPt250"] = fatIt;
            }

            // take the candidate with 2nd b-tag (order in mv2c10) has the highest mv2c10
            //if ( (*fatIt)->mv2c10ib85().at((*fatIt)->mv2c10ib85().size() -2) > maxMv2c10_secondib85Pt200 ) {
            //   maxMv2c10_secondib85Pt200 = (*fatIt)->mv2c10ib85().at((*fatIt)->mv2c10ib85().size() -2);
            //   fHiggsTag["1iJ2ib85iPt200_1iJtl1ib85iPt250"] = fatIt;
            //}
         }
         if ( thereIs2ib77iPt250(*fatIt ) and  (*fatIt)->sumMv2c10ib77() > maxSumMv2c10ib77_2ib77iPt250 ) {
            maxSumMv2c10ib77_2ib77iPt250 = (*fatIt)->sumMv2c10ib77();
            fHiggsTag["1iJ2ib77iPt250_1iJtl1ib77ePt250"] = fatIt;
         }
         if ( thereIstl1ib77ePt250(*fatIt ) and (*fatIt)->v4()->M() > maxMass_tl1ib77ePt250 ) {
            maxMass_tl1ib77ePt250 = (*fatIt)->v4()->M();
            fTopTag  ["1iJ2ib77iPt200_1iJtl1ib77ePt250"] = fatIt;
            fTopTag  ["1iJ2ib77iPt250_1iJtl1ib77ePt250"] = fatIt;
         }
         if ( thereIstl1ib85iPt250(*fatIt ) and (*fatIt)->v4()->M() > maxMass_tl1ib85iPt250 ) {
            maxMass_tl1ib85iPt250 = (*fatIt)->v4()->M();
            fTopTag  ["1iJtl1ib85iPt250"] = fatIt;
            fTopTag  ["1iJtl1ib85iPt250_1ab85i"] = fatIt;
         }
         if ( thereIstl1ib70e1inb70iPt250(*fatIt ) and (*fatIt)->v4()->Pt() > maxPt_tl1ib70e1inb70iPt250 ) {
            maxPt_tl1ib70e1inb70iPt250 = (*fatIt)->v4()->Pt();
            fTopTag  ["1iJtl1ib70e1inb70iPt250_3aji2ab70i"] = fatIt;
            fTopTag  ["1iJtl1ib70e1inb70iPt250_3aji3ab70i"] = fatIt;
         }
      } // for fatIt

      fHiggsTag["1iJ2ib77iPt200_1iJtl1ib77ePt250_0ab77e"] = fHiggsTag["1iJ2ib77iPt200_1iJtl1ib77ePt250"];
      fHiggsTag["1iJ2ib77iPt200_1iJtl1ib77ePt250_1ab77i"] = fHiggsTag["1iJ2ib77iPt200_1iJtl1ib77ePt250"];

      fHiggsTag["1iJ2ib85iPt200_1iJtl1ib85iPt250_0ab85e"] = fHiggsTag["1iJ2ib85iPt200_1iJtl1ib85iPt250"] ;
      fHiggsTag["1iJ2ib85iPt200_1iJtl1ib85iPt250_1ab85i"] = fHiggsTag["1iJ2ib85iPt200_1iJtl1ib85iPt250"] ;

      fHiggsTag["1iJ2ib77iPt250_1iJtl1ib77ePt250_0ab77e"] = fHiggsTag["1iJ2ib77iPt250_1iJtl1ib77ePt250"];
      fHiggsTag["1iJ2ib77iPt250_1iJtl1ib77ePt250_1ab77i"] = fHiggsTag["1iJ2ib77iPt250_1iJtl1ib77ePt250"];

      fTopTag  ["1iJ2ib77iPt200_1iJtl1ib77ePt250_0ab77e"] = fTopTag  ["1iJ2ib77iPt200_1iJtl1ib77ePt250"];
      fTopTag  ["1iJ2ib77iPt200_1iJtl1ib77ePt250_1ab77i"] = fTopTag  ["1iJ2ib77iPt200_1iJtl1ib77ePt250"];

      fTopTag  ["1iJ2ib77iPt250_1iJtl1ib77ePt250_0ab77e"] = fTopTag  ["1iJ2ib77iPt250_1iJtl1ib77ePt250"];
      fTopTag  ["1iJ2ib77iPt250_1iJtl1ib77ePt250_1ab77i"] = fTopTag  ["1iJ2ib77iPt250_1iJtl1ib77ePt250"];

      double maxMass_1ib85iPt250(0);
      // define here your top-tag per selection (special case where Higgs candidates and top candidates overlap)
      for( fatIt = fatJets.begin(); fatIt != fatJets.end(); ++fatIt) {
         if ( thereIstl1ib85iPt250(*fatIt ) and (*fatIt)->v4()->M() > maxMass_1ib85iPt250
           and fatIt != fHiggsTag["1iJ2ib85iPt200_1iJtl1ib85iPt250"]) { // not the Higgs-tag
            maxMass_1ib85iPt250 = (*fatIt)->v4()->M();
            fTopTag["1iJ2ib85iPt200_1iJtl1ib85iPt250"] = fatIt;
         }
      } // for fatIt

      if ( fTopTag.find("1iJ2ib85iPt200_1iJtl1ib85iPt250") != fTopTag.end() ) {
         fTopTag["1iJ2ib85iPt200_1iJtl1ib85iPt250_0ab85e"] = fTopTag["1iJ2ib85iPt200_1iJtl1ib85iPt250"];
         fTopTag["1iJ2ib85iPt200_1iJtl1ib85iPt250_1ab85i"] = fTopTag["1iJ2ib85iPt200_1iJtl1ib85iPt250"];
      }

      // _____ SELECTIONS : look at fselection in the header file _____
      fselection["preselection"] = true;
      fselection["resolved"] = isResolved; 
      fselection["boosted3211"] = is3211;
      //fselection["0ji0bi1te"] = nloosetJ == 1; 
      //fselection["0ji0bi2ti"] = nloosetJ >= 2; 
      //fselection["1te1le"] = nloosetJ == 1;               // exactly 1 top-jet
      //fselection["1te1el"] = nloosetJ == 1 and elchannel; // exactly 1 top-jet el channel
      //fselection["1te1mu"] = nloosetJ == 1 and muchannel; // exactly 1 top-jet mu channel
      //fselection["1ti1le"] = nloosetJ >= 1;               // at least 1 top-jet
      //fselection["1ti1el"] = nloosetJ >= 1 and elchannel; // at least 1 top-jet el channel
      //fselection["1ti1mu"] = nloosetJ >= 1 and muchannel; // at least 1 top-jet mu channel
      //fselection["3ji1bi1te1le"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 1 and nloosetJ == 1; 
      //fselection["3ji1bi1te1el"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 1 and nloosetJ == 1 and elchannel; 
      //fselection["3ji1bi1te1mu"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 1 and nloosetJ == 1 and muchannel; 
      //fselection["3ji1bi1ti1le"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 1 and nloosetJ >= 1; // preselection
      //fselection["3ji1bi1ti1el"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 1 and nloosetJ >= 1 and elchannel; // preselection e-channel
      //fselection["3ji1bi1ti1mu"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 1 and nloosetJ >= 1 and muchannel; // preselection mu-channel
      //fselection["3ji2bi1ti1le"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 2 and nloosetJ >= 1; // signal region 1
      //fselection["3ji2bi1ti1el"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 2 and nloosetJ >= 1 and elchannel; // signal region 1 e-channel
      //fselection["3ji2bi1ti1mu"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 2 and nloosetJ >= 1 and muchannel; // signal region 1 mu-channel
      //fselection["3ji2bi1te1le"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 2 and nloosetJ == 1; // signal region 1
      //fselection["3ji2bi1te1el"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 2 and nloosetJ == 1 and elchannel; // signal region 1 e-channel
      //fselection["3ji2bi1te1mu"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 2 and nloosetJ == 1 and muchannel; // signal region 1 mu-channel
      //fselection["3ji3bi1ti1le"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 3 and nloosetJ >= 1; // signal region 2
      //fselection["3ji3bi1ti1el"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 3 and nloosetJ >= 1 and elchannel; // signal region 2 e-channel
      //fselection["3ji3bi1ti1mu"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 3 and nloosetJ >= 1 and muchannel; // signal region 2 mu-channel
      //fselection["3ji3bi1te1le"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 3 and nloosetJ == 1; // signal region 2
      //fselection["3ji3bi1te1el"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 3 and nloosetJ == 1 and elchannel; // signal region 2 e-channel
      //fselection["3ji3bi1te1mu"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 3 and nloosetJ == 1 and muchannel; // signal region 2 mu-channel

      //fselection["3ji1bi1tle1le"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 1 and nloosetJ == 1; 
      //fselection["3ji1bi1tl1inbie1le"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 1 and nloosetJ == 1; 
      //fselection["3ji2bi1tle1le"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 2 and nloosetJ == 1; // signal region 1
      //fselection["3ji3bi1tle1le"] = njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 3 and nloosetJ == 1; // signal region 2
      // _____ tighest top-jet, exclusive _____
      //fselection["3ji2bi1tte"] = (ntighttJ == 1 and njOusidettagsTight3211 >= 3 and nb70OutsidettagsTight3211 >= 2) 
      //                         or ( nloosetJ == 1 and njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 2)
      //                         or ( ntighttJ == 0 and nloosetJ >= 2 and njOutOfFirtPtLoosetJ >= 3 and nb70jOutOfFirstPtLoosetJ >= 2);

      //fselection["3ji3bi1tte"] = (ntighttJ == 1 and njOusidettagsTight3211 >= 3 and nb70OutsidettagsTight3211 >= 3) 
      //                      or ( nloosetJ == 1 and njOusidettags3211 >= 3 and nb70Outsidettags3211 >= 3)
      //                      or ( ntighttJ == 0 and nloosetJ >= 2 and njOutOfFirtPtLoosetJ >= 3 and nb70jOutOfFirstPtLoosetJ >= 3);

      //// _____ resolved _____
      //fselection["re4ji2bi"] = nj >= 4 and nb70j >= 2; 
      //fselection["re4ji2biel"] = nj >= 4 and nb70j >= 2 and elchannel;
      //fselection["re4ji2bimu"] = nj >= 4 and nb70j >= 2 and muchannel;
        
        
      // _____ SELECTIONS : welcome to 2016 !  /////
      //fselection["1J2ib70iAnd1Jtl1ib70e1inb70iOutOfJ2ib70i"] = nJ2ib70i == 1 and nJtl1ib70e1inb70iOutOfJ2ib70i == 1 ;

      // 77% b-tag WP


      // __ min_element(... <-> 1st element : sort fat jets by decreasing nb of inside b-tags (77%),
      // in case of equality, choose the highest sum of mv2c10 over the inside b-tags 77% __
      //fHiggsTag["1iJ2ib77iPt200_1iJtl1ib77ePt250_1ab77i"] = min_element( fatJets.begin(), fatJets.end(), sumMv2c10ib77ordering);
      //fTopTag  ["1iJ2ib77iPt200_1iJtl1ib77ePt250_1ab77i"] = min_element( fatJets.begin(), fatJets.end(), nib77iTo1AndMassOrdering);

      //fselection["1iJPt250"] = count_if(fatJets.begin(), fatJets.end(), thereIsPt250) >= 1;

      //fselection["1iJ1ijiPt250"] = count_if(fatJets.begin(), fatJets.end(), thereIs1ijiPt250) >= 1;

      fselection["1iJtl1ib85iPt250"] = fTopTag["1iJtl1ib85iPt250"]  != fatJets.end()
                                               and isBoosted;

      int ab85_1iJtl1ib85iPt250 = -1;
      if ( fselection["1iJtl1ib85iPt250"] )  {
         ab85_1iJtl1ib85iPt250 = nb85j - (*fTopTag["1iJtl1ib85iPt250"])->nb85jInsideJ();
      }
      fselection["1iJtl1ib85iPt250_1ab85i"] = ab85_1iJtl1ib85iPt250 >= 1;

      bool is1iJtl1ib70e1inb70iPt250 =     fTopTag["1iJtl1ib70e1inb70iPt250_3aji2ab70i"] != fatJets.end()
                                               and isBoosted;

      // number of additional jets and b-tags, i.e outside the Higgs and top tags
      int aj_1iJtl1ib70e1inb70iPt250 = -1;
      int ab70_1iJtl1ib70e1inb70iPt250 = -1;
      if ( is1iJtl1ib70e1inb70iPt250 ) {
          aj_1iJtl1ib70e1inb70iPt250 = nj - (*fTopTag["1iJtl1ib70e1inb70iPt250_3aji2ab70i"])->njInsideJ()  ;
          ab70_1iJtl1ib70e1inb70iPt250 = nb70j - (*fTopTag["1iJtl1ib70e1inb70iPt250_3aji2ab70i"])->nb70jInsideJ()  ;
      }

      fselection["1iJtl1ib70e1inb70iPt250_3aji2ab70i"] = is1iJtl1ib70e1inb70iPt250 and aj_1iJtl1ib70e1inb70iPt250 >= 3 and ab70_1iJtl1ib70e1inb70iPt250 >= 2;
      fselection["1iJtl1ib70e1inb70iPt250_3aji3ab70i"] = is1iJtl1ib70e1inb70iPt250 and aj_1iJtl1ib70e1inb70iPt250 >= 3 and ab70_1iJtl1ib70e1inb70iPt250 >= 3;

      // _____ Higgs-tag pT > 200 GeV _____
      //fselection["1iJ2ib77iPt200"] = count_if(fatJets.begin(), fatJets.end(), thereIs2ib77iPt200) >= 1
      //                                  and isBoosted;

      //fselection["1iJ2ib77iPt200_1iJtl1ib77ePt250"] = count_if(fatJets.begin(), fatJets.end(), thereIs2ib77iPt200) >= 1 and
      //                                             count_if(fatJets.begin(), fatJets.end(), thereIstl1ib77ePt250) >= 1
      //                                             and isBoosted;

      //fselection["1iJ2ib77iPt200_1iJtl1ib77ePt250_0ab77e"] = count_if(fatJets.begin(), fatJets.end(), thereIs2ib77iPt200) >= 1
      //                                      and count_if(fatJets.begin(), fatJets.end(), thereIstl1ib77ePt250) >= 1 and
      //                                     nb77j - ( (*fHiggsTag["1iJ2ib77iPt200_1iJtl1ib77ePt250_0ab77e"])->nb77jInsideJ() +
      //                             (*fTopTag["1iJ2ib77iPt200_1iJtl1ib77ePt250_0ab77e"])->nb77jInsideJ() ) == 0
      //                                      and isBoosted;
      //                             // nb of jets in the events -
      //                             // nb of b-tags in the fat jet which contains the highest nb of b-tag - nb b-tag in top-tag

      //fselection["1iJ2ib77iPt200_1iJtl1ib77ePt250_1ab77i"] = count_if(fatJets.begin(), fatJets.end(), thereIs2ib77iPt200) >= 1
      //                                      and count_if(fatJets.begin(), fatJets.end(), thereIstl1ib77ePt250) >= 1 and
      //                                     nb77j - ( (*fHiggsTag["1iJ2ib77iPt200_1iJtl1ib77ePt250_1ab77i"])->nb77jInsideJ() +
      //                             (*fTopTag["1iJ2ib77iPt200_1iJtl1ib77ePt250_1ab77i"])->nb77jInsideJ() ) >= 1 
      //                                      and isBoosted;
      //                             // nb of jets in the events -
      //                             // nb of b-tags in the fat jet which contains the highest nb of b-tag - nb b-tag in top-tag


      //// _____ Higgs-tag pT > 250 GeV _____
      //fselection["1iJ2ib77iPt250"] = count_if(fatJets.begin(), fatJets.end(), thereIs2ib77iPt250) >= 1
      //                                      and isBoosted;

      //fselection["1iJ2ib77iPt250_1iJtl1ib77ePt250"] = count_if(fatJets.begin(), fatJets.end(), thereIs2ib77iPt250) >= 1 and
      //                                             count_if(fatJets.begin(), fatJets.end(), thereIstl1ib77ePt250) >= 1
      //                                      and isBoosted;

      //fselection["1iJ2ib77iPt250_1iJtl1ib77ePt250_0ab77e"] = count_if(fatJets.begin(), fatJets.end(), thereIs2ib77iPt250) >= 1
      //                                       and count_if(fatJets.begin(), fatJets.end(), thereIstl1ib77ePt250) >= 1 and
      //                                     nb77j - ( (*fHiggsTag["1iJ2ib77iPt250_1iJtl1ib77ePt250_0ab77e"])->nb77jInsideJ() +
      //                             (*fTopTag["1iJ2ib77iPt250_1iJtl1ib77ePt250_0ab77e"])->nb77jInsideJ() ) == 0 
      //                                      and isBoosted;
      //                             // nb of jets in the events -
      //                             // nb of b-tags in the fat jet which contains the highest nb of b-tag - nb b-tag in top-tag

      //fselection["1iJ2ib77iPt250_1iJtl1ib77ePt250_1ab77i"] = count_if(fatJets.begin(), fatJets.end(), thereIs2ib77iPt250) >= 1
      //                                       and count_if(fatJets.begin(), fatJets.end(), thereIstl1ib77ePt250) >= 1 and
      //                                     nb77j - ( (*fHiggsTag["1iJ2ib77iPt250_1iJtl1ib77ePt250_1ab77i"])->nb77jInsideJ() +
      //                             (*fTopTag["1iJ2ib77iPt250_1iJtl1ib77ePt250_1ab77i"])->nb77jInsideJ() ) >= 1
      //                                      and isBoosted;
      //                           // nb of jets in the events -
      //                           // nb of b-tags in the fat jet which contains the highest nb of b-tag - nb b-tag in top-tag


    // _____ b-tag 85% WP, Higgs-tag pT > 200 GeV _____
      fselection["1iJ2ib85iPt200"] = fHiggsTag["1iJ2ib85iPt200"] != fatJets.end()
                                            and isBoosted;

      bool is1iJ2ib85iPt200_1iJtl1ib85iPt250 = fHiggsTag["1iJ2ib85iPt200_1iJtl1ib85iPt250"] != fatJets.end() 
                                                    and fTopTag["1iJ2ib85iPt200_1iJtl1ib85iPt250"] != fatJets.end()
                                                    and isBoosted;

      fselection["1iJ2ib85iPt200_1iJtl1ib85iPt250"] = is1iJ2ib85iPt200_1iJtl1ib85iPt250;

      // number of additional b-tags 85%, i.e outside the Higgs and top tags
      int addbtags_1iJ2ib85iPt200_1iJtl1ib85iPt250 = -1;
      if ( is1iJ2ib85iPt200_1iJtl1ib85iPt250 ) addbtags_1iJ2ib85iPt200_1iJtl1ib85iPt250 = nb85j 
          - ( (*fHiggsTag["1iJ2ib85iPt200_1iJtl1ib85iPt250_0ab85e"])->nb85jInsideJ() 
                + (*fTopTag["1iJ2ib85iPt200_1iJtl1ib85iPt250_0ab85e"])->nb85jInsideJ() ) ;

      fselection["1iJ2ib85iPt200_1iJtl1ib85iPt250_0ab85e"] = is1iJ2ib85iPt200_1iJtl1ib85iPt250
                                                   and addbtags_1iJ2ib85iPt200_1iJtl1ib85iPt250 == 0;

      fselection["1iJ2ib85iPt200_1iJtl1ib85iPt250_1ab85i"] = is1iJ2ib85iPt200_1iJtl1ib85iPt250
                                                   and addbtags_1iJ2ib85iPt200_1iJtl1ib85iPt250 >= 1;


      if (fselection.size() != fnselections) {
         cout << "ERROR : you defined more(or less) selections in the Loop() than in the constructor" << endl;
         cout << "verify naming of your selections (keys of map)" << endl;
         return;
      }

      resolvedBDT_withReco_tr = ClassifBDTOutput_withReco_basic > -2 ? ClassifBDTOutput_withReco_basic : -2;
      //resolvedBDT_withReco_tr = ClassifBDTOutput_withReco_basic;
      //if (!fProd.Contains("1l_ljet")) resolvedBDT_withReco_tr =  ClassifBDTOutput_withReco_basic ;

      if(fsaveResolvedCutflow and isResolved) { cout << "ko 1" << endl;  FillCutflowResolved();}
      if( fsaveBoostedCutflow and isBoosted) { cout << "ko 2" << endl;   FillCutflowBoosted(); }
      // _____ loop over selections _____
      for ( map<TString, bool>::iterator it = fselection.begin(); it != fselection.end(); ++it) {

         if ( it->second ) {
         fEvtWeight = evtwgtb70;
         if (it->first.Contains("b77")) fEvtWeight = evtwgtb77;
         if (it->first.Contains("b85")) fEvtWeight = evtwgtbContinuous;

         // fill the histograms
         if (fTreeName == "nominal_Loose" and saveHistos) FillHisto(it->first, fEvtWeight);

         // fill the variables used by the Trees
         if (saveTreeForFit or saveTreeForTMVA) FillVarForTMVA(it->first);
         
         //if (it->first == "1iJ2ib85iPt200_1iJtl1ib85iPt250" and fTreeName == "nominal_Loose")
         //   maliste << mcChannelNumber << "  " << eventNumber << "      " << ClassifBDTOutput_withReco_basic << endl;


         // _____ fill the tree _____
         if (fTreeName == "nominal_Loose" and saveTreeForTMVA) {
            m_treeForTMVA[it->first]->Fill();
         }
         // evaluate the BDT weight
         if (saveTreeForFit and m_tmvaReader.find(it->first) != m_tmvaReader.end() )
              m_tmvaReader_output[it->first] = m_tmvaReader[it->first]->EvaluateMVA( "BDT" );


         } // if selection X
         else {
            if (fTreeName == "nominal_Loose" and saveTreeForTMVA) {
               if ( isResolved ) {
                  m_treeResolvedMinusSel[it->first]->Fill();
               }
            }

            // set default value to BDT weight if event do not pass the sel
            if (saveTreeForFit and m_tmvaReader.find(it->first) != m_tmvaReader.end() )
                 m_tmvaReader_output[it->first] = -2;

         }
      } // for selections

      if (saveTreeForFit) m_treeFitVarRegion->Fill();

      // _____ free memory
      delete lepton;
      for( jetIt = c_jet.begin(); jetIt != c_jet.end(); ++jetIt) {
         delete (*jetIt);
      } 
      c_jet.clear();
      for( fatIt = fatJets.begin(); fatIt != fatJets.end(); ++fatIt) {
         delete (*fatIt);
      } 
      fatJets.clear();
      delete met;
      //cout << "entry : " << jentry << endl;
      //if (jentry > 1e5) break;
   } // for jentry

   if (fTreeName == "nominal_Loose" and saveHistos) FinalizeHisto();
   if (fTreeName == "nominal_Loose" and saveTreeForTMVA) FinalizeTMVATree();
   if (saveTreeForFit) FinalizeFitTree();

   //maliste.close();
}
