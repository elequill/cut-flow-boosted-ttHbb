//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Feb  8 14:01:01 2016 by ROOT version 5.34/32
// from TTree nominal/tree
// found on file: user.elequill.samples2015forttH.p2454.250116/ttH/user.elequill.7530247._000001.output.root
//////////////////////////////////////////////////////////

#ifndef cutFlow_h
#define cutFlow_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "TSystem.h"

// Header file for the classes stored in the TTree if any.
#include <cmath>
#include <vector>
#include <vector>
#include <vector>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <TLorentzVector.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TString.h>
#include <TCanvas.h>
#include <map>
#include <utility>
#include "particleClasses/Lepton.C"
#include "particleClasses/FatJet.C"
#include "particleClasses/Jet.C"
#include "myFunctions.C"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"


// Fixed size dimensions of array or collections stored in the TTree if any.

using namespace std;

class cutFlow {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain
   TFile*          fFileOutHisto;
   //TFile*          fFileOutHisto_tr; // truth info
   TString         fProcess;
   TString         fProd; // production ex : "phys-higgs.02042303.1l_sys"
   TString         fTreeName; // tree name ex : "nominal_Loose"
   // _____ map of selection : output TFile with trees for TMVA _____
   map<TString, TFile*>  fFileOutTree;
   map<TString, TFile*>  fFileOutResolvedMinusSel;
   TFile*  fFileOutFitVarRegion;
   // _____ map of selection : tree _____
   map<TString, TTree*> m_treeForTMVA;
   map<TString, TTree*> m_treeResolvedMinusSel;  // tree contain resolved events minus the one in the selection
   TTree*  m_treeFitVarRegion;   // tree contain flag for each selection, and fitted variables
   double          fDataLumi; // fb-1
   bool            fBoosted;  // if true, use the boosted selection
   bool            fttbarb;   // if true, use of the ttbar + HF sample
   bool            fFirstBinCutflow;   // if true, set the number  of events in the dataset to fmcFirstBinCutFlow
   map<TString, bool> fselection;  // selection name, event selected True Or False
   map<TString, vector<FatJet*>::iterator> fHiggsTag;  // selection name, iterator to the Higgs candidate
   map<TString, vector<FatJet*>::iterator> fTopTag;  // selection name, iterator to the Higgs candidate
   unsigned int fnselections;

   map<TString, TMVA::Reader*> m_tmvaReader; // TMVA reader for a given selection
   map<TString, float> m_tmvaReader_output;

   //map<int,double> fmcLumi; // in fb-1
   map<int,double> fmcSumWeightTree; // in fb-1 ( sum the 'sumWeights' values over the 'sumWeights' trees in each dataset )
   map<int,double> fmcFirstBinCutFlow; // in fb-1
   map<int,double> fmcCrossSection; // in pb


   // _____ CUTS _____
   double ptmin_lep;

   // __ large-R jets __
   double ptmin_J; // (GeV)
   double ptmax_J;
   double massmin_J;
   double radius_J;
   double ptmin_ttag;
   double ptmin_ttag_3211;
   double etamax_J;

   // __  jets __ https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingBenchmarks#MV2c10_tagger_added_on_11th_May
   double mv2c10min_b60j;  // 60% WP 
   double mv2c10min_b70j;  // 70% WP 
   double mv2c10min_b77j;  // 77% WP 
   double mv2c10min_b85j;  // 85% WP 

   // _____ Object definition _____
   // _____ lepton
   Lepton* lepton;
   // _____ jets
   vector<Jet*> c_jet;   vector<Jet*>::iterator jetIt;
   // _____ large-R jets
   vector<FatJet*> fatJets;  vector<FatJet*>::iterator  fatIt, fatIt2;
   // _____ missing transverse momentum
   TLorentzVector* met;

   double fEvtWeight; // event  MC for a given selection
   float evtwgtb70;     // event MC weight, b-tag SF 70%
   float evtwgtb77;     // event MC weight, b-tag SF 77%
   float evtwgtb85;     // event MC weight, b-tag SF 85%
   float evtwgtbContinuous;     // event MC weight, b-tag SF for pseudo-continuous
   float evtwgtForCutflow; // without pile up weight
   bool muchannel, elchannel;
   bool is2015, is2016;
   int nel, nmu;        // number of el/mu with pT > 25 GeV
   // _____ large-R jet related quantities _____
   int nJ, nJPt250;                // number of large-R jets with pT > 250 GeV
   int nloosetJ;               // number of large-R jets with pT > 250 GeV and |eta| < 2.0 and loose top-tag
   int nloosetJPt250_woOR;          // number of large-R jets with pT > 250 GeV and |eta| < 2.0 and loose top-tag, wo el overlap removal (for cutflow)
   int ntighttJ;               // number of large-R jets with pT > 250 GeV and |eta| < 2.0 and tight top-tag
   // _____ jet related quantities _____
   int nj, nb70j, nb77j, nb85j, njOusidettags3211, nb70Outsidettags3211;  // number of jets pt > 25GeV, b jets, jets/b-tagged jets outside any top-tagged jet (loose)
   int njOusidettagsTight3211, nb70OutsidettagsTight3211;  // number of jets/b-tagged jets outside any top-tagged jet
   int njOutOfFirtPtLoosetJ, nb70jOutOfFirstPtLoosetJ;  // number of jets/b-tagged jets outside the first pT large-R jet
   double sumPseudoMv2c10_b70, sumPseudoMv2c10_b77, sumPseudoMv2c10_b85;
   bool is3211;
   bool isBoosted; bool isResolved;
   int Pass_4JE2BE_tr;  // == 4 jets, >= 2 b-tags (70%)
   int Pass_4JE3BE_tr;
   int Pass_4JE4BI_tr;
   int Pass_5JE2BE_tr;  // == 5 jets, >= 2 b-tags (70%)
   int Pass_5JE3BE_tr;
   int Pass_5JE4BI_tr;
   int Pass_6JI2BE_tr;  // == 6 jets, >= 2 b-tags (70%)
   int Pass_6JI3BE_tr;
   int Pass_6JI4BI_tr;
   bool fsaveBoostedCutflow, fsaveResolvedCutflow;
   int leptonflavour;
   float lumiDatasetMinus1;

   // HISTOS

   TH2D* h2_cutFlowValidationBoostedYield_elmu;
   TH2D* h2_cutFlowValidationBoostedStat_elmu;
   TH2D* h2_cutFlowValidationResolvedYield_elmu;
   TH2D* h2_cutFlowValidationResolvedStat_elmu;

   // _____ fat-jets ______
   map<TString,TH1D*> h1_J_n;
   map<TString,TH1D*> h1_J_pt;
   map<TString,TH1D*> h1_J_eta;
   map<TString,TH1D*> h1_J_phi;
   map<TString,TH1D*> h1_J_m;

   // _____ Higgs tag ______
   //map<TString,TH1D*> h1_Htag_n;
   map<TString,TH1D*> h1_Htag_pt;
   map<TString,TH1D*> h1_Htag_eta;
   map<TString,TH1D*> h1_Htag_phi;
   map<TString,TH1D*> h1_Htag_m;
   map<TString,TH1D*> h1_Htag_dRlep;
   map<TString,TH1D*> h1_Htag_nib70;
   map<TString,TH1D*> h1_Htag_ninb70;
   map<TString,TH1D*> h1_Htag_nib77;
   map<TString,TH1D*> h1_Htag_ninb77;
   map<TString,TH1D*> h1_Htag_nib85;
   map<TString,TH1D*> h1_Htag_ninb85;
   map<TString,TH1D*> h1_Htag_defTopTag;
   map<TString,TH1D*> h1_Htag_D2;
   map<TString,TH1D*> h1_Htag_C2;
   map<TString,TH1D*> h1_Htag_tau21_wta;
   map<TString,TH1D*> h1_Htag_tau32_wta;
   map<TString,TH1D*> h1_Htag_sumPseudoMv2c10ib70;
   map<TString,TH1D*> h1_Htag_sumPseudoMv2c10ib77;
   map<TString,TH1D*> h1_Htag_sumPseudoMv2c10ib85;

   // _____ top tag ______
   //map<TString,TH1D*> h1_ttag_n;
   map<TString,TH1D*> h1_ttag_pt;
   map<TString,TH1D*> h1_ttag_eta;
   map<TString,TH1D*> h1_ttag_phi;
   map<TString,TH1D*> h1_ttag_m;
   map<TString,TH1D*> h1_ttag_dRlep;
   map<TString,TH1D*> h1_ttag_nib70;
   map<TString,TH1D*> h1_ttag_ninb70;
   map<TString,TH1D*> h1_ttag_nib77;
   map<TString,TH1D*> h1_ttag_ninb77;
   map<TString,TH1D*> h1_ttag_nib85;
   map<TString,TH1D*> h1_ttag_ninb85;
   map<TString,TH1D*> h1_ttag_defTopTag;
   map<TString,TH1D*> h1_ttag_D2;
   map<TString,TH1D*> h1_ttag_C2;
   map<TString,TH1D*> h1_ttag_tau21_wta;
   map<TString,TH1D*> h1_ttag_tau32_wta;
   map<TString,TH1D*> h1_ttag_sumPseudoMv2c10ib70;
   map<TString,TH1D*> h1_ttag_sumPseudoMv2c10ib77;
   map<TString,TH1D*> h1_ttag_sumPseudoMv2c10ib85;

   map<TString,TH1D*> h1_J2ib77i_n;
   map<TString,TH1D*> h1_Jtl1ib77_n;


   //map<TString,TH1D*> h1_Jpair_2ib70i_tl1ib70eOrtt_n;
   //map<TString,TH1D*> h1_Jpair_1ib70em100_m140__n;

   map<TString,TH1D*> h1_hadTopPt;
   map<TString,TH2D*> h2_hadTopPt_ttbarPt;
   map<TString,TH2D*> h2_hadTopPt_ttbarPt_u;
   map<TString,TH2D*> h2_hadTopPt_ttbarPt_c;
   map<TString,TH2D*> h2_nJversusnB70;
   map<TString,TH2D*> h2_nJversusnB70_nMC;

   // _____ top-jets (loose) ______
   map<TString,TH1D*> h1_loosetJ_n;
   map<TString,TH1D*> h1_loosetJ_pt;
   map<TString,TH1D*> h1_loosetJ_eta;
   map<TString,TH1D*> h1_loosetJ_phi;
   map<TString,TH1D*> h1_loosetJ_m;
   map<TString,TH1D*> h1_loosetJ_dRlepton;

   // _____ top-jets (loose, no el overlap) ______


   // _____ top-jets (tight) ______
   map<TString,TH1D*> h1_tighttJ_n;
   map<TString,TH1D*> h1_tighttJ_pt;
   map<TString,TH1D*> h1_tighttJ_eta;
   map<TString,TH1D*> h1_tighttJ_phi;
   map<TString,TH1D*> h1_tighttJ_m;

   // _____ jets ______
   map<TString,TH1D*> h1_j_n;
   map<TString,TH1D*> h1_j_pt;
   map<TString,TH1D*> h1_j_eta;
   map<TString,TH1D*> h1_j_phi;

   map<TString,TH1D*> h1_j_nOutsideHnT;

   // _____ b- jets (70%)______
   map<TString,TH1D*> h1_b70j_n;
   map<TString,TH1D*> h1_b70j_pt;
   map<TString,TH1D*> h1_b70j_eta;
   map<TString,TH1D*> h1_b70j_phi;

   // _____ b- jets (77%)______
   map<TString,TH1D*> h1_b77j_n;
   map<TString,TH1D*> h1_b77j_pt;
   map<TString,TH1D*> h1_b77j_eta;
   map<TString,TH1D*> h1_b77j_phi;

   // _____ b- jets (85%)______
   map<TString,TH1D*> h1_b85j_n;
   map<TString,TH1D*> h1_b85j_pt;
   map<TString,TH1D*> h1_b85j_eta;
   map<TString,TH1D*> h1_b85j_phi;
   
   map<TString,TH1D*> h1_b77j_nOutsideHnT;
   map<TString,TH1D*> h1_b85j_nOutsideHnT;
   map<TString,TH1D*> h1_sumMv2c10_ob77_tNHtag;
   map<TString,TH1D*> h1_sumMv2c10_ob85_tNHtag;

   // _____ jets outside top-jet (loose) ______
   map<TString,TH1D*> h1_jOutOfLoosetJ_n;
   map<TString,TH1D*> h1_jOutOfLoosetJ_pt;
   map<TString,TH1D*> h1_jOutOfLoosetJ_eta;
   map<TString,TH1D*> h1_jOutOfLoosetJ_phi;

   // _____ b-jets (70%) outside top-jet (loose) ______
   map<TString,TH1D*> h1_b70jOutOfLoosetJ_n;
   map<TString,TH1D*> h1_b70jOutOfLoosetJ_pt;
   map<TString,TH1D*> h1_b70jOutOfLoosetJ_eta;
   map<TString,TH1D*> h1_b70jOutOfLoosetJ_phi;

   // _____ electron _____
   map<TString,TH1D*> h1_el_n  ;
   map<TString,TH1D*> h1_el_n_cutflow  ;
   map<TString,TH1D*> h1_el_pt;
   map<TString,TH1D*> h1_el_eta;
   map<TString,TH1D*> h1_el_phi;

   // _____ muon _____
   map<TString,TH1D*> h1_mu_n  ;
   map<TString,TH1D*> h1_mu_pt;
   map<TString,TH1D*> h1_mu_eta;
   map<TString,TH1D*> h1_mu_phi;

   // _____ MET _____
   map<TString,TH1D*> h1_met_met;
   map<TString,TH1D*> h1_met_phi;

   map<TString,TH1D*> h1_all;
   map<TString,TH1D*> h1_dR_ttagHtag;
   map<TString,TH1D*> h1_TopHFFilterFlag;
   map<TString,TH1D*> h1_ClassifBDTOutput_withReco_basic_binned;
   map<TString,TH1D*> h1_ClassifBDTOutput_withReco_basic;

   // _____ declare variables to write in trees _____
   //  Higgs-tagg                                 top-tag
   float Htag_pt_tr;                            float ttag_pt_tr;
   float Htag_eta_tr;                           float ttag_eta_tr;
   float Htag_phi_tr;                           float ttag_phi_tr;
   float Htag_m_tr;                             float ttag_m_tr;
   float Htag_dRlep_tr;                         float ttag_dRlep_tr;
   float Htag_nib70_tr;                         float ttag_nib70_tr;    // 70% WP
   float Htag_ninb70_tr;                        float ttag_ninb70_tr;
   float Htag_nib77_tr;                         float ttag_nib77_tr;    // 77% WP
   float Htag_ninb77_tr;                        float ttag_ninb77_tr;
   float Htag_nib85_tr;                         float ttag_nib85_tr;    // 85% WP
   float Htag_ninb85_tr;                        float ttag_ninb85_tr;
   float Htag_defTopTag_tr;                     float ttag_defTopTag_tr;// 0 if not top loose, 2 if top tight, else 1
   float Htag_D2_tr;                            float ttag_D2_tr;
   float Htag_C2_tr;                            float ttag_C2_tr;
   float Htag_tau21_wta_tr;                     float ttag_tau21_wta_tr;
   float Htag_tau32_wta_tr;                     float ttag_tau32_wta_tr;
   float Htag_sumPseudoMv2c10ib70_tr;           float ttag_sumPseudoMv2c10ib70_tr;
   float Htag_sumPseudoMv2c10ib77_tr;           float ttag_sumPseudoMv2c10ib77_tr;
   float Htag_sumPseudoMv2c10ib85_tr;           float ttag_sumPseudoMv2c10ib85_tr;

   
   float sumMv2c10_ob70_tNHtag_tr;
   float sumMv2c10_ob77_tNHtag_tr;
   float sumMv2c10_ob85_tNHtag_tr;
   float nob70_tNHtag_tr;
   float nob77_tNHtag_tr;
   float nob85_tNHtag_tr;
   float noj_tNHtag_tr;
   float nb70_tr;
   float nb77_tr;
   float nb85_tr;
   float nj_tr;
   float dR_ttagHtag_tr;
   float selFinalWeight_tr;
   float resolvedBDT_withReco_tr;


   // Declaration of leaf types
   vector<float>   *mc_generator_weights;
   Float_t         weight_mc;
   Float_t         weight_pileup;
   Float_t         weight_leptonSF;
   Float_t         weight_bTagSF_60;
   Float_t         weight_bTagSF_70;
   Float_t         weight_bTagSF_77;
   Float_t         weight_bTagSF_85;
   Float_t         weight_bTagSF_Continuous;
   Float_t         weight_jvt;
   Float_t         weight_pileup_UP;
   Float_t         weight_pileup_DOWN;
   Float_t         weight_leptonSF_EL_SF_Trigger_UP;
   Float_t         weight_leptonSF_EL_SF_Trigger_DOWN;
   Float_t         weight_leptonSF_EL_SF_Reco_UP;
   Float_t         weight_leptonSF_EL_SF_Reco_DOWN;
   Float_t         weight_leptonSF_EL_SF_ID_UP;
   Float_t         weight_leptonSF_EL_SF_ID_DOWN;
   Float_t         weight_leptonSF_EL_SF_Isol_UP;
   Float_t         weight_leptonSF_EL_SF_Isol_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
   Float_t         weight_indiv_SF_EL_Trigger;
   Float_t         weight_indiv_SF_EL_Trigger_UP;
   Float_t         weight_indiv_SF_EL_Trigger_DOWN;
   Float_t         weight_indiv_SF_EL_Reco;
   Float_t         weight_indiv_SF_EL_Reco_UP;
   Float_t         weight_indiv_SF_EL_Reco_DOWN;
   Float_t         weight_indiv_SF_EL_ID;
   Float_t         weight_indiv_SF_EL_ID_UP;
   Float_t         weight_indiv_SF_EL_ID_DOWN;
   Float_t         weight_indiv_SF_EL_Isol;
   Float_t         weight_indiv_SF_EL_Isol_UP;
   Float_t         weight_indiv_SF_EL_Isol_DOWN;
   Float_t         weight_indiv_SF_MU_Trigger;
   Float_t         weight_indiv_SF_MU_Trigger_STAT_UP;
   Float_t         weight_indiv_SF_MU_Trigger_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_Trigger_SYST_UP;
   Float_t         weight_indiv_SF_MU_Trigger_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_ID;
   Float_t         weight_indiv_SF_MU_ID_STAT_UP;
   Float_t         weight_indiv_SF_MU_ID_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_ID_SYST_UP;
   Float_t         weight_indiv_SF_MU_ID_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_ID_STAT_LOWPT_UP;
   Float_t         weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN;
   Float_t         weight_indiv_SF_MU_ID_SYST_LOWPT_UP;
   Float_t         weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN;
   Float_t         weight_indiv_SF_MU_Isol;
   Float_t         weight_indiv_SF_MU_Isol_STAT_UP;
   Float_t         weight_indiv_SF_MU_Isol_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_Isol_SYST_UP;
   Float_t         weight_indiv_SF_MU_Isol_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_TTVA;
   Float_t         weight_indiv_SF_MU_TTVA_STAT_UP;
   Float_t         weight_indiv_SF_MU_TTVA_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_TTVA_SYST_UP;
   Float_t         weight_indiv_SF_MU_TTVA_SYST_DOWN;
   Float_t         weight_jvt_UP;
   Float_t         weight_jvt_DOWN;
   vector<float>   *weight_bTagSF_60_eigenvars_B_up;
   vector<float>   *weight_bTagSF_60_eigenvars_C_up;
   vector<float>   *weight_bTagSF_60_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_60_eigenvars_B_down;
   vector<float>   *weight_bTagSF_60_eigenvars_C_down;
   vector<float>   *weight_bTagSF_60_eigenvars_Light_down;
   Float_t         weight_bTagSF_60_extrapolation_up;
   Float_t         weight_bTagSF_60_extrapolation_down;
   Float_t         weight_bTagSF_60_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_60_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_70_eigenvars_B_up;
   vector<float>   *weight_bTagSF_70_eigenvars_C_up;
   vector<float>   *weight_bTagSF_70_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_70_eigenvars_B_down;
   vector<float>   *weight_bTagSF_70_eigenvars_C_down;
   vector<float>   *weight_bTagSF_70_eigenvars_Light_down;
   Float_t         weight_bTagSF_70_extrapolation_up;
   Float_t         weight_bTagSF_70_extrapolation_down;
   Float_t         weight_bTagSF_70_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_70_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_77_eigenvars_B_up;
   vector<float>   *weight_bTagSF_77_eigenvars_C_up;
   vector<float>   *weight_bTagSF_77_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_77_eigenvars_B_down;
   vector<float>   *weight_bTagSF_77_eigenvars_C_down;
   vector<float>   *weight_bTagSF_77_eigenvars_Light_down;
   Float_t         weight_bTagSF_77_extrapolation_up;
   Float_t         weight_bTagSF_77_extrapolation_down;
   Float_t         weight_bTagSF_77_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_77_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_85_eigenvars_B_up;
   vector<float>   *weight_bTagSF_85_eigenvars_C_up;
   vector<float>   *weight_bTagSF_85_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_85_eigenvars_B_down;
   vector<float>   *weight_bTagSF_85_eigenvars_C_down;
   vector<float>   *weight_bTagSF_85_eigenvars_Light_down;
   Float_t         weight_bTagSF_85_extrapolation_up;
   Float_t         weight_bTagSF_85_extrapolation_down;
   Float_t         weight_bTagSF_85_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_85_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_Continuous_eigenvars_B_up;
   vector<float>   *weight_bTagSF_Continuous_eigenvars_C_up;
   vector<float>   *weight_bTagSF_Continuous_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_Continuous_eigenvars_B_down;
   vector<float>   *weight_bTagSF_Continuous_eigenvars_C_down;
   vector<float>   *weight_bTagSF_Continuous_eigenvars_Light_down;
   Float_t         weight_bTagSF_Continuous_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_Continuous_extrapolation_from_charm_down;
   ULong64_t       eventNumber;
   UInt_t          runNumber;
   UInt_t          randomRunNumber;
   UInt_t          mcChannelNumber;
   Float_t         mu;
   UInt_t          backgroundFlags;
   UInt_t          hasBadMuon;
   vector<float>   *el_pt;
   vector<float>   *el_eta;
   vector<float>   *el_cl_eta;
   vector<float>   *el_phi;
   vector<float>   *el_e;
   vector<float>   *el_charge;
   vector<float>   *el_topoetcone20;
   vector<float>   *el_ptvarcone20;
   vector<char>    *el_isTight;
   vector<float>   *el_d0sig;
   vector<float>   *el_delta_z0_sintheta;
   vector<int>     *el_true_type;
   vector<int>     *el_true_origin;
   vector<int>     *el_true_typebkg;
   vector<int>     *el_true_originbkg;
   vector<float>   *mu_pt;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   vector<float>   *mu_e;
   vector<float>   *mu_charge;
   vector<float>   *mu_topoetcone20;
   vector<float>   *mu_ptvarcone30;
   vector<char>    *mu_isTight;
   vector<float>   *mu_d0sig;
   vector<float>   *mu_delta_z0_sintheta;
   vector<int>     *mu_true_type;
   vector<int>     *mu_true_origin;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_e;
   vector<float>   *jet_mv2c10;
   vector<float>   *jet_mv2c20;
   vector<float>   *jet_jvt;
   vector<int>     *jet_truthflav;
   vector<char>    *jet_isbtagged_60;
   vector<char>    *jet_isbtagged_70;
   vector<char>    *jet_isbtagged_77;
   vector<char>    *jet_isbtagged_85;
   vector<char>    *jet_isbtagged_FlatBEff_30;
   vector<char>    *jet_isbtagged_FlatBEff_40;
   vector<char>    *jet_isbtagged_FlatBEff_50;
   vector<char>    *jet_isbtagged_FlatBEff_60;
   vector<char>    *jet_isbtagged_FlatBEff_70;
   vector<char>    *jet_isbtagged_FlatBEff_77;
   vector<char>    *jet_isbtagged_FlatBEff_85;
   vector<char>    *jet_isbtagged_30;
   vector<char>    *jet_isbtagged_50;
   vector<char>    *jet_isbtagged_80;
   vector<char>    *jet_isbtagged_90;
   vector<int>     *jet_tagWeightBin;
   vector<float>   *ljet_pt;
   vector<float>   *ljet_eta;
   vector<float>   *ljet_phi;
   vector<float>   *ljet_e;
   vector<float>   *ljet_m;
   vector<float>   *ljet_sd12;
   vector<float>   *rcjet_pt;
   vector<float>   *rcjet_eta;
   vector<float>   *rcjet_phi;
   vector<float>   *rcjet_e;
   vector<float>   *rcjet_d12;
   vector<float>   *rcjet_d23;
   vector<vector<float> > *rcjetsub_pt;
   vector<vector<float> > *rcjetsub_eta;
   vector<vector<float> > *rcjetsub_phi;
   vector<vector<float> > *rcjetsub_e;
   vector<vector<float> > *rcjetsub_mv2c10;
   Float_t         met_met;
   Float_t         met_phi;
   Int_t           boosted_ejets_2015;
   Int_t           boosted_ejets_2016;
   Int_t           boosted_mujets_2015;
   Int_t           boosted_mujets_2016;
   Int_t           ejets_2015;
   Int_t           ejets_2016;
   Int_t           mujets_2015;
   Int_t           mujets_2016;
   Int_t           ee_2015;
   Int_t           ee_2016;
   Int_t           mumu_2015;
   Int_t           mumu_2016;
   Int_t           emu_2015;
   Int_t           emu_2016;
   Char_t          HLT_mu20_iloose_L1MU15;
   Char_t          HLT_e60_lhmedium_nod0;
   Char_t          HLT_mu26_ivarmedium;
   Char_t          HLT_e26_lhtight_nod0_ivarloose;
   Char_t          HLT_e140_lhloose_nod0;
   Char_t          HLT_mu50;
   Char_t          HLT_e60_lhmedium;
   Char_t          HLT_e24_lhmedium_L1EM20VH;
   Char_t          HLT_e120_lhloose;
   vector<char>    *el_trigMatch_HLT_e60_lhmedium_nod0;
   vector<char>    *el_trigMatch_HLT_e26_lhtight_nod0_ivarloose;
   vector<char>    *el_trigMatch_HLT_e140_lhloose_nod0;
   vector<char>    *el_trigMatch_HLT_e60_lhmedium;
   vector<char>    *el_trigMatch_HLT_e24_lhmedium_L1EM20VH;
   vector<char>    *el_trigMatch_HLT_e120_lhloose;
   vector<char>    *mu_trigMatch_HLT_mu26_ivarmedium;
   vector<char>    *mu_trigMatch_HLT_mu50;
   vector<char>    *mu_trigMatch_HLT_mu20_iloose_L1MU15;
   Int_t           TopHeavyFlavorFilterFlag;
   ULong64_t       prwHash;
   vector<int>     *el_true_pdg;
   vector<float>   *el_true_pt;
   vector<float>   *el_true_eta;
   vector<int>     *mu_true_pdg;
   vector<float>   *mu_true_pt;
   vector<float>   *mu_true_eta;
   vector<int>     *jet_truthPartonLabel;
   Int_t           nJets;
   Int_t           nBTags;
   Int_t           nBTags60;
   Int_t           nBTags70;
   Int_t           nBTags77;
   Int_t           nBTags85;
   Int_t           nBTagsFlatBEff_30;
   Int_t           nBTagsFlatBEff_40;
   Int_t           nBTagsFlatBEff_50;
   Int_t           nBTagsFlatBEff_60;
   Int_t           nBTagsFlatBEff_70;
   Int_t           nBTagsFlatBEff_77;
   Int_t           nBTagsFlatBEff_85;
   Int_t           nBTags30;
   Int_t           nBTags50;
   Int_t           nBTags80;
   Int_t           nBTags90;
   Int_t           nPrimaryVtx;
   Int_t           nElectrons;
   Int_t           nMuons;
   Int_t           nHFJets;
   Float_t         HT_jets;
   Float_t         HT_all;
   Float_t         Centrality_all;
   Float_t         Mbb_MindR;
   Float_t         dRbb_MaxPt;
   Float_t         Mjj_MaxPt;
   Float_t         pT_jet5;
   Float_t         H1_all;
   Float_t         dRbb_avg;
   Float_t         Mbj_MaxPt;
   Float_t         dRlepbb_MindR;
   Float_t         Muu_MindR;
   Float_t         Aplan_bjets;
   Float_t         dRuu_MindR;
   Float_t         Mjjj_MaxPt;
   Int_t           Njet_pt40;
   Float_t         Mbj_MindR;
   Float_t         Mjj_MindR;
   Float_t         pTuu_MindR;
   Float_t         Mbb_MaxM;
   Float_t         Mbj_Wmass;
   Float_t         dRbj_Wmass;
   Int_t           tauVetoFlag;
   Int_t           isTrueSL;
   Int_t           isTrueDIL;
   Float_t         dEtajj_MaxdEta;
   Float_t         MHiggs;
   Float_t         dRHl_MindR;
   Int_t           NHiggs_30;
   Float_t         Mjj_MinM;
   Float_t         dRHl_MaxdR;
   Float_t         Mjj_HiggsMass;
   Float_t         dRlj_MindR;
   Float_t         H4_all;
   Float_t         pT_jet3;
   Float_t         dRbb_MaxM;
   Float_t         Aplan_jets;
   //Int_t           Njet_pt40;
   Float_t         Mbb_MaxPt;
   Int_t           nLjets;
   Float_t         HhadT_nJets;
   Float_t         HhadT_nLjets;
   Float_t         FirstLjetPt;
   Float_t         SecondLjetPt;
   Float_t         FirstLjetM;
   Float_t         SecondLjetM;
   Float_t         HT_ljets;
   Int_t           nLjet_m100;
   Int_t           nLjet_m50;
   Int_t           nJetOutsideLjet;
   Int_t           nBjetOutsideLjet;
   Float_t         dRbb_min;
   Float_t         dRjj_min;
   Float_t         HiggsbbM;
   Float_t         HiggsjjM;
   vector<float>   *ljet_sd23;
   vector<float>   *ljet_tau21;
   vector<float>   *ljet_tau32;
   vector<float>   *ljet_tau21_wta;
   vector<float>   *ljet_tau32_wta;
   vector<float>   *ljet_D2;
   vector<float>   *ljet_C2;
   vector<int>     *ljet_topTag;
   vector<bool>    *ljet_bosonTag;
   vector<int>     *ljet_topTag_loose;
   vector<bool>    *ljet_bosonTag_loose;
   Int_t           ljet_topTagN;
   Int_t           ljet_topTagN_loose;
   vector<int>     *ljet_truthmatch;
   vector<int>     *jet_truthmatch;
   vector<float>   *truth_jet_pt;
   vector<float>   *truth_jet_eta;
   vector<float>   *truth_jet_phi;
   vector<float>   *truth_jet_m;
   vector<float>   *truth_pt;
   vector<float>   *truth_eta;
   vector<float>   *truth_phi;
   vector<float>   *truth_m;
   vector<int>     *truth_pdgid;
   vector<int>     *truth_status;
   vector<int>     *truth_tthbb_info;
   Char_t          truth_nHiggs;
   Char_t          truth_nTop;
   Char_t          truth_nLepTop;
   Char_t          truth_nVectorBoson;
   Float_t         truth_ttbar_pt;
   Float_t         truth_top_pt;
   Float_t         truth_higgs_eta;
   Float_t         truth_tbar_pt;
   Char_t          truth_HDecay;
   Int_t           truth_top_dilep_filter;
   Float_t         semilepMVAreco_higgs_mass;
   Float_t         semilepMVAreco_bbhiggs_dR;
   Float_t         semilepMVAreco_higgsbleptop_mass;
   Float_t         semilepMVAreco_higgsleptop_dR;
   Float_t         semilepMVAreco_higgslep_dR;
   Float_t         semilepMVAreco_leptophadtop_dR;
   Float_t         semilepMVAreco_higgsq1hadW_mass;
   Float_t         semilepMVAreco_hadWb1Higgs_mass;
   Float_t         semilepMVAreco_b1higgsbhadtop_dR;
   Float_t         semilepMVAreco_higgsbleptop_withH_dR;
   Float_t         semilepMVAreco_higgsttbar_withH_dR;
   Float_t         semilepMVAreco_higgsbhadtop_withH_dR;
   Float_t         semilepMVAreco_leptophadtop_withH_dR;
   Float_t         semilepMVAreco_ttH_Ht_withH;
   Float_t         semilepMVAreco_BDT_output;
   Float_t         semilepMVAreco_BDT_withH_output;
   Int_t           semilepMVAreco_BDT_output_truthMatchPattern;
   Int_t           semilepMVAreco_BDT_withH_output_truthMatchPattern;
   Int_t           semilepMVAreco_Ncombinations;
   Int_t           semilepMVAreco_nuApprox_recoBDT;
   Int_t           semilepMVAreco_nuApprox_recoBDT_withH;
   vector<int>     *jet_semilepMVAreco_recoBDT_cand;
   vector<int>     *jet_semilepMVAreco_recoBDT_withH_cand;
   Float_t         semilepMVAreco_BDT_output_6jsplit;
   Float_t         semilepMVAreco_BDT_withH_output_6jsplit;
   Int_t           semilepMVAreco_nuApprox_recoBDT_6jsplit;
   Int_t           semilepMVAreco_nuApprox_recoBDT_withH_6jsplit;
   vector<int>     *jet_semilepMVAreco_recoBDT_cand_6jsplit;
   vector<int>     *jet_semilepMVAreco_recoBDT_withH_cand_6jsplit;
   Float_t         ClassifBDTOutput_basic;
   Float_t         ClassifBDTOutput_withReco_basic;
   Float_t         ClassifBDTOutput_6jsplit;
   Float_t         ClassifBDTOutput_withReco_6jsplit;
   Float_t         Zjets_Systematic_ckkw15;
   Float_t         Zjets_Systematic_ckkw30;
   Float_t         Zjets_Systematic_fac025;
   Float_t         Zjets_Systematic_fac4;
   Float_t         Zjets_Systematic_renorm025;
   Float_t         Zjets_Systematic_renorm4;
   Float_t         Zjets_Systematic_qsf025;
   Float_t         Zjets_Systematic_qsf4;
   Float_t         NBFricoNN_ljets;
   Float_t         NBFricoNN_dil;
   Float_t         ClassifHPLUS_Semilep_HF_BDT200_Output;
   Float_t         ClassifHPLUS_Semilep_HF_BDT225_Output;
   Float_t         ClassifHPLUS_Semilep_HF_BDT250_Output;
   Float_t         ClassifHPLUS_Semilep_HF_BDT275_Output;
   Float_t         ClassifHPLUS_Semilep_HF_BDT300_Output;
   Float_t         ClassifHPLUS_Semilep_HF_BDT350_Output;
   Float_t         ClassifHPLUS_Semilep_HF_BDT400_Output;
   Float_t         ClassifHPLUS_Semilep_HF_BDT500_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT1000_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT2000_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT200_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT225_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT250_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT275_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT300_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT350_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT400_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT500_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT600_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT700_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT800_Output;
   Float_t         ClassifHPLUS_Semilep_INC_BDT900_Output;
   vector<int>     *el_LHMedium;
   vector<int>     *el_LHTight;
   vector<char>    *el_isoGradient;
   vector<char>    *el_isoGradientLoose;
   vector<char>    *el_isoTight;
   vector<char>    *el_isoLoose;
   vector<char>    *el_isoLooseTrackOnly;
   vector<char>    *el_isoFixedCutTight;
   vector<char>    *el_isoFixedCutTightTrackOnly;
   vector<char>    *el_isoFixedCutLoose;
   vector<char>    *mu_Tight;
   vector<char>    *mu_Medium;
   vector<char>    *mu_isoGradient;
   vector<char>    *mu_isoGradientLoose;
   vector<char>    *mu_isoTight;
   vector<char>    *mu_isoLoose;
   vector<char>    *mu_isoLooseTrackOnly;
   vector<char>    *mu_isoFixedCutTightTrackOnly;
   vector<char>    *mu_isoFixedCutLoose;
   Int_t           HF_Classification;
   Int_t           HF_SimpleClassification;
   Float_t         q1_pt;
   Float_t         q1_eta;
   Float_t         q1_phi;
   Float_t         q1_m;
   Float_t         q2_pt;
   Float_t         q2_eta;
   Float_t         q2_phi;
   Float_t         q2_m;
   Float_t         qq_pt;
   Float_t         qq_ht;
   Float_t         qq_dr;
   Float_t         qq_m;
   Int_t           nTruthJets15;
   Int_t           nTruthJets20;
   Int_t           nTruthJets25;
   Int_t           nTruthJets25W;
   Int_t           nTruthJets50;
   Float_t         weight_ttbb_Nominal;
   Float_t         weight_ttbb_CSS_KIN;
   Float_t         weight_ttbb_NNPDF;
   Float_t         weight_ttbb_MSTW;
   Float_t         weight_ttbb_Q_CMMPS;
   Float_t         weight_ttbb_glosoft;
   Float_t         weight_ttbb_defaultX05;
   Float_t         weight_ttbb_defaultX2;
   Float_t         weight_ttbb_MPIup;
   Float_t         weight_ttbb_MPIdown;
   Float_t         weight_ttbb_MPIfactor;
   Float_t         weight_ttbb_aMcAtNloHpp;
   Float_t         weight_ttbb_aMcAtNloPy8;
   Float_t         weight_ttbar_FracRw;
   Float_t         ttHF_mva_discriminant;

   Float_t         mu_original_xAOD;
   Int_t           boosted_ejets_2015_Loose;
   Int_t           boosted_ejets_2016_Loose;
   Int_t           boosted_mujets_2015_Loose;
   Int_t           boosted_mujets_2016_Loose;
   Int_t           ejets_2015_Loose;
   Int_t           ejets_2016_Loose;
   Int_t           mujets_2015_Loose;
   Int_t           mujets_2016_Loose;
   Char_t          HLT_mu24;
   Char_t          HLT_e24_lhmedium_nod0_L1EM18VH;
   vector<char>    *el_trigMatch_HLT_e24_lhmedium_nod0_L1EM18VH;
   vector<char>    *mu_trigMatch_HLT_mu24;
   Float_t         PS_HLT_mu24;
   Float_t         PS_HLT_e120_lhloose;
   Float_t         PS_HLT_e24_lhmedium_L1EM20VH;
   Float_t         PS_HLT_mu50;
   Float_t         PS_HLT_e140_lhloose_nod0;
   Float_t         PS_HLT_e60_lhmedium;
   Float_t         PS_HLT_e26_lhtight_nod0_ivarloose;
   Float_t         PS_HLT_mu26_ivarmedium;
   Float_t         PS_HLT_e60_lhmedium_nod0;
   Float_t         PS_HLT_e24_lhmedium_nod0_L1EM18VH;
   Float_t         PS_HLT_mu20_iloose_L1MU15;

   // List of branches
   TBranch        *b_mc_generator_weights;   //!
   TBranch        *b_weight_mc;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_leptonSF;   //!
   TBranch        *b_weight_bTagSF_60;   //!
   TBranch        *b_weight_bTagSF_70;   //!
   TBranch        *b_weight_bTagSF_77;   //!
   TBranch        *b_weight_bTagSF_85;   //!
   TBranch        *b_weight_bTagSF_Continuous;   //!
   TBranch        *b_weight_jvt;   //!
   TBranch        *b_weight_pileup_UP;   //!
   TBranch        *b_weight_pileup_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_Trigger;   //!
   TBranch        *b_weight_indiv_SF_EL_Trigger_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_Trigger_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ID;   //!
   TBranch        *b_weight_indiv_SF_EL_ID_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ID_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_SYST_DOWN;   //!
   TBranch        *b_weight_jvt_UP;   //!
   TBranch        *b_weight_jvt_DOWN;   //!
   TBranch        *b_weight_bTagSF_60_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_60_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_60_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_60_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_60_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_60_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_60_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_60_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_60_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_60_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_70_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_70_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_70_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_70_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_77_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_77_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_77_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_77_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_85_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_85_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_85_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_85_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_85_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_85_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_85_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_85_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_85_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_85_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_Continuous_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_Continuous_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_Continuous_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_Continuous_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_Continuous_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_Continuous_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_Continuous_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_Continuous_extrapolation_from_charm_down;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_randomRunNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_backgroundFlags;   //!
   TBranch        *b_hasBadMuon;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_cl_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_topoetcone20;   //!
   TBranch        *b_el_ptvarcone20;   //!
   TBranch        *b_el_isTight;   //!
   TBranch        *b_el_d0sig;   //!
   TBranch        *b_el_delta_z0_sintheta;   //!
   TBranch        *b_el_true_type;   //!
   TBranch        *b_el_true_origin;   //!
   TBranch        *b_el_true_typebkg;   //!
   TBranch        *b_el_true_originbkg;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_topoetcone20;   //!
   TBranch        *b_mu_ptvarcone30;   //!
   TBranch        *b_mu_isTight;   //!
   TBranch        *b_mu_d0sig;   //!
   TBranch        *b_mu_delta_z0_sintheta;   //!
   TBranch        *b_mu_true_type;   //!
   TBranch        *b_mu_true_origin;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_mv2c10;   //!
   TBranch        *b_jet_mv2c20;   //!
   TBranch        *b_jet_jvt;   //!
   TBranch        *b_jet_truthflav;   //!
   TBranch        *b_jet_isbtagged_60;   //!
   TBranch        *b_jet_isbtagged_70;   //!
   TBranch        *b_jet_isbtagged_77;   //!
   TBranch        *b_jet_isbtagged_85;   //!
   TBranch        *b_jet_isbtagged_FlatBEff_30;   //!
   TBranch        *b_jet_isbtagged_FlatBEff_40;   //!
   TBranch        *b_jet_isbtagged_FlatBEff_50;   //!
   TBranch        *b_jet_isbtagged_FlatBEff_60;   //!
   TBranch        *b_jet_isbtagged_FlatBEff_70;   //!
   TBranch        *b_jet_isbtagged_FlatBEff_77;   //!
   TBranch        *b_jet_isbtagged_FlatBEff_85;   //!
   TBranch        *b_jet_isbtagged_30;   //!
   TBranch        *b_jet_isbtagged_50;   //!
   TBranch        *b_jet_isbtagged_80;   //!
   TBranch        *b_jet_isbtagged_90;   //!
   TBranch        *b_jet_tagWeightBin;   //!
   TBranch        *b_ljet_pt;   //!
   TBranch        *b_ljet_eta;   //!
   TBranch        *b_ljet_phi;   //!
   TBranch        *b_ljet_e;   //!
   TBranch        *b_ljet_m;   //!
   TBranch        *b_ljet_sd12;   //!
   TBranch        *b_rcjet_pt;   //!
   TBranch        *b_rcjet_eta;   //!
   TBranch        *b_rcjet_phi;   //!
   TBranch        *b_rcjet_e;   //!
   TBranch        *b_rcjet_d12;   //!
   TBranch        *b_rcjet_d23;   //!
   TBranch        *b_rcjetsub_pt;   //!
   TBranch        *b_rcjetsub_eta;   //!
   TBranch        *b_rcjetsub_phi;   //!
   TBranch        *b_rcjetsub_e;   //!
   TBranch        *b_rcjetsub_mv2c10;   //!
   TBranch        *b_met_met;   //!
   TBranch        *b_met_phi;   //!
   TBranch        *b_boosted_ejets_2015;   //!
   TBranch        *b_boosted_ejets_2016;   //!
   TBranch        *b_boosted_mujets_2015;   //!
   TBranch        *b_boosted_mujets_2016;   //!
   TBranch        *b_ejets_2015;   //!
   TBranch        *b_ejets_2016;   //!
   TBranch        *b_mujets_2015;   //!
   TBranch        *b_mujets_2016;   //!
   TBranch        *b_ee_2015;   //!
   TBranch        *b_ee_2016;   //!
   TBranch        *b_mumu_2015;   //!
   TBranch        *b_mumu_2016;   //!
   TBranch        *b_emu_2015;   //!
   TBranch        *b_emu_2016;   //!
   TBranch        *b_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_HLT_mu26_ivarmedium;   //!
   TBranch        *b_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_HLT_mu50;   //!
   TBranch        *b_HLT_e60_lhmedium;   //!
   TBranch        *b_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_HLT_e120_lhloose;   //!
   TBranch        *b_el_trigMatch_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_el_trigMatch_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_el_trigMatch_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_el_trigMatch_HLT_e60_lhmedium;   //!
   TBranch        *b_el_trigMatch_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_el_trigMatch_HLT_e120_lhloose;   //!
   TBranch        *b_mu_trigMatch_HLT_mu26_ivarmedium;   //!
   TBranch        *b_mu_trigMatch_HLT_mu50;   //!
   TBranch        *b_mu_trigMatch_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_TopHeavyFlavorFilterFlag;   //!
   TBranch        *b_prwHash;   //!
   TBranch        *b_el_true_pdg;   //!
   TBranch        *b_el_true_pt;   //!
   TBranch        *b_el_true_eta;   //!
   TBranch        *b_mu_true_pdg;   //!
   TBranch        *b_mu_true_pt;   //!
   TBranch        *b_mu_true_eta;   //!
   TBranch        *b_jet_truthPartonLabel;   //!
   TBranch        *b_nJets;   //!
   TBranch        *b_nBTags;   //!
   TBranch        *b_nBTags60;   //!
   TBranch        *b_nBTags70;   //!
   TBranch        *b_nBTags77;   //!
   TBranch        *b_nBTags85;   //!
   TBranch        *b_nBTagsFlatBEff_30;   //!
   TBranch        *b_nBTagsFlatBEff_40;   //!
   TBranch        *b_nBTagsFlatBEff_50;   //!
   TBranch        *b_nBTagsFlatBEff_60;   //!
   TBranch        *b_nBTagsFlatBEff_70;   //!
   TBranch        *b_nBTagsFlatBEff_77;   //!
   TBranch        *b_nBTagsFlatBEff_85;   //!
   TBranch        *b_nBTags30;   //!
   TBranch        *b_nBTags50;   //!
   TBranch        *b_nBTags80;   //!
   TBranch        *b_nBTags90;   //!
   TBranch        *b_nPrimaryVtx;   //!
   TBranch        *b_nElectrons;   //!
   TBranch        *b_nMuons;   //!
   TBranch        *b_nHFJets;   //!
   TBranch        *b_HT_jets;   //!
   TBranch        *b_HT_all;   //!
   TBranch        *b_Centrality_all;   //!
   TBranch        *b_Mbb_MindR;   //!
   TBranch        *b_dRbb_MaxPt;   //!
   TBranch        *b_Mjj_MaxPt;   //!
   TBranch        *b_pT_jet5;   //!
   TBranch        *b_H1_all;   //!
   TBranch        *b_dRbb_avg;   //!
   TBranch        *b_Mbj_MaxPt;   //!
   TBranch        *b_dRlepbb_MindR;   //!
   TBranch        *b_Muu_MindR;   //!
   TBranch        *b_Aplan_bjets;   //!
   TBranch        *b_dRuu_MindR;   //!
   TBranch        *b_Mjjj_MaxPt;   //!
   TBranch        *b_Njet_pt40;   //!
   TBranch        *b_Mbj_MindR;   //!
   TBranch        *b_Mjj_MindR;   //!
   TBranch        *b_pTuu_MindR;   //!
   TBranch        *b_Mbb_MaxM;   //!
   TBranch        *b_Mbj_Wmass;   //!
   TBranch        *b_dRbj_Wmass;   //!
   TBranch        *b_tauVetoFlag;   //!
   TBranch        *b_isTrueSL;   //!
   TBranch        *b_isTrueDIL;   //!
   TBranch        *b_dEtajj_MaxdEta;   //!
   TBranch        *b_MHiggs;   //!
   TBranch        *b_dRHl_MindR;   //!
   TBranch        *b_NHiggs_30;   //!
   TBranch        *b_Mjj_MinM;   //!
   TBranch        *b_dRHl_MaxdR;   //!
   TBranch        *b_Mjj_HiggsMass;   //!
   TBranch        *b_dRlj_MindR;   //!
   TBranch        *b_H4_all;   //!
   TBranch        *b_pT_jet3;   //!
   TBranch        *b_dRbb_MaxM;   //!
   TBranch        *b_Aplan_jets;   //!
   //TBranch        *b_Njet_pt40;   //!
   TBranch        *b_Mbb_MaxPt;   //!
   TBranch        *b_nLjets;   //!
   TBranch        *b_HhadT_nJets;   //!
   TBranch        *b_HhadT_nLjets;   //!
   TBranch        *b_FirstLjetPt;   //!
   TBranch        *b_SecondLjetPt;   //!
   TBranch        *b_FirstLjetM;   //!
   TBranch        *b_SecondLjetM;   //!
   TBranch        *b_HT_ljets;   //!
   TBranch        *b_nLjet_m100;   //!
   TBranch        *b_nLjet_m50;   //!
   TBranch        *b_nJetOutsideLjet;   //!
   TBranch        *b_nBjetOutsideLjet;   //!
   TBranch        *b_dRbb_min;   //!
   TBranch        *b_dRjj_min;   //!
   TBranch        *b_HiggsbbM;   //!
   TBranch        *b_HiggsjjM;   //!
   TBranch        *b_ljet_sd23;   //!
   TBranch        *b_ljet_tau21;   //!
   TBranch        *b_ljet_tau32;   //!
   TBranch        *b_ljet_tau21_wta;   //!
   TBranch        *b_ljet_tau32_wta;   //!
   TBranch        *b_ljet_D2;   //!
   TBranch        *b_ljet_C2;   //!
   TBranch        *b_ljet_topTag;   //!
   TBranch        *b_ljet_topTag_loose;   //!
   TBranch        *b_ljet_topTagN;   //!
   TBranch        *b_ljet_topTagN_loose;   //!
   TBranch        *b_ljet_truthmatch;   //!
   TBranch        *b_jet_truthmatch;   //!
   TBranch        *b_truth_jet_pt;   //!
   TBranch        *b_truth_jet_eta;   //!
   TBranch        *b_truth_jet_phi;   //!
   TBranch        *b_truth_jet_m;   //!
   TBranch        *b_truth_pt;   //!
   TBranch        *b_truth_eta;   //!
   TBranch        *b_truth_phi;   //!
   TBranch        *b_truth_m;   //!
   TBranch        *b_truth_pdgid;   //!
   TBranch        *b_truth_status;   //!
   TBranch        *b_truth_tthbb_info;   //!
   TBranch        *b_truth_nHiggs;   //!
   TBranch        *b_truth_nTop;   //!
   TBranch        *b_truth_nLepTop;   //!
   TBranch        *b_truth_nVectorBoson;   //!
   TBranch        *b_truth_ttbar_pt;   //!
   TBranch        *b_truth_top_pt;   //!
   TBranch        *b_truth_higgs_eta;   //!
   TBranch        *b_truth_tbar_pt;   //!
   TBranch        *b_truth_HDecay;   //!
   TBranch        *b_truth_top_dilep_filter;   //!
   TBranch        *b_semilepMVAreco_higgs_mass;   //!
   TBranch        *b_semilepMVAreco_bbhiggs_dR;   //!
   TBranch        *b_semilepMVAreco_higgsbleptop_mass;   //!
   TBranch        *b_semilepMVAreco_higgsleptop_dR;   //!
   TBranch        *b_semilepMVAreco_higgslep_dR;   //!
   TBranch        *b_semilepMVAreco_leptophadtop_dR;   //!
   TBranch        *b_semilepMVAreco_higgsq1hadW_mass;   //!
   TBranch        *b_semilepMVAreco_hadWb1Higgs_mass;   //!
   TBranch        *b_semilepMVAreco_b1higgsbhadtop_dR;   //!
   TBranch        *b_semilepMVAreco_higgsbleptop_withH_dR;   //!
   TBranch        *b_semilepMVAreco_higgsttbar_withH_dR;   //!
   TBranch        *b_semilepMVAreco_higgsbhadtop_withH_dR;   //!
   TBranch        *b_semilepMVAreco_leptophadtop_withH_dR;   //!
   TBranch        *b_semilepMVAreco_ttH_Ht_withH;   //!
   TBranch        *b_semilepMVAreco_BDT_output;   //!
   TBranch        *b_semilepMVAreco_BDT_withH_output;   //!
   TBranch        *b_semilepMVAreco_BDT_output_truthMatchPattern;   //!
   TBranch        *b_semilepMVAreco_BDT_withH_output_truthMatchPattern;   //!
   TBranch        *b_semilepMVAreco_Ncombinations;   //!
   TBranch        *b_semilepMVAreco_nuApprox_recoBDT;   //!
   TBranch        *b_semilepMVAreco_nuApprox_recoBDT_withH;   //!
   TBranch        *b_jet_semilepMVAreco_recoBDT_cand;   //!
   TBranch        *b_jet_semilepMVAreco_recoBDT_withH_cand;   //!
   TBranch        *b_semilepMVAreco_BDT_output_6jsplit;   //!
   TBranch        *b_semilepMVAreco_BDT_withH_output_6jsplit;   //!
   TBranch        *b_semilepMVAreco_nuApprox_recoBDT_6jsplit;   //!
   TBranch        *b_semilepMVAreco_nuApprox_recoBDT_withH_6jsplit;   //!
   TBranch        *b_jet_semilepMVAreco_recoBDT_cand_6jsplit;   //!
   TBranch        *b_jet_semilepMVAreco_recoBDT_withH_cand_6jsplit;   //!
   TBranch        *b_ClassifBDTOutput_basic;   //!
   TBranch        *b_ClassifBDTOutput_withReco_basic;   //!
   TBranch        *b_ClassifBDTOutput_6jsplit;   //!
   TBranch        *b_ClassifBDTOutput_withReco_6jsplit;   //!
   TBranch        *b_Zjets_Systematic_ckkw15;   //!
   TBranch        *b_Zjets_Systematic_ckkw30;   //!
   TBranch        *b_Zjets_Systematic_fac025;   //!
   TBranch        *b_Zjets_Systematic_fac4;   //!
   TBranch        *b_Zjets_Systematic_renorm025;   //!
   TBranch        *b_Zjets_Systematic_renorm4;   //!
   TBranch        *b_Zjets_Systematic_qsf025;   //!
   TBranch        *b_Zjets_Systematic_qsf4;   //!
   TBranch        *b_NBFricoNN_ljets;   //!
   TBranch        *b_NBFricoNN_dil;   //!
   TBranch        *b_ClassifHPLUS_Semilep_HF_BDT200_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_HF_BDT225_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_HF_BDT250_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_HF_BDT275_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_HF_BDT300_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_HF_BDT350_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_HF_BDT400_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_HF_BDT500_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT1000_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT2000_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT200_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT225_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT250_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT275_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT300_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT350_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT400_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT500_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT600_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT700_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT800_Output;   //!
   TBranch        *b_ClassifHPLUS_Semilep_INC_BDT900_Output;   //!
   TBranch        *b_el_LHMedium;   //!
   TBranch        *b_el_LHTight;   //!
   TBranch        *b_el_isoGradient;   //!
   TBranch        *b_el_isoGradientLoose;   //!
   TBranch        *b_el_isoTight;   //!
   TBranch        *b_el_isoLoose;   //!
   TBranch        *b_el_isoLooseTrackOnly;   //!
   TBranch        *b_el_isoFixedCutTight;   //!
   TBranch        *b_el_isoFixedCutTightTrackOnly;   //!
   TBranch        *b_el_isoFixedCutLoose;   //!
   TBranch        *b_mu_Tight;   //!
   TBranch        *b_mu_Medium;   //!
   TBranch        *b_mu_isoGradient;   //!
   TBranch        *b_mu_isoGradientLoose;   //!
   TBranch        *b_mu_isoTight;   //!
   TBranch        *b_mu_isoLoose;   //!
   TBranch        *b_mu_isoLooseTrackOnly;   //!
   TBranch        *b_mu_isoFixedCutTightTrackOnly;   //!
   TBranch        *b_mu_isoFixedCutLoose;   //!
   TBranch        *b_HF_Classification;   //!
   TBranch        *b_HF_SimpleClassification;   //!
   TBranch        *b_q1_pt;   //!
   TBranch        *b_q1_eta;   //!
   TBranch        *b_q1_phi;   //!
   TBranch        *b_q1_m;   //!
   TBranch        *b_q2_pt;   //!
   TBranch        *b_q2_eta;   //!
   TBranch        *b_q2_phi;   //!
   TBranch        *b_q2_m;   //!
   TBranch        *b_qq_pt;   //!
   TBranch        *b_qq_ht;   //!
   TBranch        *b_qq_dr;   //!
   TBranch        *b_qq_m;   //!
   TBranch        *b_nTruthJets15;   //!
   TBranch        *b_nTruthJets20;   //!
   TBranch        *b_nTruthJets25;   //!
   TBranch        *b_nTruthJets25W;   //!
   TBranch        *b_nTruthJets50;   //!
   TBranch        *b_weight_ttbb_Nominal;   //!
   TBranch        *b_weight_ttbb_CSS_KIN;   //!
   TBranch        *b_weight_ttbb_NNPDF;   //!
   TBranch        *b_weight_ttbb_MSTW;   //!
   TBranch        *b_weight_ttbb_Q_CMMPS;   //!
   TBranch        *b_weight_ttbb_glosoft;   //!
   TBranch        *b_weight_ttbb_defaultX05;   //!
   TBranch        *b_weight_ttbb_defaultX2;   //!
   TBranch        *b_weight_ttbb_MPIup;   //!
   TBranch        *b_weight_ttbb_MPIdown;   //!
   TBranch        *b_weight_ttbb_MPIfactor;   //!
   TBranch        *b_weight_ttbb_aMcAtNloHpp;   //!
   TBranch        *b_weight_ttbb_aMcAtNloPy8;   //!
   TBranch        *b_weight_ttbar_FracRw;   //!
   TBranch        *b_ttHF_mva_discriminant;   //!

   TBranch        *b_mu_original_xAOD;                                   // specific to data
   TBranch        *b_boosted_ejets_2015_Loose;
   TBranch        *b_boosted_ejets_2016_Loose;
   TBranch        *b_boosted_mujets_2015_Loose;
   TBranch        *b_boosted_mujets_2016_Loose;
   TBranch        *b_ejets_2015_Loose;
   TBranch        *b_ejets_2016_Loose;
   TBranch        *b_mujets_2015_Loose;
   TBranch        *b_mujets_2016_Loose;
   TBranch        *b_HLT_mu24;
   TBranch        *b_HLT_e24_lhmedium_nod0_L1EM18VH;
   TBranch        *b_el_trigMatch_HLT_e24_lhmedium_nod0_L1EM18VH;
   TBranch        *b_mu_trigMatch_HLT_mu24;
   TBranch        *b_PS_HLT_mu24;
   TBranch        *b_PS_HLT_e120_lhloose;
   TBranch        *b_PS_HLT_e24_lhmedium_L1EM20VH;
   TBranch        *b_PS_HLT_mu50;
   TBranch        *b_PS_HLT_e140_lhloose_nod0;
   TBranch        *b_PS_HLT_e60_lhmedium;
   TBranch        *b_PS_HLT_e26_lhtight_nod0_ivarloose;
   TBranch        *b_PS_HLT_mu26_ivarmedium;
   TBranch        *b_PS_HLT_e60_lhmedium_nod0;
   TBranch        *b_PS_HLT_e24_lhmedium_nod0_L1EM18VH;
   TBranch        *b_PS_HLT_mu20_iloose_L1MU15;



   cutFlow(TTree *tree=0);
   virtual ~cutFlow();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     ReadInputTreeLeaf(TTree *tree);
   virtual void     InitHisto();
   virtual void     InitTreeForTMVA();
   virtual void     InitTreeForFit();
   virtual void     InitTMVAReader();
   virtual void     FillHisto(TString selection, double evtwgt);
   virtual void     FillCutflowBoosted();
   virtual void     FillCutflowResolved();
   virtual void     FillVarForTMVA(TString selection);
   virtual void     Loop(TString prod, TString sample, TString treeName,// prod ex : tpelzer, sboutle | sample ex : ttbartest
                       bool choose2016, bool choose2015, bool chooseElchannel, bool chooseMuchannel,
                       bool chooseBoosted, bool chooseResolved,
                       bool saveHistos = false, bool saveBoostedCutflow = false, bool saveResolvedCutflow = false,
                       bool saveTreeForTMVA = false, bool saveTreeForFit = false
                    );
   virtual void     FinalizeHisto();
   virtual void     FinalizeTMVATree();
   virtual void     FinalizeFitTree();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   virtual void     mcProcessNormalisation(TString prod);
   virtual void     branchAddress(TTree *tree, TString prod, TString treeName, TString process);
   virtual void     mcCrossSections();
};

#endif

#ifdef cutFlow_cxx
cutFlow::cutFlow(TTree *tree) : fChain(0) 
{
   fDataLumi = 36.47;          cout << "data luminosity set to " << fDataLumi << " fb-1" << endl;
   fBoosted = true;          cout << "Boosted selection applied " << fBoosted << endl;
   fttbarb = true;          cout << "use ttbarb sample ? " << fttbarb << endl;
   fFirstBinCutflow = true;   cout << "dataset # of events = 1st bin cutflow ? " << fFirstBinCutflow << endl;

   mcCrossSections();

   // ____ initialise selection to 0 please _____

   fselection["preselection"] = 0; 
   fselection["resolved"] = 0; 
   fselection["boosted3211"] = 0; 
   //fselection["0ji0bi1te"] = 0;     // preselection exactly 1 loose tJ, 1 lepton
   //fselection["0ji0bi2ti"] = 0;     // preselection at least 2 loose tJ, 1 lepton
   //fselection["1te1le"] = 0; // exactly 1 top-jet
   //fselection["1te1el"] = 0; // exactly 1 top-jet el channel
   //fselection["1te1mu"] = 0; // exactly 1 top-jet mu channel
   //fselection["1ti1le"] = 0; // at least 1 top-jet
   //fselection["1ti1el"] = 0; // at least 1 top-jet el channel
   //fselection["1ti1mu"] = 0; // at least 1 top-jet mu channel
   //fselection["3ji1bi1te1le"] = 0;   // 3j, 1bj oustides lse t-jets, exactly 1t-jet, 1 lept
   //fselection["3ji1bi1te1el"] = 0;   // 3j, 1bj oustides lse t-jets, exactly 1t-jet, 1 elec
   //fselection["3ji1bi1te1mu"] = 0;   // 3j, 1bj oustides lse t-jets, exactly 1t-jet, 1 muon
   //fselection["3ji1bi1ti1le"] = 0;   // 3j, 1bj oustides lse t-jets, 1t-jet, 1 lepton
   //fselection["3ji1bi1ti1el"] = 0;   // 3j, 1bj oustides lse t-jets, 1t-jet, 1 electron
   //fselection["3ji1bi1ti1mu"] = 0;   // 3j, 1bj oustides lse t-jets, 1t-jet, 1 muon
   //fselection["3ji2bi1ti1le"] = 0; // signal region 1
   //fselection["3ji2bi1ti1el"] = 0; // signal region 1 e-channel
   //fselection["3ji2bi1ti1mu"] = 0; // signal region 1 mu-channel
   //fselection["3ji2bi1te1le"] = 0; // signal region 1 exactly 1 top-jet
   //fselection["3ji2bi1te1el"] = 0; // signal region 1 e-channel exactly 1 top-jet
   //fselection["3ji2bi1te1mu"] = 0; // signal region 1 mu-channel  exactly 1 top-jet
   //fselection["3ji3bi1ti1le"] = 0; // signal region 2
   //fselection["3ji3bi1ti1el"] = 0; // signal region 2 e-channel
   //fselection["3ji3bi1ti1mu"] = 0; // signal region 2 mu-channel
   //fselection["3ji3bi1te1le"] = 0; // signal region 2 exactly 1 top-jet
   //fselection["3ji3bi1te1el"] = 0; // signal region 2 e-channel exactly 1 top-jet
   //fselection["3ji3bi1te1mu"] = 0; // signal region 2 mu-channel exactly 1 top-jet
      // _____ tighest top-jet, exclusive _____
   //fselection["3ji2bi1tte"] = 0;     // 3j, 2bj oustides tight t-jets or loose (if no tight) or max pT if several t-taggs, 1t-jet, 1 lepton
   //fselection["3ji3bi1tte"] = 0;     // 3j, 2bj oustides tight t-jets or loose (if no tight) or max pT if several t-taggs, 1t-jet, 1 lepton
   //fselection["re4ji2bi"] = 0;      // resolved 4j, 2bj, 1 lepton
   //fselection["re4ji2biel"] = 0;    // resolved 4j, 2bj, 1 electron
   //fselection["re4ji2bimu"] = 0;    // resolved 4j, 2bj, 1 muon
        
        
   // _____ SELECTIONS : welcome to 2016 !  /////
   //fselection["1J2ib70iAnd1Jtl1ib70eOrttOutOfJ2ib70i"] = 0;
   //fselection["1J2ib70iAnd1Jtl1ib70eOrttOutOfJ2ib70i_0ob70e"] = 0; // == 0 b-tag outside 2 large-R jets
   //fselection["1J2ib70iAnd1Jtl1ib70eOrttOutOfJ2ib70i_1ob70i"] = 0; // >= 1 b-tag outside 2 large-R jets
   //fselection["1J2ib70iAnd1Jtl1ib70eOutOfJ2ib70i"] = 0;
   //fselection["1J2ib70iAnd1Jtl1ib70eOutOfJ2ib70i_0ob70e"] = 0;
   //fselection["1J2ib70iAnd1Jtl1ib70eOutOfJ2ib70i_1ob70i"] = 0;
   //fselection["1J2ib70iAnd1Jtl1ib70e1inb70iOutOfJ2ib70i"] = 0;
   //fselection["1J2ib70iAnd1Jtl1ib70e1inb70iOutOfJ2ib70i_0ob70e"] = 0;
   //fselection["1J2ib70iAnd1Jtl1ib70e1inb70iOutOfJ2ib70i_1ob70i"] = 0;
   //fselection["0J2ib70i1J1ib70em1001Jm140OutOfJ1ib70em100"] = 0;
   //fselection["0J2ib70i1J1ib70em1001Jm140OutOfJ1ib70em100_0ob70e"] = 0;
   //fselection["0J2ib70i1J1ib70em1001Jm140OutOfJ1ib70em100_1ob70i"] = 0;

   //fselection["1J2ib77iAnd1Jtl1ib77eOutOfJ2ib77i"] = 0;
   //fselection["1J2ib77iAnd1Jtl1ib77eOutOfJ2ib77i_0ob77e"] = 0;
   //fselection["1J2ib77iAnd1Jtl1ib77eOutOfJ2ib77i_1ob77i"] = 0;

   fselection["1iJtl1ib85iPt250"] = 0;
   fselection["1iJtl1ib85iPt250_1ab85i"] = 0;

   //fselection["1iJ1ijiPt250"] = 0;
   fselection["1iJtl1ib70e1inb70iPt250_3aji2ab70i"] = 0;
   fselection["1iJtl1ib70e1inb70iPt250_3aji3ab70i"] = 0;

   //fselection["1iJPt250"] = 0;

   //fselection["1iJ2ib77iPt200"] = 0;
   //fselection["1iJ2ib77iPt200_1iJtl1ib77ePt250"] = 0;
   //fselection["1iJ2ib77iPt200_1iJtl1ib77ePt250_0ab77e"] = 0;
   //fselection["1iJ2ib77iPt200_1iJtl1ib77ePt250_1ab77i"] = 0;

   //fselection["1iJ2ib77iPt250"] = 0; // >= 1 H-tag (J pT > 250 GeV, >= 2 inside b-tag 77% WP)
   //fselection["1iJ2ib77iPt250_1iJtl1ib77ePt250"] = 0; // >= 1 H-tag ("") >= 1 top-tag (J pT > 250 GeV, top loose,  = 1 inside b-tag 77% WP)
   //fselection["1iJ2ib77iPt250_1iJtl1ib77ePt250_0ab77e"] = 0; // >= 1 H-tag ("") >= 1 top-tag (""), = 0 additional b-tag
   //fselection["1iJ2ib77iPt250_1iJtl1ib77ePt250_1ab77i"] = 0; // >= 1 H-tag ("") >= 1 top-tag (""), >= 1 additional b-tag

   fselection["1iJ2ib85iPt200"] = 0;
   fselection["1iJ2ib85iPt200_1iJtl1ib85iPt250"] = 0;
   fselection["1iJ2ib85iPt200_1iJtl1ib85iPt250_0ab85e"] = 0;
   fselection["1iJ2ib85iPt200_1iJtl1ib85iPt250_1ab85i"] = 0;
   

   fnselections = fselection.size();

}

cutFlow::~cutFlow()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();

}

Int_t cutFlow::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t cutFlow::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void cutFlow::InitTreeForTMVA() { // method called at each new Loop() method

   for ( map<TString, bool>::iterator it = fselection.begin(); it != fselection.end(); ++it) {
        
     // create folders for output ttree
     TString ntupleProdFolder = "ntuplesForTMVA/"+fProd; // create folder for a given prod
     if (gSystem->Exec("test -d "+ntupleProdFolder)) gSystem->Exec("mkdir -p "+ntupleProdFolder);

     // folder for ttre with boosted selection
     TString ntupleSelFolder = ntupleProdFolder + "/" + it->first;
     if (gSystem->Exec("test -d "+ ntupleSelFolder)) gSystem->Exec("mkdir -p "+ntupleSelFolder); // create folder for each selection

     fFileOutTree[it->first] = new TFile(ntupleSelFolder+"/myTrees_"+fProcess+".root", "RECREATE") ;
     //fFileOutTree[it->first]->cd();
     // _____ create the trees _____
     //TString treename = fProcess+"_evtSel_"+it->first;
     //m_treeForTMVA.insert(make_pair (it->first, new TTree(treename, treename)) );
     m_treeForTMVA[it->first] = new TTree(fTreeName, fTreeName);

     // _____ let the branches grow ____

     m_treeForTMVA[it->first]->Branch("Htag_pt",           &Htag_pt_tr,              "Htag_pt/F"          );
     m_treeForTMVA[it->first]->Branch("Htag_eta",          &Htag_eta_tr,             "Htag_eta/F"         );
     m_treeForTMVA[it->first]->Branch("Htag_phi",          &Htag_phi_tr,             "Htag_phi/F"         );
     m_treeForTMVA[it->first]->Branch("Htag_m",            &Htag_m_tr,               "Htag_m/F"           );
     m_treeForTMVA[it->first]->Branch("Htag_dRlep",        &Htag_dRlep_tr,           "Htag_dRlep/F"       );
     m_treeForTMVA[it->first]->Branch("Htag_nib70",        &Htag_nib70_tr,           "Htag_nib70/F"       );
     m_treeForTMVA[it->first]->Branch("Htag_ninb70",       &Htag_ninb70_tr,          "Htag_ninb70/F"      );
     m_treeForTMVA[it->first]->Branch("Htag_nib77",        &Htag_nib77_tr,           "Htag_nib77/F"       );
     m_treeForTMVA[it->first]->Branch("Htag_ninb77",       &Htag_ninb77_tr,          "Htag_ninb77/F"      );
     m_treeForTMVA[it->first]->Branch("Htag_nib85",        &Htag_nib85_tr,           "Htag_nib85/F"       );
     m_treeForTMVA[it->first]->Branch("Htag_ninb85",       &Htag_ninb85_tr,          "Htag_ninb85/F"      );
     m_treeForTMVA[it->first]->Branch("Htag_defTopTag",    &Htag_defTopTag_tr,       "Htag_defTopTag/F"   );
     m_treeForTMVA[it->first]->Branch("Htag_D2",           &Htag_D2_tr,              "Htag_D2/F"          );
     m_treeForTMVA[it->first]->Branch("Htag_C2",           &Htag_C2_tr,              "Htag_C2/F"          );
     m_treeForTMVA[it->first]->Branch("Htag_tau21_wta",    &Htag_tau21_wta_tr,       "Htag_tau21_wta/F"   );
     m_treeForTMVA[it->first]->Branch("Htag_tau32_wta",    &Htag_tau32_wta_tr,       "Htag_tau32_wta/F"   );
     m_treeForTMVA[it->first]->Branch("Htag_sumPseudoMv2c10ib70",&Htag_sumPseudoMv2c10ib70_tr,"Htag_sumPseudoMv2c10ib70/F");
     m_treeForTMVA[it->first]->Branch("Htag_sumPseudoMv2c10ib77",&Htag_sumPseudoMv2c10ib77_tr,"Htag_sumPseudoMv2c10ib77/F");
     m_treeForTMVA[it->first]->Branch("Htag_sumPseudoMv2c10ib85",&Htag_sumPseudoMv2c10ib85_tr,"Htag_sumPseudoMv2c10ib85/F");

     m_treeForTMVA[it->first]->Branch("ttag_pt",           &ttag_pt_tr,              "ttag_pt/F"          );
     m_treeForTMVA[it->first]->Branch("ttag_eta",          &ttag_eta_tr,             "ttag_eta/F"         );
     m_treeForTMVA[it->first]->Branch("ttag_phi",          &ttag_phi_tr,             "ttag_phi/F"         );
     m_treeForTMVA[it->first]->Branch("ttag_m",            &ttag_m_tr,               "ttag_m/F"           );
     m_treeForTMVA[it->first]->Branch("ttag_dRlep",        &ttag_dRlep_tr,           "ttag_dRlep/F"       );
     m_treeForTMVA[it->first]->Branch("ttag_nib70",        &ttag_nib70_tr,           "ttag_nib70/F"       );
     m_treeForTMVA[it->first]->Branch("ttag_ninb70",       &ttag_ninb70_tr,          "ttag_ninb70/F"      );
     m_treeForTMVA[it->first]->Branch("ttag_nib77",        &ttag_nib77_tr,           "ttag_nib77/F"       );
     m_treeForTMVA[it->first]->Branch("ttag_ninb77",       &ttag_ninb77_tr,          "ttag_ninb77/F"      );
     m_treeForTMVA[it->first]->Branch("ttag_nib85",        &ttag_nib85_tr,           "ttag_nib85/F"       );
     m_treeForTMVA[it->first]->Branch("ttag_ninb85",       &ttag_ninb85_tr,          "ttag_ninb85/F"      );
     m_treeForTMVA[it->first]->Branch("ttag_defTopTag",    &ttag_defTopTag_tr,       "ttag_defTopTag/F"   );
     m_treeForTMVA[it->first]->Branch("ttag_D2",           &ttag_D2_tr,              "ttag_D2/F"          );
     m_treeForTMVA[it->first]->Branch("ttag_C2",           &ttag_C2_tr,              "ttag_C2/F"          );
     m_treeForTMVA[it->first]->Branch("ttag_tau21_wta",    &ttag_tau21_wta_tr,       "ttag_tau21_wta/F"   );
     m_treeForTMVA[it->first]->Branch("ttag_tau32_wta",    &ttag_tau32_wta_tr,       "ttag_tau32_wta/F"   );
     m_treeForTMVA[it->first]->Branch("ttag_sumPseudoMv2c10ib70",&ttag_sumPseudoMv2c10ib70_tr,"ttag_sumPseudoMv2c10ib70/F");
     m_treeForTMVA[it->first]->Branch("ttag_sumPseudoMv2c10ib77",&ttag_sumPseudoMv2c10ib77_tr,"ttag_sumPseudoMv2c10ib77/F");
     m_treeForTMVA[it->first]->Branch("ttag_sumPseudoMv2c10ib85",&ttag_sumPseudoMv2c10ib85_tr,"ttag_sumPseudoMv2c10ib85/F");

     m_treeForTMVA[it->first]->Branch("sumMv2c10_ob70_tNHtag", &sumMv2c10_ob70_tNHtag_tr, "sumMv2c10_ob70_tNHtag/F" );
     m_treeForTMVA[it->first]->Branch("sumMv2c10_ob77_tNHtag", &sumMv2c10_ob77_tNHtag_tr, "sumMv2c10_ob77_tNHtag/F" );
     m_treeForTMVA[it->first]->Branch("sumMv2c10_ob85_tNHtag", &sumMv2c10_ob85_tNHtag_tr, "sumMv2c10_ob85_tNHtag/F" );
     m_treeForTMVA[it->first]->Branch("nob70_tNHtag"   ,  &nob70_tNHtag_tr,   "nob70_tNHtag/F" );
     m_treeForTMVA[it->first]->Branch("nob77_tNHtag"   ,  &nob77_tNHtag_tr,   "nob77_tNHtag/F" );
     m_treeForTMVA[it->first]->Branch("nob85_tNHtag"   ,  &nob85_tNHtag_tr,   "nob85_tNHtag/F" );
     m_treeForTMVA[it->first]->Branch("noj_tNHtag"     ,  &noj_tNHtag_tr  ,   "noj_tNHtag/F"   );
     m_treeForTMVA[it->first]->Branch("nb70"           ,  &nb70_tr,           "nb70/F"         );
     m_treeForTMVA[it->first]->Branch("nb77"           ,  &nb77_tr,           "nb77/F"         );
     m_treeForTMVA[it->first]->Branch("nb85"           ,  &nb85_tr,           "nb85/F"         );
     m_treeForTMVA[it->first]->Branch("nj"             ,  &nj_tr  ,           "nj/F"           );
     m_treeForTMVA[it->first]->Branch("mu_channel"     ,  &leptonflavour,     "mu_channel/F"   );
     m_treeForTMVA[it->first]->Branch("dR_ttagHtag"    ,  &dR_ttagHtag_tr ,   "dR_ttagHtag/F"  );
     m_treeForTMVA[it->first]->Branch("HhadT_jets"     ,  &HT_jets     ,   "HhadT_jets/F"   );
     m_treeForTMVA[it->first]->Branch("HT_all"         ,  &HT_all      ,   "HT_all/F"       );
     m_treeForTMVA[it->first]->Branch("weight_allb70"  ,  &evtwgtb70,  "weight_allb70/F");
     m_treeForTMVA[it->first]->Branch("weight_allb77"  ,  &evtwgtb77,  "weight_allb77/F");
     m_treeForTMVA[it->first]->Branch("weight_allb85"  ,  &evtwgtb85,  "weight_allb85/F");
     m_treeForTMVA[it->first]->Branch("FinalWeight"    ,  &selFinalWeight_tr, "selFinalWeight/F");
     m_treeForTMVA[it->first]->Branch("ClassifBDTOutput_withReco_basic", &resolvedBDT_withReco_tr, "ClassifBDTOutput_withReco_basic/F");

     m_treeForTMVA[it->first]->Branch("Pass_4JE2BE",  &Pass_4JE2BE_tr,  "Pass_4JE2BE/I");
     m_treeForTMVA[it->first]->Branch("Pass_4JE3BE",  &Pass_4JE3BE_tr,  "Pass_4JE3BE/I");
     m_treeForTMVA[it->first]->Branch("Pass_4JE4BI",  &Pass_4JE4BI_tr,  "Pass_4JE4BI/I");
     m_treeForTMVA[it->first]->Branch("Pass_5JE2BE",  &Pass_5JE2BE_tr,  "Pass_5JE2BE/I");
     m_treeForTMVA[it->first]->Branch("Pass_5JE3BE",  &Pass_5JE3BE_tr,  "Pass_5JE3BE/I");
     m_treeForTMVA[it->first]->Branch("Pass_5JE4BI",  &Pass_5JE4BI_tr,  "Pass_5JE4BI/I");
     m_treeForTMVA[it->first]->Branch("Pass_6JI2BE",  &Pass_6JI2BE_tr,  "Pass_6JI2BE/I");
     m_treeForTMVA[it->first]->Branch("Pass_6JI3BE",  &Pass_6JI3BE_tr,  "Pass_6JI3BE/I");
     m_treeForTMVA[it->first]->Branch("Pass_6JI4BI",  &Pass_6JI4BI_tr,  "Pass_6JI4BI/I");


     TString ntupleReProdFolder = "ntuples_ResolvedMinusSel/"+fProd; // create folder for a given prod
     if (gSystem->Exec("test -d "+ntupleReProdFolder)) gSystem->Exec("mkdir -p "+ntupleReProdFolder);

     //folder for ttree with resolved minus boosted selection
     TString ntupleReSelFolder = ntupleReProdFolder + "/" + it->first;
     if (gSystem->Exec("test -d "+ ntupleReSelFolder)) gSystem->Exec("mkdir -p "+ntupleReSelFolder); // create folder for each selection

     fFileOutResolvedMinusSel[it->first] = new TFile(ntupleReSelFolder+"/myTrees_"+fProcess+".root", "RECREATE") ;
     //fFileOutResolvedMinusSel[it->first]->cd();
     //TString treename2 = fProcess+"_resolved_m_evtSel_"+it->first;
     m_treeResolvedMinusSel[it->first] = new TTree(fTreeName, fTreeName);

     m_treeResolvedMinusSel[it->first]->Branch("FinalWeight",  &evtwgtb70,  "FinalWeight/F");
     m_treeResolvedMinusSel[it->first]->Branch("ClassifBDTOutput_withReco_basic",  &resolvedBDT_withReco_tr,  "ClassifBDTOutput_withReco_basic/F");
     m_treeResolvedMinusSel[it->first]->Branch("HhadT_jets",   &HT_jets,         "HhadT_jets/F");
     m_treeResolvedMinusSel[it->first]->Branch("mu_channel",   &leptonflavour,   "mu_channel/F");
     m_treeResolvedMinusSel[it->first]->Branch("Pass_4JE2BE",  &Pass_4JE2BE_tr,  "Pass_4JE2BE/I");
     m_treeResolvedMinusSel[it->first]->Branch("Pass_4JE3BE",  &Pass_4JE3BE_tr,  "Pass_4JE3BE/I");
     m_treeResolvedMinusSel[it->first]->Branch("Pass_4JE4BI",  &Pass_4JE4BI_tr,  "Pass_4JE4BI/I");
     m_treeResolvedMinusSel[it->first]->Branch("Pass_5JE2BE",  &Pass_5JE2BE_tr,  "Pass_5JE2BE/I");
     m_treeResolvedMinusSel[it->first]->Branch("Pass_5JE3BE",  &Pass_5JE3BE_tr,  "Pass_5JE3BE/I");
     m_treeResolvedMinusSel[it->first]->Branch("Pass_5JE4BI",  &Pass_5JE4BI_tr,  "Pass_5JE4BI/I");
     m_treeResolvedMinusSel[it->first]->Branch("Pass_6JI2BE",  &Pass_6JI2BE_tr,  "Pass_6JI2BE/I");
     m_treeResolvedMinusSel[it->first]->Branch("Pass_6JI3BE",  &Pass_6JI3BE_tr,  "Pass_6JI3BE/I");
     m_treeResolvedMinusSel[it->first]->Branch("Pass_6JI4BI",  &Pass_6JI4BI_tr,  "Pass_6JI4BI/I");

   } //for it

}

void cutFlow::InitTreeForFit() { // method called at each new Loop() method

   InitTMVAReader();
   ///////////////////////////////////////////////////////////////
   //       TREES WITH REGION FLAG AND FITTED VARIABLES         //
   ///////////////////////////////////////////////////////////////
   TString ntupleFitProdFolder = "ntuplesForFit/"+fProd; // create folder for a given prod
   if (gSystem->Exec("test -d "+ntupleFitProdFolder)) gSystem->Exec("mkdir -p "+ntupleFitProdFolder);

   // create folder for each tree
   TString ntupleFitTreeNameFolder = ntupleFitProdFolder + "/" + fTreeName;
   if (gSystem->Exec("test -d "+ ntupleFitTreeNameFolder)) gSystem->Exec("mkdir -p "+ntupleFitTreeNameFolder); 

   fFileOutFitVarRegion = new TFile(ntupleFitTreeNameFolder+"/myTrees_"+fProcess+".root", "RECREATE") ;
   m_treeFitVarRegion = new TTree(fTreeName, "");
   m_treeFitVarRegion->SetAutoSave(30000000000);

   m_treeFitVarRegion->Branch("weight_normalise",     &lumiDatasetMinus1,  "weight_normalise/F");
   m_treeFitVarRegion->Branch("weight_mc",            &weight_mc,          "weight_mc/F");
   m_treeFitVarRegion->Branch("weight_pileup",        &weight_pileup,      "weight_pileup/F");
   m_treeFitVarRegion->Branch("weight_jvt",           &weight_jvt,         "weight_jvt/F");
   m_treeFitVarRegion->Branch("weight_leptonSF",      &weight_leptonSF,    "weight_leptonSF/F");
   m_treeFitVarRegion->Branch("weight_ttbb_Nominal",  &weight_ttbb_Nominal,"weight_ttbb_Nominal/F");
   m_treeFitVarRegion->Branch("weight_bTagSF_70",     &weight_bTagSF_70,   "weight_bTagSF_70/F");
   m_treeFitVarRegion->Branch("weight_bTagSF_77",     &weight_bTagSF_77,   "weight_bTagSF_77/F");
   m_treeFitVarRegion->Branch("weight_bTagSF_85",     &weight_bTagSF_85,   "weight_bTagSF_85/F");
   m_treeFitVarRegion->Branch("weight_bTagSF_Continuous",  &weight_bTagSF_Continuous,  "weight_bTagSF_Continuous/F");
   m_treeFitVarRegion->Branch("ClassifBDTOutput_withReco_basic",  &resolvedBDT_withReco_tr,  "ClassifBDTOutput_withReco_basic/F");
   m_treeFitVarRegion->Branch("HhadT_jets",   &HT_jets,         "HhadT_jets/F");
   m_treeFitVarRegion->Branch("HF_SimpleClassification",   &HF_SimpleClassification,   "HF_SimpleClassification/I");
   m_treeFitVarRegion->Branch("mu_channel",   &leptonflavour,   "mu_channel/I");
   m_treeFitVarRegion->Branch("boosted",      &isBoosted,       "boosted/B");
   m_treeFitVarRegion->Branch("resolved",     &isResolved,      "resolved/B");
   m_treeFitVarRegion->Branch("year2016",     &is2016,          "year2016/B");
   m_treeFitVarRegion->Branch("Pass_4JE2BE",  &Pass_4JE2BE_tr,  "Pass_4JE2BE/I");
   m_treeFitVarRegion->Branch("Pass_4JE3BE",  &Pass_4JE3BE_tr,  "Pass_4JE3BE/I");
   m_treeFitVarRegion->Branch("Pass_4JE4BI",  &Pass_4JE4BI_tr,  "Pass_4JE4BI/I");
   m_treeFitVarRegion->Branch("Pass_5JE2BE",  &Pass_5JE2BE_tr,  "Pass_5JE2BE/I");
   m_treeFitVarRegion->Branch("Pass_5JE3BE",  &Pass_5JE3BE_tr,  "Pass_5JE3BE/I");
   m_treeFitVarRegion->Branch("Pass_5JE4BI",  &Pass_5JE4BI_tr,  "Pass_5JE4BI/I");
   m_treeFitVarRegion->Branch("Pass_6JI2BE",  &Pass_6JI2BE_tr,  "Pass_6JI2BE/I");
   m_treeFitVarRegion->Branch("Pass_6JI3BE",  &Pass_6JI3BE_tr,  "Pass_6JI3BE/I");
   m_treeFitVarRegion->Branch("Pass_6JI4BI",  &Pass_6JI4BI_tr,  "Pass_6JI4BI/I");

   for ( map<TString, bool>::iterator it = fselection.begin(); it != fselection.end(); ++it) {
      m_treeFitVarRegion->Branch("Pass_"+it->first,  &(fselection[it->first]), it->first+"/B");
      if ( m_tmvaReader.find(it->first) != m_tmvaReader.end() ) { // if tmva reader exist for this selection
         m_treeFitVarRegion->Branch("BDT_"+it->first,  &(m_tmvaReader_output[it->first]), "BDT_"+it->first+"/F");
      }
   } // for it 

   if ( fTreeName == "nominal_Loose" ) {


      m_treeFitVarRegion->Branch("weight_pileup_UP"        , &weight_pileup_UP         , "weight_pileup_UP/F");
      m_treeFitVarRegion->Branch("weight_pileup_DOWN"      , &weight_pileup_DOWN       , "weight_pileup_DOWN/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_EL_SF_Trigger_UP"        , &weight_leptonSF_EL_SF_Trigger_UP         , "weight_leptonSF_EL_SF_Trigger_UP/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_EL_SF_Trigger_DOWN"      , &weight_leptonSF_EL_SF_Trigger_DOWN       , "weight_leptonSF_EL_SF_Trigger_DOWN/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_EL_SF_Reco_UP"           , &weight_leptonSF_EL_SF_Reco_UP            , "weight_leptonSF_EL_SF_Reco_UP/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_EL_SF_Reco_DOWN"         , &weight_leptonSF_EL_SF_Reco_DOWN          , "weight_leptonSF_EL_SF_Reco_DOWN/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_EL_SF_ID_UP"             , &weight_leptonSF_EL_SF_ID_UP              , "weight_leptonSF_EL_SF_ID_UP/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_EL_SF_ID_DOWN"           , &weight_leptonSF_EL_SF_ID_DOWN            , "weight_leptonSF_EL_SF_ID_DOWN/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_EL_SF_Isol_UP"           , &weight_leptonSF_EL_SF_Isol_UP            , "weight_leptonSF_EL_SF_Isol_UP/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_EL_SF_Isol_DOWN"         , &weight_leptonSF_EL_SF_Isol_DOWN          , "weight_leptonSF_EL_SF_Isol_DOWN/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_MU_SF_Trigger_STAT_UP"   , &weight_leptonSF_MU_SF_Trigger_STAT_UP    , "weight_leptonSF_MU_SF_Trigger_STAT_UP/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_MU_SF_Trigger_STAT_DOWN" , &weight_leptonSF_MU_SF_Trigger_STAT_DOWN  , "weight_leptonSF_MU_SF_Trigger_STAT_DOWN/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_MU_SF_Trigger_SYST_UP"   , &weight_leptonSF_MU_SF_Trigger_SYST_UP    , "weight_leptonSF_MU_SF_Trigger_SYST_UP/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_MU_SF_Trigger_SYST_DOWN" , &weight_leptonSF_MU_SF_Trigger_SYST_DOWN  , "weight_leptonSF_MU_SF_Trigger_SYST_DOWN/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_MU_SF_ID_STAT_UP"        , &weight_leptonSF_MU_SF_ID_STAT_UP         , "weight_leptonSF_MU_SF_ID_STAT_UP/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_MU_SF_ID_STAT_DOWN"      , &weight_leptonSF_MU_SF_ID_STAT_DOWN       , "weight_leptonSF_MU_SF_ID_STAT_DOWN/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_MU_SF_ID_SYST_UP"        , &weight_leptonSF_MU_SF_ID_SYST_UP         , "weight_leptonSF_MU_SF_ID_SYST_UP/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_MU_SF_ID_SYST_DOWN"      , &weight_leptonSF_MU_SF_ID_SYST_DOWN       , "weight_leptonSF_MU_SF_ID_SYST_DOWN/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP"  , &weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP   , "weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN , "weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP"  , &weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP   , "weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN , "weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_MU_SF_Isol_STAT_UP"      , &weight_leptonSF_MU_SF_Isol_STAT_UP       , "weight_leptonSF_MU_SF_Isol_STAT_UP/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_MU_SF_Isol_STAT_DOWN"    , &weight_leptonSF_MU_SF_Isol_STAT_DOWN     , "weight_leptonSF_MU_SF_Isol_STAT_DOWN/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_MU_SF_Isol_SYST_UP"      , &weight_leptonSF_MU_SF_Isol_SYST_UP       , "weight_leptonSF_MU_SF_Isol_SYST_UP/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_MU_SF_Isol_SYST_DOWN"    , &weight_leptonSF_MU_SF_Isol_SYST_DOWN     , "weight_leptonSF_MU_SF_Isol_SYST_DOWN/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_MU_SF_TTVA_STAT_UP"      , &weight_leptonSF_MU_SF_TTVA_STAT_UP       , "weight_leptonSF_MU_SF_TTVA_STAT_UP/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_MU_SF_TTVA_STAT_DOWN"    , &weight_leptonSF_MU_SF_TTVA_STAT_DOWN     , "weight_leptonSF_MU_SF_TTVA_STAT_DOWN/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_MU_SF_TTVA_SYST_UP"      , &weight_leptonSF_MU_SF_TTVA_SYST_UP       , "weight_leptonSF_MU_SF_TTVA_SYST_UP/F");
      m_treeFitVarRegion->Branch("weight_leptonSF_MU_SF_TTVA_SYST_DOWN"    , &weight_leptonSF_MU_SF_TTVA_SYST_DOWN     , "weight_leptonSF_MU_SF_TTVA_SYST_DOWN/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_EL_Trigger"              , &weight_indiv_SF_EL_Trigger               , "weight_indiv_SF_EL_Trigger/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_EL_Trigger_UP"           , &weight_indiv_SF_EL_Trigger_UP            , "weight_indiv_SF_EL_Trigger_UP/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_EL_Trigger_DOWN"         , &weight_indiv_SF_EL_Trigger_DOWN          , "weight_indiv_SF_EL_Trigger_DOWN/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_EL_Reco"                 , &weight_indiv_SF_EL_Reco                  , "weight_indiv_SF_EL_Reco/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_EL_Reco_UP"              , &weight_indiv_SF_EL_Reco_UP               , "weight_indiv_SF_EL_Reco_UP/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_EL_Reco_DOWN"            , &weight_indiv_SF_EL_Reco_DOWN             , "weight_indiv_SF_EL_Reco_DOWN/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_EL_ID"                   , &weight_indiv_SF_EL_ID                    , "weight_indiv_SF_EL_ID/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_EL_ID_UP"                , &weight_indiv_SF_EL_ID_UP                 , "weight_indiv_SF_EL_ID_UP/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_EL_ID_DOWN"              , &weight_indiv_SF_EL_ID_DOWN               , "weight_indiv_SF_EL_ID_DOWN/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_EL_Isol"                 , &weight_indiv_SF_EL_Isol                  , "weight_indiv_SF_EL_Isol/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_EL_Isol_UP"              , &weight_indiv_SF_EL_Isol_UP               , "weight_indiv_SF_EL_Isol_UP/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_EL_Isol_DOWN"            , &weight_indiv_SF_EL_Isol_DOWN             , "weight_indiv_SF_EL_Isol_DOWN/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_Trigger"              , &weight_indiv_SF_MU_Trigger               , "weight_indiv_SF_MU_Trigger/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_Trigger_STAT_UP"      , &weight_indiv_SF_MU_Trigger_STAT_UP       , "weight_indiv_SF_MU_Trigger_STAT_UP/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_Trigger_STAT_DOWN"    , &weight_indiv_SF_MU_Trigger_STAT_DOWN     , "weight_indiv_SF_MU_Trigger_STAT_DOWN/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_Trigger_SYST_UP"      , &weight_indiv_SF_MU_Trigger_SYST_UP       , "weight_indiv_SF_MU_Trigger_SYST_UP/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_Trigger_SYST_DOWN"    , &weight_indiv_SF_MU_Trigger_SYST_DOWN     , "weight_indiv_SF_MU_Trigger_SYST_DOWN/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_ID"                   , &weight_indiv_SF_MU_ID                    , "weight_indiv_SF_MU_ID/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_ID_STAT_UP"           , &weight_indiv_SF_MU_ID_STAT_UP            , "weight_indiv_SF_MU_ID_STAT_UP/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_ID_STAT_DOWN"         , &weight_indiv_SF_MU_ID_STAT_DOWN          , "weight_indiv_SF_MU_ID_STAT_DOWN/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_ID_SYST_UP"           , &weight_indiv_SF_MU_ID_SYST_UP            , "weight_indiv_SF_MU_ID_SYST_UP/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_ID_SYST_DOWN"         , &weight_indiv_SF_MU_ID_SYST_DOWN          , "weight_indiv_SF_MU_ID_SYST_DOWN/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_ID_STAT_LOWPT_UP"     , &weight_indiv_SF_MU_ID_STAT_LOWPT_UP      , "weight_indiv_SF_MU_ID_STAT_LOWPT_UP/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN"   , &weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN    , "weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_ID_SYST_LOWPT_UP"     , &weight_indiv_SF_MU_ID_SYST_LOWPT_UP      , "weight_indiv_SF_MU_ID_SYST_LOWPT_UP/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN"   , &weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN    , "weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_Isol"                 , &weight_indiv_SF_MU_Isol                  , "weight_indiv_SF_MU_Isol/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_Isol_STAT_UP"         , &weight_indiv_SF_MU_Isol_STAT_UP          , "weight_indiv_SF_MU_Isol_STAT_UP/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_Isol_STAT_DOWN"       , &weight_indiv_SF_MU_Isol_STAT_DOWN        , "weight_indiv_SF_MU_Isol_STAT_DOWN/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_Isol_SYST_UP"         , &weight_indiv_SF_MU_Isol_SYST_UP          , "weight_indiv_SF_MU_Isol_SYST_UP/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_Isol_SYST_DOWN"       , &weight_indiv_SF_MU_Isol_SYST_DOWN        , "weight_indiv_SF_MU_Isol_SYST_DOWN/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_TTVA"                 , &weight_indiv_SF_MU_TTVA                  , "weight_indiv_SF_MU_TTVA/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_TTVA_STAT_UP"         , &weight_indiv_SF_MU_TTVA_STAT_UP          , "weight_indiv_SF_MU_TTVA_STAT_UP/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_TTVA_STAT_DOWN"       , &weight_indiv_SF_MU_TTVA_STAT_DOWN        , "weight_indiv_SF_MU_TTVA_STAT_DOWN/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_TTVA_SYST_UP"         , &weight_indiv_SF_MU_TTVA_SYST_UP          , "weight_indiv_SF_MU_TTVA_SYST_UP/F");
      m_treeFitVarRegion->Branch("weight_indiv_SF_MU_TTVA_SYST_DOWN"       , &weight_indiv_SF_MU_TTVA_SYST_DOWN        , "weight_indiv_SF_MU_TTVA_SYST_DOWN/F");
      m_treeFitVarRegion->Branch("weight_jvt_UP"                           , &weight_jvt_UP                            , "weight_jvt_UP/F");
      m_treeFitVarRegion->Branch("weight_jvt_DOWN"                         , &weight_jvt_DOWN                          , "weight_jvt_DOWN/F");

     m_treeFitVarRegion->Branch("weight_bTagSF_70_eigenvars_B_up"                       , weight_bTagSF_70_eigenvars_B_up        ); 
     m_treeFitVarRegion->Branch("weight_bTagSF_70_eigenvars_C_up"                       , weight_bTagSF_70_eigenvars_C_up        ); 
     m_treeFitVarRegion->Branch("weight_bTagSF_70_eigenvars_Light_up"                   , weight_bTagSF_70_eigenvars_Light_up    ); 
     m_treeFitVarRegion->Branch("weight_bTagSF_70_eigenvars_B_down"                     , weight_bTagSF_70_eigenvars_B_down      ); 
     m_treeFitVarRegion->Branch("weight_bTagSF_70_eigenvars_C_down"                     , weight_bTagSF_70_eigenvars_C_down      ); 
     m_treeFitVarRegion->Branch("weight_bTagSF_70_eigenvars_Light_down"                 , weight_bTagSF_70_eigenvars_Light_down  ); 
     m_treeFitVarRegion->Branch("weight_bTagSF_70_extrapolation_up"                     , &weight_bTagSF_70_extrapolation_up                      , "weight_bTagSF_70_extrapolation_up/F");
     m_treeFitVarRegion->Branch("weight_bTagSF_70_extrapolation_down"                   , &weight_bTagSF_70_extrapolation_down                    , "weight_bTagSF_70_extrapolation_down/F");
     m_treeFitVarRegion->Branch("weight_bTagSF_70_extrapolation_from_charm_up"          , &weight_bTagSF_70_extrapolation_from_charm_up           , "weight_bTagSF_70_extrapolation_from_charm_up/F");
     m_treeFitVarRegion->Branch("weight_bTagSF_70_extrapolation_from_charm_down"        , &weight_bTagSF_70_extrapolation_from_charm_down         , "weight_bTagSF_70_extrapolation_from_charm_down/F");
     m_treeFitVarRegion->Branch("weight_bTagSF_Continuous_eigenvars_B_up"               , weight_bTagSF_Continuous_eigenvars_B_up       );
     m_treeFitVarRegion->Branch("weight_bTagSF_Continuous_eigenvars_C_up"               , weight_bTagSF_Continuous_eigenvars_C_up       );
     m_treeFitVarRegion->Branch("weight_bTagSF_Continuous_eigenvars_Light_up"           , weight_bTagSF_Continuous_eigenvars_Light_up   );
     m_treeFitVarRegion->Branch("weight_bTagSF_Continuous_eigenvars_B_down"             , weight_bTagSF_Continuous_eigenvars_B_down     );
     m_treeFitVarRegion->Branch("weight_bTagSF_Continuous_eigenvars_C_down"             , weight_bTagSF_Continuous_eigenvars_C_down     );
     m_treeFitVarRegion->Branch("weight_bTagSF_Continuous_eigenvars_Light_down"         , weight_bTagSF_Continuous_eigenvars_Light_down );
     m_treeFitVarRegion->Branch("weight_bTagSF_Continuous_extrapolation_from_charm_up"  , &weight_bTagSF_Continuous_extrapolation_from_charm_up   , "weight_bTagSF_Continuous_extrapolation_from_charm_up/F");
     m_treeFitVarRegion->Branch("weight_bTagSF_Continuous_extrapolation_from_charm_down", &weight_bTagSF_Continuous_extrapolation_from_charm_down , "weight_bTagSF_Continuous_extrapolation_from_charm_down/F");
   }

}

void cutFlow::InitHisto() { // method called at each new Loop() method
   // _____ for each process (ex : ttbar, ttH) create a new file with histos _____
   TString histoProdFolder = "histos/"+fProd;
   if (gSystem->Exec("test -d "+histoProdFolder)) gSystem->Exec("mkdir -p "+histoProdFolder); // create folder if it doesnt exist
   TString histoTreeFolder = histoProdFolder + "/" + fTreeName;
   if (gSystem->Exec("test -d "+histoTreeFolder)) gSystem->Exec("mkdir -p "+histoTreeFolder); // create folder if it doesnt exist
   fFileOutHisto = new TFile(histoTreeFolder + "/myHistograms_"+fProcess+".root", "RECREATE");
   //if (fProcess != "data") fFileOutHisto_tr = new TFile("histos/"+fProd+"/myHistograms_tr_"+fProcess+".root", "RECREATE"); // truth info

   // _____ define the bining _____
   int nbinphi(8);   double phimax(3.2); // phi
   int nbineta(26);   double etamax(2.6); //eta
   int nbinetalep(12);   double etamaxlep(2.4); //eta
   int nbindR(21);    double dRmax(4.2); //dR
   int nbinptlep(13); double minptlep(25), maxptlep(350); // pT lep
   int nbinptmet(14); double minptmet(0), maxptmet(350); // pT lep
   int nbinnlep(3);
   int nbinptj(19);   double minptj(25), maxptj(500);  // pT jet
   int nbinnj(13);
   int nbinnb70j(7);
   int nbinptJ(13);   double minptJ(200), maxptJ(850);  // pT fat jet
   int nbinnJ(6);
   int nbinmJ(30);    double minmJ(50), maxmJ(350);
   int nbinD2J(30);    double minD2J(0.), maxD2J(3.0);
   int nbinC2J(25);    double minC2J(0.), maxC2J(0.5);
   int nbintauwtaJ(22);    double mintauwtaJ(0.), maxtauwtaJ(0.88);
   Float_t binsResolvedBDT[] = { -1.0, -0.34, -0.19, -0.07, 0.03, 0.12, 0.21, 0.3, 1.0 };
   int otherBinsResolvedBDT = 16;

   // _____ instanciate variables to write in trees _____

   if ( fsaveBoostedCutflow ) {
      h2_cutFlowValidationBoostedYield_elmu = new TH2D("h2_cutFlowValidationBoostedYield_elmu", "", 7, 0, 7, 2, 0, 2); // 0 for el, 1 for mu
      h2_cutFlowValidationBoostedStat_elmu = new TH2D("h2_cutFlowValidationBoostedStat_elmu", "", 7, 0, 7, 2, 0, 2); // 0 for el, 1 for mu
      h2_cutFlowValidationBoostedYield_elmu->SetTitle(fProcess+";cut number;electron                   muon");
      h2_cutFlowValidationBoostedStat_elmu ->SetTitle(fProcess+";cut number;electron                   muon");
      h2_cutFlowValidationBoostedYield_elmu->SetNdivisions(10, "x");
      h2_cutFlowValidationBoostedStat_elmu ->SetNdivisions(10, "x");
      h2_cutFlowValidationBoostedYield_elmu->SetNdivisions(1, "y");
      h2_cutFlowValidationBoostedStat_elmu ->SetNdivisions(1, "y");
      h2_cutFlowValidationBoostedYield_elmu->GetYaxis()->CenterTitle();
      h2_cutFlowValidationBoostedStat_elmu ->GetYaxis()->CenterTitle();
   }

   if ( fsaveResolvedCutflow ) {
      h2_cutFlowValidationResolvedYield_elmu = new TH2D("h2_cutFlowValidationResolvedYield_elmu", "", 13, 0, 13, 2, 0, 2); // 0 for el, 1 for mu
      h2_cutFlowValidationResolvedStat_elmu = new TH2D("h2_cutFlowValidationResolvedStat_elmu", "", 13, 0, 13, 2, 0, 2); // 0 for el, 1 for mu
      h2_cutFlowValidationResolvedYield_elmu->SetTitle(fProcess+";cut number;electron                   muon");
      h2_cutFlowValidationResolvedStat_elmu ->SetTitle(fProcess+";cut number;electron                   muon");
      h2_cutFlowValidationResolvedYield_elmu ->SetNdivisions(10);
      h2_cutFlowValidationResolvedStat_elmu  ->SetNdivisions(10);
      h2_cutFlowValidationResolvedYield_elmu ->SetNdivisions(0);
      h2_cutFlowValidationResolvedStat_elmu  ->SetNdivisions(0);
   }

   for ( map<TString, bool>::iterator it = fselection.begin(); it != fselection.end(); ++it) {
        
    fFileOutHisto->cd();

   // instanciate histos
     h1_J_n                         [it->first] = new TH1D("h1_J_n_sel"+it->first, "", nbinnJ, 0, nbinnJ);
     h1_J_pt                        [it->first] = new TH1D("h1_J_pt_sel"+it->first, "", nbinptJ, minptJ, maxptJ);
     h1_J_eta                       [it->first] = new TH1D("h1_J_eta_sel"+it->first, "", nbineta-6, -(etamax-0.6), etamax-0.6);
     h1_J_phi                       [it->first] = new TH1D("h1_J_phi_sel"+it->first, "", nbinphi, -phimax, phimax);
     h1_J_m                         [it->first] = new TH1D("h1_J_m_sel"+it->first, "", nbinmJ, minmJ, maxmJ);
     //h1_Htag_n                      [it->first] = new TH1D("h1_Htag_n_sel"+it->first, "", nbinnJ, 0, nbinnJ);
     h1_Htag_pt                     [it->first] = new TH1D("h1_Htag_pt_sel"+it->first, "", nbinptJ, minptJ, maxptJ);
     h1_Htag_eta                    [it->first] = new TH1D("h1_Htag_eta_sel"+it->first, "", (nbineta-6)/2, -(etamax-0.6), etamax-0.6);
     h1_Htag_phi                    [it->first] = new TH1D("h1_Htag_phi_sel"+it->first, "", nbinphi, -phimax, phimax);
     h1_Htag_m                      [it->first] = new TH1D("h1_Htag_m_sel"+it->first, "", nbinmJ, minmJ, maxmJ);
     h1_Htag_dRlep                  [it->first] = new TH1D("h1_Htag_dRlep_sel"+it->first, "", nbindR, 0, dRmax);
     h1_Htag_nib70                  [it->first] = new TH1D("h1_Htag_nib70_sel"+it->first, "", nbinnb70j, 0, nbinnb70j);
     h1_Htag_ninb70                 [it->first] = new TH1D("h1_Htag_ninb70_sel"+it->first, "", nbinnb70j, 0, nbinnb70j);
     h1_Htag_nib77                  [it->first] = new TH1D("h1_Htag_nib77_sel"+it->first, "", nbinnb70j, 0, nbinnb70j);
     h1_Htag_ninb77                 [it->first] = new TH1D("h1_Htag_ninb77_sel"+it->first, "", nbinnb70j, 0, nbinnb70j);
     h1_Htag_nib85                  [it->first] = new TH1D("h1_Htag_nib85_sel"+it->first, "", nbinnb70j, 0, nbinnb70j);
     h1_Htag_ninb85                 [it->first] = new TH1D("h1_Htag_ninb85_sel"+it->first, "", nbinnb70j, 0, nbinnb70j);
     h1_Htag_defTopTag              [it->first] = new TH1D("h1_Htag_defTopTag_sel"+it->first, "", 3,0,3);
     h1_Htag_D2                     [it->first] = new TH1D("h1_Htag_D2_sel"+it->first, "", nbinD2J, minD2J, maxD2J);
     h1_Htag_C2                     [it->first] = new TH1D("h1_Htag_C2_sel"+it->first, "", nbinC2J, minC2J, maxC2J);
     h1_Htag_tau21_wta              [it->first] = new TH1D("h1_Htag_tau21_wta_sel"+it->first, "", nbintauwtaJ, mintauwtaJ, maxtauwtaJ);
     h1_Htag_tau32_wta              [it->first] = new TH1D("h1_Htag_tau32_wta_sel"+it->first, "", nbintauwtaJ, mintauwtaJ, maxtauwtaJ);
     h1_Htag_sumPseudoMv2c10ib70    [it->first] = new TH1D("h1_Htag_sumPseudoMv2c10ib70_sel"+it->first, "", 12, 0, 12);
     h1_Htag_sumPseudoMv2c10ib77    [it->first] = new TH1D("h1_Htag_sumPseudoMv2c10ib77_sel"+it->first, "", 12, 0, 12);
     h1_Htag_sumPseudoMv2c10ib85    [it->first] = new TH1D("h1_Htag_sumPseudoMv2c10ib85_sel"+it->first, "", 12, 0, 12);
     //h1_ttag_n                      [it->first] = new TH1D("h1_ttag_n_sel"+it->first, "", nbinnJ, 0, nbinnJ);
     h1_ttag_pt                     [it->first] = new TH1D("h1_ttag_pt_sel"+it->first, "", nbinptJ, minptJ, maxptJ);
     h1_ttag_eta                    [it->first] = new TH1D("h1_ttag_eta_sel"+it->first, "", (nbineta-6)/2, -(etamax-0.6), etamax-0.6);
     h1_ttag_phi                    [it->first] = new TH1D("h1_ttag_phi_sel"+it->first, "", nbinphi, -phimax, phimax);
     h1_ttag_m                      [it->first] = new TH1D("h1_ttag_m_sel"+it->first, "", nbinmJ, minmJ, maxmJ);
     h1_ttag_dRlep                  [it->first] = new TH1D("h1_ttag_dRlep_sel"+it->first, "", nbindR, 0, dRmax);
     h1_ttag_nib70                  [it->first] = new TH1D("h1_ttag_nib70_sel"+it->first, "", nbinnb70j, 0, nbinnb70j);
     h1_ttag_ninb70                 [it->first] = new TH1D("h1_ttag_ninb70_sel"+it->first, "", nbinnb70j, 0, nbinnb70j);
     h1_ttag_nib77                  [it->first] = new TH1D("h1_ttag_nib77_sel"+it->first, "", nbinnb70j, 0, nbinnb70j);
     h1_ttag_ninb77                 [it->first] = new TH1D("h1_ttag_ninb77_sel"+it->first, "", nbinnb70j, 0, nbinnb70j);
     h1_ttag_nib85                  [it->first] = new TH1D("h1_ttag_nib85_sel"+it->first, "", nbinnb70j, 0, nbinnb70j);
     h1_ttag_ninb85                 [it->first] = new TH1D("h1_ttag_ninb85_sel"+it->first, "", nbinnb70j, 0, nbinnb70j);
     h1_ttag_defTopTag              [it->first] = new TH1D("h1_ttag_defTopTag_sel"+it->first, "", 3,0,3);
     h1_ttag_D2                     [it->first] = new TH1D("h1_ttag_D2_sel"+it->first, "", nbinD2J, minD2J, maxD2J);
     h1_ttag_C2                     [it->first] = new TH1D("h1_ttag_C2_sel"+it->first, "", nbinC2J, minC2J, maxC2J);
     h1_ttag_tau21_wta              [it->first] = new TH1D("h1_ttag_tau21_wta_sel"+it->first, "", nbintauwtaJ, mintauwtaJ, maxtauwtaJ);
     h1_ttag_tau32_wta              [it->first] = new TH1D("h1_ttag_tau32_wta_sel"+it->first, "", nbintauwtaJ, mintauwtaJ, maxtauwtaJ);
     h1_ttag_sumPseudoMv2c10ib70    [it->first] = new TH1D("h1_ttag_sumPseudoMv2c10ib70_sel"+it->first, "", 12, 0, 12);
     h1_ttag_sumPseudoMv2c10ib77    [it->first] = new TH1D("h1_ttag_sumPseudoMv2c10ib77_sel"+it->first, "", 12, 0, 12);
     h1_ttag_sumPseudoMv2c10ib85    [it->first] = new TH1D("h1_ttag_sumPseudoMv2c10ib85_sel"+it->first, "", 12, 0, 12);
     h1_J2ib77i_n                   [it->first] = new TH1D("h1_J2ib77i_n_sel"+it->first, "", nbinnJ, 0, nbinnJ);
     h1_Jtl1ib77_n                  [it->first] = new TH1D("h1_Jtl1ib77_n_sel"+it->first, "", nbinnJ, 0, nbinnJ);
     h2_hadTopPt_ttbarPt            [it->first] = new TH2D("h2_hadTopPt_ttbarPt_sel"+it->first, "", nbinptJ, minptJ, maxptJ, nbinptJ, minptJ, maxptJ);
     h2_hadTopPt_ttbarPt_u          [it->first] = new TH2D("h2_hadTopPt_ttbarPt_u_sel"+it->first, "", nbinptJ, minptJ, maxptJ, nbinptJ, minptJ, maxptJ);
     h2_hadTopPt_ttbarPt_c          [it->first] = new TH2D("h2_hadTopPt_ttbarPt_c_sel"+it->first, "", nbinptJ, minptJ, maxptJ, nbinptJ, minptJ, maxptJ);
     h2_nJversusnB70                [it->first] = new TH2D("h2_nJversusnB70_sel"+it->first, "", 3, 2, 5, 3, 4, 7);
     h2_nJversusnB70_nMC            [it->first] = new TH2D("h2_nJversusnB70_nMC_sel"+it->first, "", 3, 2, 5, 3, 4, 7);
     h1_hadTopPt                    [it->first] = new TH1D("h1_hadTopPt_sel"+it->first, "", nbinptJ, minptJ, maxptJ);
     h1_loosetJ_n                   [it->first] = new TH1D("h1_loosetJ_n_sel"+it->first, "", nbinnJ, 0, nbinnJ);
     h1_loosetJ_pt                  [it->first] = new TH1D("h1_loosetJ_pt_sel"+it->first, "", nbinptJ, minptJ, maxptJ);
     h1_loosetJ_eta                 [it->first] = new TH1D("h1_loosetJ_eta_sel"+it->first, "", nbineta-6, -(etamax-0.6), etamax-0.6);
     h1_loosetJ_phi                 [it->first] = new TH1D("h1_loosetJ_phi_sel"+it->first, "", nbinphi, -phimax, phimax);
     h1_loosetJ_m                   [it->first] = new TH1D("h1_loosetJ_m_sel"+it->first, "", nbinmJ, minmJ, maxmJ);
     h1_loosetJ_dRlepton            [it->first] = new TH1D("h1_loosetJ_dRlepton_sel"+it->first, "", nbindR, 0, dRmax);
     h1_tighttJ_n                   [it->first] = new TH1D("h1_tighttJ_n_sel"+it->first, "", nbinnJ, 0, nbinnJ);
     h1_tighttJ_pt                  [it->first] = new TH1D("h1_tighttJ_pt_sel"+it->first, "", nbinptJ, minptJ, maxptJ);
     h1_tighttJ_eta                 [it->first] = new TH1D("h1_tighttJ_eta_sel"+it->first, "", nbineta-6, -(etamax-0.6), etamax-0.6);
     h1_tighttJ_phi                 [it->first] = new TH1D("h1_tighttJ_phi_sel"+it->first, "", nbinphi, -phimax, phimax);
     h1_tighttJ_m                   [it->first] = new TH1D("h1_tighttJ_m_sel"+it->first, "", nbinmJ, minmJ, maxmJ);
     h1_j_n                         [it->first] = new TH1D("h1_j_n_sel"+it->first, "", nbinnj-2, 2, nbinnj);
     h1_j_pt                        [it->first] = new TH1D("h1_j_pt_sel"+it->first, "", nbinptj, minptj, maxptj);
     h1_j_eta                       [it->first] = new TH1D("h1_j_eta_sel"+it->first, "", nbineta, -etamax, etamax);
     h1_j_phi                       [it->first] = new TH1D("h1_j_phi_sel"+it->first, "", nbinphi, -phimax, phimax);
     h1_j_nOutsideHnT               [it->first] = new TH1D("h1_j_nOutsideHnT_sel"+it->first, "", nbinnj-4, 0, nbinnj-4);
     h1_b70j_n                      [it->first] = new TH1D("h1_b70j_n_sel"+it->first, "", nbinnb70j, 0, nbinnb70j);
     h1_b70j_pt                     [it->first] = new TH1D("h1_b70j_pt_sel"+it->first, "", nbinptj, minptj, maxptj);
     h1_b70j_eta                    [it->first] = new TH1D("h1_b70j_eta_sel"+it->first, "", nbineta, -etamax, etamax);
     h1_b70j_phi                    [it->first] = new TH1D("h1_b70j_phi_sel"+it->first, "", nbinphi, -phimax, phimax);
     h1_b77j_n                      [it->first] = new TH1D("h1_b77j_n_sel"+it->first, "", nbinnb70j, 0, nbinnb70j);
     h1_b77j_pt                     [it->first] = new TH1D("h1_b77j_pt_sel"+it->first, "", nbinptj, minptj, maxptj);
     h1_b77j_eta                    [it->first] = new TH1D("h1_b77j_eta_sel"+it->first, "", nbineta, -etamax, etamax);
     h1_b77j_phi                    [it->first] = new TH1D("h1_b77j_phi_sel"+it->first, "", nbinphi, -phimax, phimax);
     h1_b85j_n                      [it->first] = new TH1D("h1_b85j_n_sel"+it->first, "", nbinnb70j, 0, nbinnb70j);
     h1_b85j_pt                     [it->first] = new TH1D("h1_b85j_pt_sel"+it->first, "", nbinptj, minptj, maxptj);
     h1_b85j_eta                    [it->first] = new TH1D("h1_b85j_eta_sel"+it->first, "", nbineta, -etamax, etamax);
     h1_b85j_phi                    [it->first] = new TH1D("h1_b85j_phi_sel"+it->first, "", nbinphi, -phimax, phimax);
     h1_b77j_nOutsideHnT            [it->first] = new TH1D("h1_b77j_nOutsideHnT_sel"+it->first, "", nbinnb70j-2, 0, nbinnb70j-2);
     h1_b85j_nOutsideHnT            [it->first] = new TH1D("h1_b85j_nOutsideHnT_sel"+it->first, "", nbinnb70j-2, 0, nbinnb70j-2);
     h1_sumMv2c10_ob77_tNHtag       [it->first] = new TH1D("h1_sumMv2c10_ob77_tNHtag_sel"+it->first, "", 9, 0, 9);
     h1_sumMv2c10_ob85_tNHtag       [it->first] = new TH1D("h1_sumMv2c10_ob85_tNHtag_sel"+it->first, "", 9, 0, 9);
     h1_jOutOfLoosetJ_n             [it->first] = new TH1D("h1_jOutOfLoosetJ_n_sel"+it->first, "", nbinnj, 0, nbinnj);
     h1_jOutOfLoosetJ_pt            [it->first] = new TH1D("h1_jOutOfLoosetJ_pt_sel"+it->first, "", nbinptj, minptj, maxptj);
     h1_jOutOfLoosetJ_eta           [it->first] = new TH1D("h1_jOutOfLoosetJ_eta_sel"+it->first, "", nbineta, -etamax, etamax);
     h1_jOutOfLoosetJ_phi           [it->first] = new TH1D("h1_jOutOfLoosetJ_phi_sel"+it->first, "", nbinphi, -phimax, phimax);
     h1_b70jOutOfLoosetJ_n          [it->first] = new TH1D("h1_b70jOutOfLoosetJ_n_sel"+it->first, "", nbinnb70j, 0, nbinnb70j);
     h1_b70jOutOfLoosetJ_pt         [it->first] = new TH1D("h1_b70jOutOfLoosetJ_pt_sel"+it->first, "", nbinptj, minptj, maxptj);
     h1_b70jOutOfLoosetJ_eta        [it->first] = new TH1D("h1_b70jOutOfLoosetJ_eta_sel"+it->first, "", nbineta, -etamax, etamax);
     h1_b70jOutOfLoosetJ_phi        [it->first] = new TH1D("h1_b70jOutOfLoosetJ_phi_sel"+it->first, "", nbinphi, -phimax, phimax);
     h1_el_n                        [it->first] = new TH1D("h1_el_n_sel"+it->first, "", nbinnlep, 0, nbinnlep);
     h1_el_n_cutflow                [it->first] = new TH1D("h1_el_n_cutflow_sel"+it->first, "", nbinnlep, 0, nbinnlep);
     h1_el_pt                       [it->first] = new TH1D("h1_el_pt_sel"+it->first, "", nbinptlep, minptlep, maxptlep);
     h1_el_eta                      [it->first] = new TH1D("h1_el_eta_sel"+it->first, "", nbinetalep, -etamaxlep, etamaxlep);
     h1_el_phi                      [it->first] = new TH1D("h1_el_phi_sel"+it->first, "", nbinphi, -phimax, phimax);
     h1_mu_n                        [it->first] = new TH1D("h1_mu_n_sel"+it->first, "", nbinnlep, 0, nbinnlep);
     h1_mu_pt                       [it->first] = new TH1D("h1_mu_pt_sel"+it->first, "", nbinptlep, minptlep, maxptlep);
     h1_mu_eta                      [it->first] = new TH1D("h1_mu_eta_sel"+it->first, "", nbinetalep, -etamaxlep, etamaxlep);
     h1_mu_phi                      [it->first] = new TH1D("h1_mu_phi_sel"+it->first, "", nbinphi, -phimax, phimax);
     h1_met_met                     [it->first] = new TH1D("h1_met_met_sel"+it->first, "", nbinptmet, minptmet, maxptmet);
     h1_met_phi                     [it->first] = new TH1D("h1_met_phi_sel"+it->first, "", nbinphi, -phimax, phimax);
     h1_all                         [it->first] = new TH1D("h1_all_sel"+it->first, "", 1, 0, 1);
     h1_dR_ttagHtag                 [it->first] = new TH1D("h1_dR_ttagHtag_sel"+it->first, "", nbindR, 0, dRmax);
     h1_TopHFFilterFlag             [it->first] = new TH1D("h1_TopHFFilterFlag_sel"+it->first, "", 6, 0, 6);
     h1_ClassifBDTOutput_withReco_basic_binned [it->first] = new TH1D("h1_ClassifBDTOutput_withReco_basic_binned_sel"+it->first, "", sizeof(binsResolvedBDT)/sizeof(Float_t) - 1, binsResolvedBDT);
     h1_ClassifBDTOutput_withReco_basic [it->first] = new TH1D("h1_ClassifBDTOutput_withReco_basic_sel"+it->first, "", otherBinsResolvedBDT, -0.8 , 0.8);

     // set their labels
     h1_J_n                         [it->first]->Sumw2();
     h1_J_pt                        [it->first]->Sumw2();
     h1_J_eta                       [it->first]->Sumw2();
     h1_J_phi                       [it->first]->Sumw2();
     h1_J_m                         [it->first]->Sumw2();
     //h1_Htag_n                      [it->first]->Sumw2();
     h1_Htag_pt                     [it->first]->Sumw2();
     h1_Htag_eta                    [it->first]->Sumw2();
     h1_Htag_phi                    [it->first]->Sumw2();
     h1_Htag_m                      [it->first]->Sumw2();
     h1_Htag_dRlep                  [it->first]->Sumw2();
     h1_Htag_nib70                  [it->first]->Sumw2();
     h1_Htag_ninb70                 [it->first]->Sumw2();
     h1_Htag_nib77                  [it->first]->Sumw2();
     h1_Htag_ninb77                 [it->first]->Sumw2();
     h1_Htag_nib85                  [it->first]->Sumw2();
     h1_Htag_ninb85                 [it->first]->Sumw2();
     h1_Htag_defTopTag              [it->first]->Sumw2();
     h1_Htag_D2                     [it->first]->Sumw2();
     h1_Htag_C2                     [it->first]->Sumw2();
     h1_Htag_tau21_wta              [it->first]->Sumw2();
     h1_Htag_tau32_wta              [it->first]->Sumw2();
     h1_Htag_sumPseudoMv2c10ib70    [it->first]->Sumw2();
     h1_Htag_sumPseudoMv2c10ib77    [it->first]->Sumw2();
     h1_Htag_sumPseudoMv2c10ib85    [it->first]->Sumw2();
     //h1_ttag_n                      [it->first]->Sumw2();
     h1_ttag_pt                     [it->first]->Sumw2();
     h1_ttag_eta                    [it->first]->Sumw2();
     h1_ttag_phi                    [it->first]->Sumw2();
     h1_ttag_m                      [it->first]->Sumw2();
     h1_ttag_dRlep                  [it->first]->Sumw2();
     h1_ttag_nib70                  [it->first]->Sumw2();
     h1_ttag_ninb70                 [it->first]->Sumw2();
     h1_ttag_nib77                  [it->first]->Sumw2();
     h1_ttag_ninb77                 [it->first]->Sumw2();
     h1_ttag_nib85                  [it->first]->Sumw2();
     h1_ttag_ninb85                 [it->first]->Sumw2();
     h1_ttag_defTopTag              [it->first]->Sumw2();
     h1_ttag_D2                     [it->first]->Sumw2();
     h1_ttag_C2                     [it->first]->Sumw2();
     h1_ttag_tau21_wta              [it->first]->Sumw2();
     h1_ttag_tau32_wta              [it->first]->Sumw2();
     h1_ttag_sumPseudoMv2c10ib70    [it->first]->Sumw2();
     h1_ttag_sumPseudoMv2c10ib77    [it->first]->Sumw2();
     h1_ttag_sumPseudoMv2c10ib85    [it->first]->Sumw2();
     h1_J2ib77i_n                   [it->first]->Sumw2();
     h1_Jtl1ib77_n                  [it->first]->Sumw2();
     h2_hadTopPt_ttbarPt            [it->first]->Sumw2();
     h2_hadTopPt_ttbarPt_u          [it->first]->Sumw2();
     h2_hadTopPt_ttbarPt_c          [it->first]->Sumw2();
     h2_nJversusnB70                [it->first]->Sumw2();
     h2_nJversusnB70_nMC            [it->first]->Sumw2();
     h1_hadTopPt                    [it->first]->Sumw2();
     h1_loosetJ_n                   [it->first]->Sumw2();
     h1_loosetJ_pt                  [it->first]->Sumw2();
     h1_loosetJ_eta                 [it->first]->Sumw2();
     h1_loosetJ_phi                 [it->first]->Sumw2();
     h1_loosetJ_m                   [it->first]->Sumw2();
     h1_loosetJ_dRlepton            [it->first]->Sumw2();
     h1_tighttJ_n                   [it->first]->Sumw2();
     h1_tighttJ_pt                  [it->first]->Sumw2();
     h1_tighttJ_eta                 [it->first]->Sumw2();
     h1_tighttJ_phi                 [it->first]->Sumw2();
     h1_tighttJ_m                   [it->first]->Sumw2();
     h1_j_n                         [it->first]->Sumw2();
     h1_j_pt                        [it->first]->Sumw2();
     h1_j_eta                       [it->first]->Sumw2();
     h1_j_phi                       [it->first]->Sumw2();
     h1_j_nOutsideHnT               [it->first]->Sumw2();
     h1_b70j_n                      [it->first]->Sumw2();
     h1_b70j_pt                     [it->first]->Sumw2();
     h1_b70j_eta                    [it->first]->Sumw2();
     h1_b70j_phi                    [it->first]->Sumw2();
     h1_b77j_n                      [it->first]->Sumw2();
     h1_b77j_pt                     [it->first]->Sumw2();
     h1_b77j_eta                    [it->first]->Sumw2();
     h1_b77j_phi                    [it->first]->Sumw2();
     h1_b85j_n                      [it->first]->Sumw2();
     h1_b85j_pt                     [it->first]->Sumw2();
     h1_b85j_eta                    [it->first]->Sumw2();
     h1_b85j_phi                    [it->first]->Sumw2();
     h1_b77j_nOutsideHnT            [it->first]->Sumw2();
     h1_b85j_nOutsideHnT            [it->first]->Sumw2();
     h1_sumMv2c10_ob77_tNHtag       [it->first]->Sumw2();
     h1_sumMv2c10_ob85_tNHtag       [it->first]->Sumw2();
     h1_jOutOfLoosetJ_n             [it->first]->Sumw2();
     h1_jOutOfLoosetJ_pt            [it->first]->Sumw2();
     h1_jOutOfLoosetJ_eta           [it->first]->Sumw2();
     h1_jOutOfLoosetJ_phi           [it->first]->Sumw2();
     h1_b70jOutOfLoosetJ_n          [it->first]->Sumw2();
     h1_b70jOutOfLoosetJ_pt         [it->first]->Sumw2();
     h1_b70jOutOfLoosetJ_eta        [it->first]->Sumw2();
     h1_b70jOutOfLoosetJ_phi        [it->first]->Sumw2();
     h1_el_n                        [it->first]->Sumw2();
     h1_el_n_cutflow                [it->first]->Sumw2();
     h1_el_pt                       [it->first]->Sumw2();
     h1_el_eta                      [it->first]->Sumw2();
     h1_el_phi                      [it->first]->Sumw2();
     h1_mu_n                        [it->first]->Sumw2();
     h1_mu_pt                       [it->first]->Sumw2();
     h1_mu_eta                      [it->first]->Sumw2();
     h1_mu_phi                      [it->first]->Sumw2();
     h1_met_met                     [it->first]->Sumw2();
     h1_met_phi                     [it->first]->Sumw2();
     h1_all                         [it->first]->Sumw2();
     h1_dR_ttagHtag                 [it->first]->Sumw2();
     h1_TopHFFilterFlag             [it->first]->Sumw2();
     h1_ClassifBDTOutput_withReco_basic_binned      [it->first]->Sumw2();
     h1_ClassifBDTOutput_withReco_basic             [it->first]->Sumw2();

     h1_J_n                         [it->first]->SetTitle(fProcess+";# of large-R jets;yield");
     h1_J_pt                        [it->first]->SetTitle(fProcess+";large-R jet p_{T} (GeV);yield");
     h1_J_eta                       [it->first]->SetTitle(fProcess+";large-R jet #eta;yield");
     h1_J_phi                       [it->first]->SetTitle(fProcess+";large-R jet #phi;yield");
     h1_J_m                         [it->first]->SetTitle(fProcess+";large-R jet mass (GeV);yield");
     //h1_Htag_n                      [it->first]->SetTitle(fProcess+";# of H-tags;yield");
     h1_Htag_pt                     [it->first]->SetTitle(fProcess+";H-tag p_{T} (GeV);yield");
     h1_Htag_eta                    [it->first]->SetTitle(fProcess+";H-tag #eta;yield");
     h1_Htag_phi                    [it->first]->SetTitle(fProcess+";H-tag #phi;yield");
     h1_Htag_m                      [it->first]->SetTitle(fProcess+";H-tag mass (GeV);yield");
     h1_Htag_dRlep                  [it->first]->SetTitle(fProcess+";#DeltaR(H-tag,lepton);yield");
     h1_Htag_nib70                  [it->first]->SetTitle(fProcess+";# of b-tags (70%) inside H-tag;yield");
     h1_Htag_ninb70                 [it->first]->SetTitle(fProcess+";# of non b-tags (70%) inside H-tag;yield");
     h1_Htag_nib77                  [it->first]->SetTitle(fProcess+";# of b-tags (77%) inside H-tag;yield");
     h1_Htag_ninb77                 [it->first]->SetTitle(fProcess+";# of non b-tags (77%) inside H-tag;yield");
     h1_Htag_nib85                  [it->first]->SetTitle(fProcess+";# of b-tags (85%) inside H-tag;yield");
     h1_Htag_ninb85                 [it->first]->SetTitle(fProcess+";# of non b-tags (85%) inside H-tag;yield");
     h1_Htag_defTopTag              [it->first]->SetTitle(fProcess+";H-tag default top-tag;yield");
     h1_Htag_D2                     [it->first]->SetTitle(fProcess+";H-tag D2;yield");
     h1_Htag_C2                     [it->first]->SetTitle(fProcess+";H-tag C2;yield");
     h1_Htag_tau21_wta              [it->first]->SetTitle(fProcess+";H-tag #tau_{21} (wta);yield");
     h1_Htag_tau32_wta              [it->first]->SetTitle(fProcess+";H-tag #tau_{32} (wta);yield");
     h1_Htag_sumPseudoMv2c10ib70    [it->first]->SetTitle(fProcess+";#sum_{b-tag 70% in H-tag} pseudo MV2c10;yield");
     h1_Htag_sumPseudoMv2c10ib77    [it->first]->SetTitle(fProcess+";#sum_{b-tag 77% in H-tag} pseudo MV2c10;yield");
     h1_Htag_sumPseudoMv2c10ib85    [it->first]->SetTitle(fProcess+";#sum_{b-tag 85% in H-tag} pseudo MV2c10;yield");
     //h1_ttag_n                      [it->first]->SetTitle(fProcess+";# of t-tags;yield");
     h1_ttag_pt                     [it->first]->SetTitle(fProcess+";top-tag p_{T} (GeV);yield");
     h1_ttag_eta                    [it->first]->SetTitle(fProcess+";top-tag #eta;yield");
     h1_ttag_phi                    [it->first]->SetTitle(fProcess+";top-tag #phi;yield");
     h1_ttag_m                      [it->first]->SetTitle(fProcess+";top-tag mass (GeV);yield");
     h1_ttag_dRlep                  [it->first]->SetTitle(fProcess+";#DeltaR(top-tag,lepton);yield");
     h1_ttag_nib70                  [it->first]->SetTitle(fProcess+";# of b-tags (70%) inside top-tag;yield");
     h1_ttag_ninb70                 [it->first]->SetTitle(fProcess+";# of non b-tags (70%) inside top-tag;yield");
     h1_ttag_nib77                  [it->first]->SetTitle(fProcess+";# of b-tags (77%) inside top-tag;yield");
     h1_ttag_ninb77                 [it->first]->SetTitle(fProcess+";# of non b-tags (77%) inside top-tag;yield");
     h1_ttag_nib85                  [it->first]->SetTitle(fProcess+";# of b-tags (85%) inside top-tag;yield");
     h1_ttag_ninb85                 [it->first]->SetTitle(fProcess+";# of non b-tags (85%) inside top-tag;yield");
     h1_ttag_defTopTag              [it->first]->SetTitle(fProcess+";top-tag default top-tag;yield");
     h1_ttag_D2                     [it->first]->SetTitle(fProcess+";top-tag D2;yield");
     h1_ttag_C2                     [it->first]->SetTitle(fProcess+";top-tag C2;yield");
     h1_ttag_tau21_wta              [it->first]->SetTitle(fProcess+";top-tag #tau_{21} (wta);yield");
     h1_ttag_tau32_wta              [it->first]->SetTitle(fProcess+";top-tag #tau_{32} (wta);yield");
     h1_ttag_sumPseudoMv2c10ib70    [it->first]->SetTitle(fProcess+";#sum_{b-tag 70% in top-tag} pseudo MV2c10;yield");
     h1_ttag_sumPseudoMv2c10ib77    [it->first]->SetTitle(fProcess+";#sum_{b-tag 77% in top-tag} pseudo MV2c10;yield");
     h1_ttag_sumPseudoMv2c10ib85    [it->first]->SetTitle(fProcess+";#sum_{b-tag 85% in top-tag} pseudo MV2c10;yield");
     h1_J2ib77i_n                   [it->first]->SetTitle(fProcess+";# of large-R jets (250 GeV) #geq 2 b-tags (77%);yield");
     h1_Jtl1ib77_n                  [it->first]->SetTitle(fProcess+";# of large-R jets (250 GeV) 1 b-tag (77%) top-tag loose;yield");
     h2_hadTopPt_ttbarPt            [it->first]->SetTitle(fProcess+";truth #it{t_{had}} p_{T} (GeV);truth #it{t#bar{t}} p_{T} (GeV)");
     h2_hadTopPt_ttbarPt_u          [it->first]->SetTitle(fProcess+";truth #it{t_{had}} p_{T} (GeV);truth #it{t#bar{t}} p_{T} (GeV) (light)");
     h2_hadTopPt_ttbarPt_c          [it->first]->SetTitle(fProcess+";truth #it{t_{had}} p_{T} (GeV);truth #it{t#bar{t}} p_{T} (GeV) (c)");
     h2_nJversusnB70                [it->first]->SetTitle(fProcess+";# of b-tags (70% WP);# of jets");
     h2_nJversusnB70_nMC            [it->first]->SetTitle(fProcess+";# of b-tags (70% WP);# of jets");
     h1_hadTopPt                    [it->first]->SetTitle(fProcess+";truth #it{t_{had}} p_{T} (GeV);yield");
     h1_loosetJ_n                   [it->first]->SetTitle(fProcess+";# of large-R jet (loose top-tag);yield");
     h1_loosetJ_pt                  [it->first]->SetTitle(fProcess+";large-R jet (loose top-tag) p_{T} (GeV);yield");
     h1_loosetJ_eta                 [it->first]->SetTitle(fProcess+";large-R jet (loose top-tag) #eta;yield");
     h1_loosetJ_phi                 [it->first]->SetTitle(fProcess+";large-R jet (loose top-tag) #phi;yield");
     h1_loosetJ_m                   [it->first]->SetTitle(fProcess+";large-R jet (loose top-tag) mass (GeV);yield");
     h1_loosetJ_dRlepton            [it->first]->SetTitle(fProcess+";large-R jet (loose top-tag) dR to lepton;yield");
     h1_tighttJ_n                   [it->first]->SetTitle(fProcess+";# of large-R jet (tight top-tag);yield");
     h1_tighttJ_pt                  [it->first]->SetTitle(fProcess+";large-R jet (tight top-tag) p_{T} (GeV);yield");
     h1_tighttJ_eta                 [it->first]->SetTitle(fProcess+";large-R jet (tight top-tag) #eta;yield");
     h1_tighttJ_phi                 [it->first]->SetTitle(fProcess+";large-R jet (tight top-tag) #phi;yield");
     h1_tighttJ_m                   [it->first]->SetTitle(fProcess+";large-R jet (tight top-tag) mass (GeV);yield");
     h1_j_n                         [it->first]->SetTitle(fProcess+";# of jets;yield");
     h1_j_pt                        [it->first]->SetTitle(fProcess+";jet p_{T} (GeV);yield");
     h1_j_eta                       [it->first]->SetTitle(fProcess+";jet #eta;yield");
     h1_j_phi                       [it->first]->SetTitle(fProcess+";jet #phi;yield");
     h1_j_nOutsideHnT               [it->first]->SetTitle(fProcess+";# of jets outside H/top-tags;yield");
     h1_b70j_n                      [it->first]->SetTitle(fProcess+";# of b-tags (70%);yield");
     h1_b70j_pt                     [it->first]->SetTitle(fProcess+";b-tag (70%) p_{T} (GeV);yield");
     h1_b70j_eta                    [it->first]->SetTitle(fProcess+";b-tag (70%) #eta;yield");
     h1_b70j_phi                    [it->first]->SetTitle(fProcess+";b-tag (70%) #phi;yield");
     h1_b77j_n                      [it->first]->SetTitle(fProcess+";# of b-tags (77%);yield");
     h1_b77j_pt                     [it->first]->SetTitle(fProcess+";b-tag (77%) p_{T} (GeV);yield");
     h1_b77j_eta                    [it->first]->SetTitle(fProcess+";b-tag (77%) #eta;yield");
     h1_b77j_phi                    [it->first]->SetTitle(fProcess+";b-tag (77%) #phi;yield");
     h1_b85j_n                      [it->first]->SetTitle(fProcess+";# of b-tags (85%);yield");
     h1_b85j_pt                     [it->first]->SetTitle(fProcess+";b-tag (85%) p_{T} (GeV);yield");
     h1_b85j_eta                    [it->first]->SetTitle(fProcess+";b-tag (85%) #eta;yield");
     h1_b85j_phi                    [it->first]->SetTitle(fProcess+";b-tag (85%) #phi;yield");
     h1_b77j_nOutsideHnT            [it->first]->SetTitle(fProcess+";# of b-tags (77%) outside H/top-tags;yield");
     h1_b85j_nOutsideHnT            [it->first]->SetTitle(fProcess+";# of b-tags (85%) outside H/top-tags;yield");
     h1_sumMv2c10_ob77_tNHtag       [it->first]->SetTitle(fProcess+";#sum_{b-tag 77% outside H/top-tags} pseudo MV2c10;yield");
     h1_sumMv2c10_ob85_tNHtag       [it->first]->SetTitle(fProcess+";#sum_{b-tag 85% outside H/top-tags} pseudo MV2c10;yield");
     h1_jOutOfLoosetJ_n             [it->first]->SetTitle(fProcess+";# of jets (outside lse. top-tag);yield");
     h1_jOutOfLoosetJ_pt            [it->first]->SetTitle(fProcess+";jet (outside lse. top-tag) p_{T} (GeV);yield");
     h1_jOutOfLoosetJ_eta           [it->first]->SetTitle(fProcess+";jet (outside lse. top-tag) #eta;yield");
     h1_jOutOfLoosetJ_phi           [it->first]->SetTitle(fProcess+";jet (outside lse. top-tag) #phi;yield");
     h1_b70jOutOfLoosetJ_n          [it->first]->SetTitle(fProcess+";# of b-tags (70%, outside lse. top-tag);yield");
     h1_b70jOutOfLoosetJ_pt         [it->first]->SetTitle(fProcess+";b-tag (70%, outside lse. top-tag) p_{T} (GeV);yield");
     h1_b70jOutOfLoosetJ_eta        [it->first]->SetTitle(fProcess+";b-tag (70%, outside lse. top-tag) #eta;yield");
     h1_b70jOutOfLoosetJ_phi        [it->first]->SetTitle(fProcess+";b-tag (70%, outside lse. top-tag) #phi;yield");
     h1_el_n                        [it->first]->SetTitle(fProcess+";# of electrons;yield");
     h1_el_n_cutflow                [it->first]->SetTitle(fProcess+";# of electrons;yield");
     h1_el_pt                       [it->first]->SetTitle(fProcess+";electron p_{T} (GeV);yield");
     h1_el_eta                      [it->first]->SetTitle(fProcess+";electron #eta;yield");
     h1_el_phi                      [it->first]->SetTitle(fProcess+";electron #phi;yield");
     h1_mu_n                        [it->first]->SetTitle(fProcess+";# of muons;yield");
     h1_mu_pt                       [it->first]->SetTitle(fProcess+";muon p_{T} (GeV);yield");
     h1_mu_eta                      [it->first]->SetTitle(fProcess+";muon #eta;yield");
     h1_mu_phi                      [it->first]->SetTitle(fProcess+";muon #phi;yield");
     h1_met_met                     [it->first]->SetTitle(fProcess+";MET (GeV);yield");
     h1_met_phi                     [it->first]->SetTitle(fProcess+";MET #phi (GeV);yield");
     h1_all                         [it->first]->SetTitle(fProcess+";all;yield");
     h1_dR_ttagHtag                 [it->first]->SetTitle(fProcess+";#DeltaR(H-tag,top-tag);yield");
     h1_TopHFFilterFlag             [it->first]->SetTitle(fProcess+";top HF filter flag;yield");
     h1_ClassifBDTOutput_withReco_basic_binned      [it->first]->SetTitle(fProcess+";resolved BDT output;yield");
     h1_ClassifBDTOutput_withReco_basic             [it->first]->SetTitle(fProcess+";resolved BDT output;yield");
   }
}

void cutFlow::ReadInputTreeLeaf(TTree *tree) { // method called at each new Loop() method

   // _____ CUTS _____
   ptmin_lep = 25;

   // __ large-R jets __
   ptmin_J = 200; // (GeV)
   ptmax_J =1500;
   massmin_J = 50;
   radius_J = 1.0;
   ptmin_ttag = 250;
   ptmin_ttag_3211 = 250;
   etamax_J = 2.0;

   // __  jets __
   mv2c10min_b60j = 0.934906 ;  
   mv2c10min_b70j = 0.8244273;  
   mv2c10min_b77j = 0.645925;   
   mv2c10min_b85j = 0.1758475 ; 
   
   branchAddress(tree, fProd, fTreeName, fProcess);

   Notify();
}

void cutFlow::FillCutflowBoosted() {

      // standard boosted 3211 cutflow
      h2_cutFlowValidationBoostedYield_elmu->Fill(0., leptonflavour, evtwgtForCutflow);
      h2_cutFlowValidationBoostedStat_elmu->Fill(0., leptonflavour);
      // __ #0 __ //
      if (nJPt250 >= 1) {
         h2_cutFlowValidationBoostedYield_elmu->Fill(1., leptonflavour, evtwgtForCutflow);
         h2_cutFlowValidationBoostedStat_elmu->Fill(1., leptonflavour);
         // __ #1 __ //
         if (nloosetJPt250_woOR >= 1) {
            h2_cutFlowValidationBoostedYield_elmu->Fill(2, leptonflavour, evtwgtForCutflow);
            h2_cutFlowValidationBoostedStat_elmu->Fill(2, leptonflavour);
            // __ #2 __ //
            if (nloosetJ >= 1) {
               h2_cutFlowValidationBoostedYield_elmu->Fill(3, leptonflavour, evtwgtForCutflow);
               h2_cutFlowValidationBoostedStat_elmu->Fill(3, leptonflavour);
               // __ #3 __ //
               if (nj >= 3) {
                  h2_cutFlowValidationBoostedYield_elmu->Fill(4, leptonflavour, evtwgtForCutflow);
                  h2_cutFlowValidationBoostedStat_elmu->Fill(4, leptonflavour);
                  // __ #4 __ //
                  if (njOusidettags3211 >= 3) {
                     h2_cutFlowValidationBoostedYield_elmu->Fill(5, leptonflavour, evtwgtForCutflow);
                     h2_cutFlowValidationBoostedStat_elmu->Fill(5, leptonflavour);
                     // __ #5 __ //
                     if (nb70Outsidettags3211 >= 2) {
                        h2_cutFlowValidationBoostedYield_elmu->Fill(6, leptonflavour, evtwgtForCutflow);
                        h2_cutFlowValidationBoostedStat_elmu->Fill(6, leptonflavour);
                     }
                  }
               }
            }
         }
      }

}

void cutFlow::FillCutflowResolved() {

      // standard resolved cutflow
      // __ #0 __ //
      if (nJets >= 1) {
         h2_cutFlowValidationResolvedYield_elmu->Fill(0., leptonflavour, evtwgtForCutflow);
         h2_cutFlowValidationResolvedStat_elmu->Fill(0., leptonflavour);
      }
      // __ #1 __ //
      if (nJets >= 2) {
         h2_cutFlowValidationResolvedYield_elmu->Fill(1, leptonflavour, evtwgtForCutflow);
         h2_cutFlowValidationResolvedStat_elmu->Fill(1, leptonflavour);
      }
      // __ #2 __ //
      if (nJets >= 3) {
         h2_cutFlowValidationResolvedYield_elmu->Fill(2, leptonflavour, evtwgtForCutflow);
         h2_cutFlowValidationResolvedStat_elmu->Fill(2, leptonflavour);
      }
      // __ #3 __ //
      if (nJets >= 4) {
         h2_cutFlowValidationResolvedYield_elmu->Fill(3, leptonflavour, evtwgtForCutflow);
         h2_cutFlowValidationResolvedStat_elmu->Fill(3, leptonflavour);
      }
      // __ #4 __ //
      if (Pass_4JE2BE_tr) {
         h2_cutFlowValidationResolvedYield_elmu->Fill(4, leptonflavour, evtwgtForCutflow);
         h2_cutFlowValidationResolvedStat_elmu->Fill(4, leptonflavour);
      }
      // __ #5 __ //
      if (Pass_4JE3BE_tr) {
         h2_cutFlowValidationResolvedYield_elmu->Fill(5, leptonflavour, evtwgtForCutflow);
         h2_cutFlowValidationResolvedStat_elmu->Fill(5, leptonflavour);
      }
      // __ #6 __ //
      if (Pass_4JE4BI_tr) {
         h2_cutFlowValidationResolvedYield_elmu->Fill(6, leptonflavour, evtwgtForCutflow);
         h2_cutFlowValidationResolvedStat_elmu->Fill(6, leptonflavour);
      }
      // __ #7 __ //
      if (Pass_5JE2BE_tr) {
         h2_cutFlowValidationResolvedYield_elmu->Fill(7, leptonflavour, evtwgtForCutflow);
         h2_cutFlowValidationResolvedStat_elmu->Fill(7, leptonflavour);
      }
      // __ #8 __ //
      if (Pass_5JE3BE_tr) {
         h2_cutFlowValidationResolvedYield_elmu->Fill(8, leptonflavour, evtwgtForCutflow);
         h2_cutFlowValidationResolvedStat_elmu->Fill(8, leptonflavour);
      }
      // __ #9 __ //
      if (Pass_5JE4BI_tr) {
         h2_cutFlowValidationResolvedYield_elmu->Fill(9, leptonflavour, evtwgtForCutflow);
         h2_cutFlowValidationResolvedStat_elmu->Fill(9, leptonflavour);
      }
      // __ #10 __ //
      if (Pass_6JI2BE_tr) {
         h2_cutFlowValidationResolvedYield_elmu->Fill(10, leptonflavour, evtwgtForCutflow);
         h2_cutFlowValidationResolvedStat_elmu->Fill(10, leptonflavour);
      }
      // __ #11 __ //
      if (Pass_6JI3BE_tr) {
         h2_cutFlowValidationResolvedYield_elmu->Fill(11, leptonflavour, evtwgtForCutflow);
         h2_cutFlowValidationResolvedStat_elmu->Fill(11, leptonflavour);
      }
      // __ #12 __ //
      if (Pass_6JI4BI_tr) {
         h2_cutFlowValidationResolvedYield_elmu->Fill(12, leptonflavour, evtwgtForCutflow);
         h2_cutFlowValidationResolvedStat_elmu->Fill(12, leptonflavour);
      }

}

void cutFlow::FillHisto(TString selection, double evtwgt) {

   if (elchannel) {
      h1_el_pt[selection] ->Fill(lepton->v4()->Pt(), evtwgt);
      h1_el_eta[selection]->Fill(lepton->v4()->Eta(), evtwgt);
      h1_el_phi[selection]->Fill(lepton->v4()->Phi(), evtwgt);
      h1_el_n[selection]->Fill(1, evtwgt);
      h1_el_n_cutflow[selection]->Fill(1);
      h1_mu_n[selection]->Fill(0., evtwgt);
   } else if (muchannel) {
      h1_mu_pt[selection] ->Fill(lepton->v4()->Pt(), evtwgt);
      h1_mu_eta[selection]->Fill(lepton->v4()->Eta(), evtwgt);
      h1_mu_phi[selection]->Fill(lepton->v4()->Phi(), evtwgt);
      h1_mu_n[selection]->Fill(1, evtwgt);
      h1_el_n[selection]->Fill(0., evtwgt);
      h1_el_n_cutflow[selection]->Fill(0.);
   }

   // _____ jet loop _____
   for( jetIt = c_jet.begin(); jetIt != c_jet.end(); ++jetIt) {
      h1_j_pt[selection] ->Fill((*jetIt)->v4()->Pt(), evtwgt );
      h1_j_eta[selection]->Fill((*jetIt)->v4()->Eta(), evtwgt);
      h1_j_phi[selection]->Fill((*jetIt)->v4()->Phi(), evtwgt);

      bool outsideLooseTop = (*jetIt)->closestLooseTop() ? (*jetIt)->v4()->DeltaR(*((*jetIt)->closestLooseTop()->v4())) > radius_J : 0;
      //bool outsideLooseTop = true;
      // _____ outside loose top-jets loop _____
      if (outsideLooseTop) {
         h1_jOutOfLoosetJ_pt[selection] ->Fill((*jetIt)->v4()->Pt(), evtwgt );
         h1_jOutOfLoosetJ_eta[selection]->Fill((*jetIt)->v4()->Eta(), evtwgt);
         h1_jOutOfLoosetJ_phi[selection]->Fill((*jetIt)->v4()->Phi(), evtwgt);
      }

      // _____ b-jets _____
      if ( (*jetIt)->mv2c10() > mv2c10min_b70j )  {
         h1_b70j_pt[selection] ->Fill((*jetIt)->v4()->Pt(), evtwgt );
         h1_b70j_eta[selection]->Fill((*jetIt)->v4()->Eta(), evtwgt);
         h1_b70j_phi[selection]->Fill((*jetIt)->v4()->Phi(), evtwgt);

         // _____ outside loose top-jets loop _____
         if (outsideLooseTop) {
            h1_b70jOutOfLoosetJ_pt[selection] ->Fill((*jetIt)->v4()->Pt(), evtwgt);
            h1_b70jOutOfLoosetJ_eta[selection]->Fill((*jetIt)->v4()->Eta(), evtwgt);
            h1_b70jOutOfLoosetJ_phi[selection]->Fill((*jetIt)->v4()->Phi(), evtwgt);
         }
      }
      if ( (*jetIt)->mv2c10() > mv2c10min_b77j )  {
         h1_b77j_pt[selection] ->Fill((*jetIt)->v4()->Pt(), evtwgt );
         h1_b77j_eta[selection]->Fill((*jetIt)->v4()->Eta(), evtwgt);
         h1_b77j_phi[selection]->Fill((*jetIt)->v4()->Phi(), evtwgt);
      }
      if ( (*jetIt)->mv2c10() > mv2c10min_b85j )  {
         h1_b85j_pt[selection] ->Fill((*jetIt)->v4()->Pt(), evtwgt );
         h1_b85j_eta[selection]->Fill((*jetIt)->v4()->Eta(), evtwgt);
         h1_b85j_phi[selection]->Fill((*jetIt)->v4()->Phi(), evtwgt);
      }
   } // for jetIt
   h1_j_n[selection]->Fill(nj, evtwgt);
   h1_jOutOfLoosetJ_n[selection]->Fill(njOusidettags3211, evtwgt);
   h1_b70j_n[selection]->Fill(nb70j, evtwgt);
   h1_b77j_n[selection]->Fill(nb77j, evtwgt);
   h1_b85j_n[selection]->Fill(nb85j, evtwgt);
   h1_b70jOutOfLoosetJ_n[selection]->Fill(nb70Outsidettags3211, evtwgt);

   // _____ large-R jet loop _____
   for( fatIt = fatJets.begin(); fatIt != fatJets.end(); ++fatIt) {
      h1_J_pt[selection] ->Fill((*fatIt)->v4()->Pt(),  evtwgt);
      h1_J_eta[selection]->Fill((*fatIt)->v4()->Eta(), evtwgt);
      h1_J_phi[selection]->Fill((*fatIt)->v4()->Phi(), evtwgt);
      h1_J_m[selection]  ->Fill((*fatIt)->v4()->M(),   evtwgt);
      // _____ loose top-jet _____
      if ( (*fatIt)->topTag_loose() ) {
         double deltaRToLepton = (*fatIt)->v4()->DeltaR(*(lepton->v4()));

         h1_loosetJ_pt[selection]      ->Fill((*fatIt)->v4()->Pt(),  evtwgt);
         h1_loosetJ_eta[selection]     ->Fill((*fatIt)->v4()->Eta(), evtwgt);
         h1_loosetJ_phi[selection]     ->Fill((*fatIt)->v4()->Phi(), evtwgt);
         h1_loosetJ_m[selection]       ->Fill((*fatIt)->v4()->M(),   evtwgt);
         h1_loosetJ_dRlepton[selection]->Fill( deltaRToLepton,       evtwgt);

      } // end of if loose
      // _____ tight top-jet _____
      if ( (*fatIt)->topTag() ) {
         h1_tighttJ_pt[selection] ->Fill((*fatIt)->v4()->Pt(),  evtwgt);
         h1_tighttJ_eta[selection]->Fill((*fatIt)->v4()->Eta(), evtwgt);
         h1_tighttJ_phi[selection]->Fill((*fatIt)->v4()->Phi(), evtwgt);
         h1_tighttJ_m[selection]  ->Fill((*fatIt)->v4()->M(),   evtwgt);
      }

   } // end of for fatIt


   h1_J_n[selection]->Fill(nJ, evtwgt);
   h1_loosetJ_n[selection]->Fill(nloosetJ, evtwgt);
   h1_tighttJ_n[selection]->Fill(ntighttJ, evtwgt);
   h1_J2ib77i_n[selection]         ->Fill(count_if(fatJets.begin(), fatJets.end(), thereIs2ib77iPt250),   evtwgt);
   h1_Jtl1ib77_n[selection]        ->Fill(count_if(fatJets.begin(), fatJets.end(), thereIstl1ib77ePt250), evtwgt);

   if ( fTreeName == "nominal_Loose" and !fProcess.Contains("data") ) {
      for (unsigned int itruth = 0; itruth < truth_pt->size(); itruth++) {

         if (truth_nLepTop > 1) break;

         if ( isHadTop( truth_pdgid->at(itruth), truth_tthbb_info->at(itruth)) ) {
            h1_hadTopPt[selection]->Fill( truth_pt->at(itruth) / 1e3,     evtwgt );
            h2_hadTopPt_ttbarPt[selection]->Fill( truth_pt->at(itruth) / 1e3, truth_ttbar_pt / 1e3,     evtwgt );
            if (TopHeavyFlavorFilterFlag == 0) h2_hadTopPt_ttbarPt_u[selection]->Fill( truth_pt->at(itruth) / 1e3, truth_ttbar_pt / 1e3,     evtwgt );
            if (TopHeavyFlavorFilterFlag == 4) h2_hadTopPt_ttbarPt_c[selection]->Fill( truth_pt->at(itruth) / 1e3, truth_ttbar_pt / 1e3,     evtwgt );
            break;
         }
      }
   }

   h2_nJversusnB70[selection]->Fill(nb70j, nj, evtwgt);
   h2_nJversusnB70_nMC[selection]->Fill(nb70j, nj);

   // _____ MET _____
   h1_met_met[selection]->Fill(met->Pt(), evtwgt);
   h1_met_phi[selection]->Fill(met->Phi(), evtwgt);

   h1_all[selection]->Fill(0., evtwgt);
   h1_TopHFFilterFlag[selection]->Fill(TopHeavyFlavorFilterFlag, evtwgt);
   h1_ClassifBDTOutput_withReco_basic[selection]->Fill(ClassifBDTOutput_withReco_basic, evtwgt);
   h1_ClassifBDTOutput_withReco_basic_binned[selection]->Fill(ClassifBDTOutput_withReco_basic, evtwgt);

   // __ fill histos and tree variables with H-tags variables
   if ( fHiggsTag[selection] != fatJets.end() ) { // check if an Higgs-tag has been defined for this selection

      h1_Htag_pt        [selection]->Fill( (*fHiggsTag[selection])->v4()->Pt(), evtwgt);
      h1_Htag_eta       [selection]->Fill( (*fHiggsTag[selection])->v4()->Eta(), evtwgt);
      h1_Htag_phi       [selection]->Fill( (*fHiggsTag[selection])->v4()->Phi(), evtwgt);
      h1_Htag_m         [selection]->Fill( (*fHiggsTag[selection])->v4()->M(), evtwgt);
      h1_Htag_dRlep     [selection]->Fill( (*fHiggsTag[selection])->v4()->DeltaR(*(lepton->v4()))  , evtwgt);
      h1_Htag_nib70     [selection]->Fill( (*fHiggsTag[selection])->nb70jInsideJ(), evtwgt);
      h1_Htag_ninb70    [selection]->Fill( (*fHiggsTag[selection])->nnb70jInsideJ(), evtwgt);
      h1_Htag_nib77     [selection]->Fill( (*fHiggsTag[selection])->nb77jInsideJ(), evtwgt);
      h1_Htag_ninb77    [selection]->Fill( (*fHiggsTag[selection])->nnb77jInsideJ(), evtwgt);
      h1_Htag_nib85     [selection]->Fill( (*fHiggsTag[selection])->nb85jInsideJ(), evtwgt);
      h1_Htag_ninb85    [selection]->Fill( (*fHiggsTag[selection])->nnb85jInsideJ(), evtwgt);
      h1_Htag_defTopTag [selection]->Fill( (*fHiggsTag[selection])->defTopTag(), evtwgt);
      h1_Htag_D2        [selection]->Fill( (*fHiggsTag[selection])->D2(), evtwgt);
      h1_Htag_C2        [selection]->Fill( (*fHiggsTag[selection])->C2(), evtwgt);
      h1_Htag_tau21_wta [selection]->Fill( (*fHiggsTag[selection])->tau21_wta(), evtwgt);
      h1_Htag_tau32_wta [selection]->Fill( (*fHiggsTag[selection])->tau32_wta(), evtwgt);
      h1_Htag_sumPseudoMv2c10ib70 [selection]->Fill( (*fHiggsTag[selection])->sumPseudoMv2c10ib70(), evtwgt);
      h1_Htag_sumPseudoMv2c10ib77 [selection]->Fill( (*fHiggsTag[selection])->sumPseudoMv2c10ib77(), evtwgt);
      h1_Htag_sumPseudoMv2c10ib85 [selection]->Fill( (*fHiggsTag[selection])->sumPseudoMv2c10ib85(), evtwgt);
   }

   if ( fTopTag[selection] != fatJets.end() ) { // check if an top-tag has been defined for this selection

      h1_ttag_pt        [selection]->Fill( (*fTopTag[selection])->v4()->Pt(), evtwgt);
      h1_ttag_eta       [selection]->Fill( (*fTopTag[selection])->v4()->Eta(), evtwgt);
      h1_ttag_phi       [selection]->Fill( (*fTopTag[selection])->v4()->Phi(), evtwgt);
      h1_ttag_m         [selection]->Fill( (*fTopTag[selection])->v4()->M(), evtwgt);
      h1_ttag_dRlep     [selection]->Fill( (*fTopTag[selection])->v4()->DeltaR(*(lepton->v4()))  , evtwgt);
      h1_ttag_nib70     [selection]->Fill( (*fTopTag[selection])->nb70jInsideJ(), evtwgt);
      h1_ttag_ninb70    [selection]->Fill( (*fTopTag[selection])->nnb70jInsideJ(), evtwgt);
      h1_ttag_nib77     [selection]->Fill( (*fTopTag[selection])->nb77jInsideJ(), evtwgt);
      h1_ttag_ninb77    [selection]->Fill( (*fTopTag[selection])->nnb77jInsideJ(), evtwgt);
      h1_ttag_nib85     [selection]->Fill( (*fTopTag[selection])->nb85jInsideJ(), evtwgt);
      h1_ttag_ninb85    [selection]->Fill( (*fTopTag[selection])->nnb85jInsideJ(), evtwgt);
      h1_ttag_defTopTag [selection]->Fill( (*fTopTag[selection])->defTopTag(), evtwgt);
      h1_ttag_D2        [selection]->Fill( (*fTopTag[selection])->D2(), evtwgt);
      h1_ttag_C2        [selection]->Fill( (*fTopTag[selection])->C2(), evtwgt);
      h1_ttag_tau21_wta [selection]->Fill( (*fTopTag[selection])->tau21_wta(), evtwgt);
      h1_ttag_tau32_wta [selection]->Fill( (*fTopTag[selection])->tau32_wta(), evtwgt);
      h1_ttag_sumPseudoMv2c10ib70 [selection]->Fill( (*fTopTag[selection])->sumPseudoMv2c10ib70(), evtwgt);
      h1_ttag_sumPseudoMv2c10ib77 [selection]->Fill( (*fTopTag[selection])->sumPseudoMv2c10ib77(), evtwgt);
      h1_ttag_sumPseudoMv2c10ib85 [selection]->Fill( (*fTopTag[selection])->sumPseudoMv2c10ib85(), evtwgt);
   }

   if ( fHiggsTag[selection] != fatJets.end() and fTopTag[selection] != fatJets.end() ) {

      h1_b77j_nOutsideHnT[selection]->Fill(nb77j - ( (*fHiggsTag[selection])->nb77jInsideJ() + (*fTopTag[selection])->nb77jInsideJ() ), evtwgt);
      h1_b85j_nOutsideHnT[selection]->Fill(nb85j - ( (*fHiggsTag[selection])->nb85jInsideJ() + (*fTopTag[selection])->nb85jInsideJ() ), evtwgt);
      h1_sumMv2c10_ob77_tNHtag[selection]->Fill(sumPseudoMv2c10_b77 - 
            ((*fTopTag[selection])->sumPseudoMv2c10ib77() + (*fHiggsTag[selection])->sumPseudoMv2c10ib77()), evtwgt);
      h1_sumMv2c10_ob85_tNHtag[selection]->Fill(sumPseudoMv2c10_b85 - 
            ((*fTopTag[selection])->sumPseudoMv2c10ib85() + (*fHiggsTag[selection])->sumPseudoMv2c10ib85()), evtwgt);
      h1_j_nOutsideHnT[selection]->Fill(nj - ( (*fHiggsTag[selection])->njInsideJ() + (*fTopTag[selection])->njInsideJ() ), evtwgt);
      h1_dR_ttagHtag[selection]->Fill((*fHiggsTag[selection])->v4()->DeltaR( *( (*fTopTag[selection])->v4() ) ), evtwgt);

   } else if ( fHiggsTag[selection] != fatJets.end() ) {

      h1_b77j_nOutsideHnT[selection]->Fill(nb77j - (*fHiggsTag[selection])->nb77jInsideJ() , evtwgt);
      h1_b85j_nOutsideHnT[selection]->Fill(nb85j - (*fHiggsTag[selection])->nb85jInsideJ() , evtwgt);
      h1_sumMv2c10_ob77_tNHtag[selection]->Fill(sumPseudoMv2c10_b77 - (*fHiggsTag[selection])->sumPseudoMv2c10ib77(), evtwgt);
      h1_sumMv2c10_ob85_tNHtag[selection]->Fill(sumPseudoMv2c10_b85 - (*fHiggsTag[selection])->sumPseudoMv2c10ib85(), evtwgt);
      h1_j_nOutsideHnT[selection]->Fill(nj -  (*fHiggsTag[selection])->njInsideJ() , evtwgt);

   } else if ( fTopTag[selection] != fatJets.end() ) {

      h1_b77j_nOutsideHnT[selection]->Fill(nb77j -  (*fTopTag[selection])->nb77jInsideJ() , evtwgt);
      h1_b85j_nOutsideHnT[selection]->Fill(nb85j -  (*fTopTag[selection])->nb85jInsideJ() , evtwgt);
      h1_sumMv2c10_ob77_tNHtag[selection]->Fill(sumPseudoMv2c10_b77 - (*fTopTag[selection])->sumPseudoMv2c10ib77() , evtwgt);
      h1_sumMv2c10_ob85_tNHtag[selection]->Fill(sumPseudoMv2c10_b85 - (*fTopTag[selection])->sumPseudoMv2c10ib85() , evtwgt);
      h1_j_nOutsideHnT[selection]->Fill(nj - (*fTopTag[selection])->njInsideJ() , evtwgt);

   }
   
}

void cutFlow::FillVarForTMVA(TString selection) {
         //  Higgs-tagg                    
           Htag_pt_tr = -1.;               
           Htag_eta_tr = -3.;              
           Htag_phi_tr = - 4.;             
           Htag_m_tr = -1;                 
           Htag_dRlep_tr = -1.;            
           Htag_nib70_tr = -1.;            
           Htag_ninb70_tr = -1.;           
           Htag_nib77_tr = -1.;            
           Htag_ninb77_tr = -1.;           
           Htag_nib85_tr = -1.;            
           Htag_ninb85_tr = -1.;           
           Htag_defTopTag_tr = -1.;        
           Htag_D2_tr = -1.;               
           Htag_C2_tr = -1.;               
           Htag_tau21_wta_tr = -1.;        
           Htag_tau32_wta_tr = -1.;        
           Htag_sumPseudoMv2c10ib70_tr = 0;
           Htag_sumPseudoMv2c10ib77_tr = 0;
           Htag_sumPseudoMv2c10ib85_tr = 0;


         // __ fill histos and tree variables with H-tags variables
         if ( fHiggsTag[selection] != fatJets.end() ) { // check if an Higgs-tag has been defined for this selection

            Htag_pt_tr        = (*fHiggsTag[selection])->v4()->Pt();
            Htag_eta_tr       = (*fHiggsTag[selection])->v4()->Eta();
            Htag_phi_tr       = (*fHiggsTag[selection])->v4()->Phi();
            Htag_m_tr         = (*fHiggsTag[selection])->v4()->M();
            Htag_dRlep_tr     = (*fHiggsTag[selection])->v4()->DeltaR(*(lepton->v4()))  ;
            Htag_nib70_tr     = (*fHiggsTag[selection])->nb70jInsideJ();
            Htag_ninb70_tr    = (*fHiggsTag[selection])->nnb70jInsideJ();
            Htag_nib77_tr     = (*fHiggsTag[selection])->nb77jInsideJ();
            Htag_ninb77_tr    = (*fHiggsTag[selection])->nnb77jInsideJ();
            Htag_nib85_tr     = (*fHiggsTag[selection])->nb85jInsideJ();
            Htag_ninb85_tr    = (*fHiggsTag[selection])->nnb85jInsideJ();
            Htag_defTopTag_tr = (*fHiggsTag[selection])->defTopTag();
            Htag_D2_tr        = (*fHiggsTag[selection])->D2();
            Htag_C2_tr        = (*fHiggsTag[selection])->C2();
            Htag_tau21_wta_tr = (*fHiggsTag[selection])->tau21_wta();
            Htag_tau32_wta_tr = (*fHiggsTag[selection])->tau32_wta();
            Htag_sumPseudoMv2c10ib70_tr = (*fHiggsTag[selection])->sumPseudoMv2c10ib70();
            Htag_sumPseudoMv2c10ib77_tr = (*fHiggsTag[selection])->sumPseudoMv2c10ib77();
            Htag_sumPseudoMv2c10ib85_tr = (*fHiggsTag[selection])->sumPseudoMv2c10ib85();

         }
         
         //     top-tag
         ttag_pt_tr = -1.;
         ttag_eta_tr = -3.;
         ttag_phi_tr = - 4.;
         ttag_m_tr = -1;
         ttag_dRlep_tr = -1.;
         ttag_nib70_tr = -1.;
         ttag_ninb70_tr = -1.;
         ttag_nib77_tr = -1.;
         ttag_ninb77_tr = -1.;
         ttag_nib85_tr = -1.;
         ttag_ninb85_tr = -1.;
         ttag_defTopTag_tr = -1.;
         ttag_D2_tr = -1.;
         ttag_C2_tr = -1.;
         ttag_tau21_wta_tr = -1.;
         ttag_tau32_wta_tr = -1.;
         ttag_sumPseudoMv2c10ib70_tr = 0;
         ttag_sumPseudoMv2c10ib77_tr = 0;
         ttag_sumPseudoMv2c10ib85_tr = 0;
         // __ fill histos with t-tags variables
         if ( fTopTag[selection] != fatJets.end() ) { // check if an top-tag has been defined for this selection

            ttag_pt_tr        = (*fTopTag[selection])->v4()->Pt();
            ttag_eta_tr       = (*fTopTag[selection])->v4()->Eta();
            ttag_phi_tr       = (*fTopTag[selection])->v4()->Phi();
            ttag_m_tr         = (*fTopTag[selection])->v4()->M();
            ttag_dRlep_tr     = (*fTopTag[selection])->v4()->DeltaR(*(lepton->v4()))  ;
            ttag_nib70_tr     = (*fTopTag[selection])->nb70jInsideJ();
            ttag_ninb70_tr    = (*fTopTag[selection])->nnb70jInsideJ();
            ttag_nib77_tr     = (*fTopTag[selection])->nb77jInsideJ();
            ttag_ninb77_tr    = (*fTopTag[selection])->nnb77jInsideJ();
            ttag_nib85_tr     = (*fTopTag[selection])->nb85jInsideJ();
            ttag_ninb85_tr    = (*fTopTag[selection])->nnb85jInsideJ();
            ttag_defTopTag_tr = (*fTopTag[selection])->defTopTag();
            ttag_D2_tr        = (*fTopTag[selection])->D2();
            ttag_C2_tr        = (*fTopTag[selection])->C2();
            ttag_tau21_wta_tr = (*fTopTag[selection])->tau21_wta();
            ttag_tau32_wta_tr = (*fTopTag[selection])->tau32_wta();
            ttag_sumPseudoMv2c10ib70_tr = (*fTopTag[selection])->sumPseudoMv2c10ib70();
            ttag_sumPseudoMv2c10ib77_tr = (*fTopTag[selection])->sumPseudoMv2c10ib77();
            ttag_sumPseudoMv2c10ib85_tr = (*fTopTag[selection])->sumPseudoMv2c10ib85();

         }

         sumMv2c10_ob70_tNHtag_tr = -1;
         sumMv2c10_ob77_tNHtag_tr = -1;
         sumMv2c10_ob85_tNHtag_tr = -1;
         nob70_tNHtag_tr = -1;
         nob77_tNHtag_tr = -1;
         nob85_tNHtag_tr = -1;
         noj_tNHtag_tr = -1;
         nb70_tr = -1;
         nb77_tr = -1;
         nb85_tr = -1;
         nj_tr = -1;
         dR_ttagHtag_tr = -1.;
         selFinalWeight_tr = fEvtWeight;

         if ( fHiggsTag[selection] != fatJets.end() and fTopTag[selection] != fatJets.end() ) {
            nob70_tNHtag_tr = nb70j - ( (*fHiggsTag[selection])->nb70jInsideJ() + (*fTopTag[selection])->nb70jInsideJ() ) ; 
            nob77_tNHtag_tr = nb77j - ( (*fHiggsTag[selection])->nb77jInsideJ() + (*fTopTag[selection])->nb77jInsideJ() ) ; 
            nob85_tNHtag_tr = nb85j - ( (*fHiggsTag[selection])->nb85jInsideJ() + (*fTopTag[selection])->nb85jInsideJ() ) ; 
            noj_tNHtag_tr = nj - ( (*fHiggsTag[selection])->njInsideJ() + (*fTopTag[selection])->njInsideJ() ) ;

            sumMv2c10_ob70_tNHtag_tr = sumPseudoMv2c10_b70 - (ttag_sumPseudoMv2c10ib70_tr + Htag_sumPseudoMv2c10ib70_tr);
            sumMv2c10_ob77_tNHtag_tr = sumPseudoMv2c10_b77 - (ttag_sumPseudoMv2c10ib77_tr + Htag_sumPseudoMv2c10ib77_tr);
            sumMv2c10_ob85_tNHtag_tr = sumPseudoMv2c10_b85 - (ttag_sumPseudoMv2c10ib85_tr + Htag_sumPseudoMv2c10ib85_tr);

            dR_ttagHtag_tr = (*fHiggsTag[selection])->v4()->DeltaR( *( (*fTopTag[selection])->v4() ) );

         } else if ( fHiggsTag[selection] != fatJets.end() ) { // only Higgs tag defined
            nob70_tNHtag_tr = nb70j - (*fHiggsTag[selection])->nb70jInsideJ() ; 
            nob77_tNHtag_tr = nb77j - (*fHiggsTag[selection])->nb77jInsideJ() ; 
            nob85_tNHtag_tr = nb85j - (*fHiggsTag[selection])->nb85jInsideJ() ; 
            noj_tNHtag_tr = nj - (*fHiggsTag[selection])->njInsideJ() ;

            sumMv2c10_ob70_tNHtag_tr = sumPseudoMv2c10_b70 - Htag_sumPseudoMv2c10ib70_tr;
            sumMv2c10_ob77_tNHtag_tr = sumPseudoMv2c10_b77 - Htag_sumPseudoMv2c10ib77_tr;
            sumMv2c10_ob85_tNHtag_tr = sumPseudoMv2c10_b85 - Htag_sumPseudoMv2c10ib85_tr;
         } else if ( fTopTag[selection] != fatJets.end() ) { // only top tag defined
            nob70_tNHtag_tr = nb70j - (*fTopTag[selection])->nb70jInsideJ() ; 
            nob77_tNHtag_tr = nb77j - (*fTopTag[selection])->nb77jInsideJ() ; 
            nob85_tNHtag_tr = nb85j - (*fTopTag[selection])->nb85jInsideJ() ; 
            noj_tNHtag_tr = nj -  (*fTopTag[selection])->njInsideJ() ;

            sumMv2c10_ob70_tNHtag_tr = sumPseudoMv2c10_b70 - ttag_sumPseudoMv2c10ib70_tr ;
            sumMv2c10_ob77_tNHtag_tr = sumPseudoMv2c10_b77 - ttag_sumPseudoMv2c10ib77_tr ;
            sumMv2c10_ob85_tNHtag_tr = sumPseudoMv2c10_b85 - ttag_sumPseudoMv2c10ib85_tr ;
         }

      nb70_tr = nb70j ; 
      nb77_tr = nb77j ; 
      nb85_tr = nb85j ; 
      nj_tr   = nj  ;

}

void cutFlow::FinalizeHisto(){
    //fFileOutHisto = new TFile("histos/myHistograms_"+fProcess+".root", "RECREATE");
    //if (fProcess != "data") fFileOutHisto_tr = new TFile("histos/myHistograms_tr_"+fProcess+".root", "RECREATE"); // truth info
    fFileOutHisto->cd();
    TH1D* h1_lumi = new TH1D("h1_lumi", "h1_lumi", 1, 0, 1);
    h1_lumi->Fill(0., fDataLumi);
    h1_lumi->Write();
    if ( fsaveBoostedCutflow ) {
       h2_cutFlowValidationBoostedYield_elmu->Write();
       h2_cutFlowValidationBoostedStat_elmu->Write();
    }
    if ( fsaveResolvedCutflow ) {
       h2_cutFlowValidationResolvedYield_elmu->Write();
       h2_cutFlowValidationResolvedStat_elmu->Write();
    }
    for ( map<TString, bool>::iterator it = fselection.begin(); it != fselection.end(); ++it) {
       fFileOutHisto->cd();
       h1_J_n[it->first]->Write();
       h1_J_pt[it->first]->Write();
       h1_J_eta[it->first]->Write();
       h1_J_phi[it->first]->Write();
       h1_J_m[it->first]->Write();
       //h1_Htag_n[it->first]->Write();
       h1_Htag_pt[it->first]->Write();
       h1_Htag_eta[it->first]->Write();
       h1_Htag_phi[it->first]->Write();
       h1_Htag_m[it->first]->Write();
       h1_Htag_dRlep[it->first]->Write();
       h1_Htag_nib70[it->first]->Write();
       h1_Htag_ninb70[it->first]->Write();
       h1_Htag_nib77[it->first]->Write();
       h1_Htag_ninb77[it->first]->Write();
       h1_Htag_nib85[it->first]->Write();
       h1_Htag_ninb85[it->first]->Write();
       h1_Htag_defTopTag[it->first]->Write();
       h1_Htag_D2[it->first]->Write();
       h1_Htag_C2[it->first]->Write();
       h1_Htag_tau21_wta[it->first]->Write();
       h1_Htag_tau32_wta[it->first]->Write();
       h1_Htag_sumPseudoMv2c10ib70[it->first]->Write();
       h1_Htag_sumPseudoMv2c10ib77[it->first]->Write();
       h1_Htag_sumPseudoMv2c10ib85[it->first]->Write();
       //h1_ttag_n[it->first]->Write();
       h1_ttag_pt[it->first]->Write();
       h1_ttag_eta[it->first]->Write();
       h1_ttag_phi[it->first]->Write();
       h1_ttag_m[it->first]->Write();
       h1_ttag_dRlep[it->first]->Write();
       h1_ttag_nib70[it->first]->Write();
       h1_ttag_ninb70[it->first]->Write();
       h1_ttag_nib77[it->first]->Write();
       h1_ttag_ninb77[it->first]->Write();
       h1_ttag_nib85[it->first]->Write();
       h1_ttag_ninb85[it->first]->Write();
       h1_ttag_defTopTag[it->first]->Write();
       h1_ttag_D2[it->first]->Write();
       h1_ttag_C2[it->first]->Write();
       h1_ttag_tau21_wta[it->first]->Write();
       h1_ttag_tau32_wta[it->first]->Write();
       h1_ttag_sumPseudoMv2c10ib70[it->first]->Write();
       h1_ttag_sumPseudoMv2c10ib77[it->first]->Write();
       h1_ttag_sumPseudoMv2c10ib85[it->first]->Write();
       h1_J2ib77i_n[it->first]->Write();
       h1_Jtl1ib77_n[it->first]->Write();
       h2_hadTopPt_ttbarPt[it->first]->Write();
       h2_hadTopPt_ttbarPt_u[it->first]->Write();
       h2_hadTopPt_ttbarPt_c[it->first]->Write();
       h2_nJversusnB70[it->first]->Write();
       h2_nJversusnB70_nMC[it->first]->Write();
       h1_hadTopPt[it->first]->Write();
       h1_loosetJ_n[it->first]->Write();
       h1_loosetJ_pt[it->first]->Write();
       h1_loosetJ_eta[it->first]->Write();
       h1_loosetJ_phi[it->first]->Write();
       h1_loosetJ_m[it->first]->Write();
       h1_loosetJ_dRlepton[it->first]->Write();
       h1_tighttJ_n[it->first]->Write();
       h1_tighttJ_pt[it->first]->Write();
       h1_tighttJ_eta[it->first]->Write();
       h1_tighttJ_phi[it->first]->Write();
       h1_tighttJ_m[it->first]->Write();
       h1_j_n[it->first]->Write();
       h1_j_pt[it->first]->Write();
       h1_j_eta[it->first]->Write();
       h1_j_phi[it->first]->Write();
       h1_j_nOutsideHnT[it->first]->Write();
       h1_b70j_n[it->first]->Write();
       h1_b70j_pt[it->first]->Write();
       h1_b70j_eta[it->first]->Write();
       h1_b70j_phi[it->first]->Write();
       h1_b77j_n[it->first]->Write();
       h1_b77j_pt[it->first]->Write();
       h1_b77j_eta[it->first]->Write();
       h1_b77j_phi[it->first]->Write();
       h1_b85j_n[it->first]->Write();
       h1_b85j_pt[it->first]->Write();
       h1_b85j_eta[it->first]->Write();
       h1_b85j_phi[it->first]->Write();
       h1_b77j_nOutsideHnT[it->first]->Write();
       h1_b85j_nOutsideHnT[it->first]->Write();
       h1_sumMv2c10_ob77_tNHtag[it->first]->Write();
       h1_sumMv2c10_ob85_tNHtag[it->first]->Write();
       h1_jOutOfLoosetJ_n[it->first]->Write();
       h1_jOutOfLoosetJ_pt[it->first]->Write();
       h1_jOutOfLoosetJ_eta[it->first]->Write();
       h1_jOutOfLoosetJ_phi[it->first]->Write();
       h1_b70jOutOfLoosetJ_n[it->first]->Write();
       h1_b70jOutOfLoosetJ_pt[it->first]->Write();
       h1_b70jOutOfLoosetJ_eta[it->first]->Write();
       h1_b70jOutOfLoosetJ_phi[it->first]->Write();
       h1_el_n  [it->first]->Write();
       h1_el_n_cutflow  [it->first]->Write();
       h1_el_pt[it->first]->Write();
       h1_el_eta[it->first]->Write();
       h1_el_phi[it->first]->Write();
       h1_mu_n  [it->first]->Write();
       h1_mu_pt[it->first]->Write();
       h1_mu_eta[it->first]->Write();
       h1_mu_phi[it->first]->Write();
       h1_met_met[it->first]->Write();
       h1_met_phi[it->first]->Write();
       h1_all[it->first]->Write();
       h1_dR_ttagHtag[it->first]->Write();
       h1_TopHFFilterFlag[it->first]->Write();
       h1_ClassifBDTOutput_withReco_basic_binned[it->first]->Write();
       h1_ClassifBDTOutput_withReco_basic[it->first]->Write();
       

       delete h1_J_n[it->first];
       delete h1_J_pt[it->first];
       delete h1_J_eta[it->first];
       delete h1_J_phi[it->first];
       delete h1_J_m[it->first];
       //delete h1_Htag_n[it->first];
       delete h1_Htag_pt[it->first];
       delete h1_Htag_eta[it->first];
       delete h1_Htag_phi[it->first];
       delete h1_Htag_m[it->first];
       delete h1_Htag_dRlep[it->first];
       delete h1_Htag_nib70[it->first];
       delete h1_Htag_ninb70[it->first];
       delete h1_Htag_nib77[it->first];
       delete h1_Htag_ninb77[it->first];
       delete h1_Htag_nib85[it->first];
       delete h1_Htag_ninb85[it->first];
       delete h1_Htag_defTopTag[it->first];
       delete h1_Htag_D2[it->first];
       delete h1_Htag_C2[it->first];
       delete h1_Htag_tau21_wta[it->first];
       delete h1_Htag_tau32_wta[it->first];
       delete h1_Htag_sumPseudoMv2c10ib70[it->first];
       delete h1_Htag_sumPseudoMv2c10ib77[it->first];
       delete h1_Htag_sumPseudoMv2c10ib85[it->first];
       //delete h1_ttag_n[it->first];
       delete h1_ttag_pt[it->first];
       delete h1_ttag_eta[it->first];
       delete h1_ttag_phi[it->first];
       delete h1_ttag_m[it->first];
       delete h1_ttag_dRlep[it->first];
       delete h1_ttag_nib70[it->first];
       delete h1_ttag_ninb70[it->first];
       delete h1_ttag_nib77[it->first];
       delete h1_ttag_ninb77[it->first];
       delete h1_ttag_nib85[it->first];
       delete h1_ttag_ninb85[it->first];
       delete h1_ttag_defTopTag[it->first];
       delete h1_ttag_D2[it->first];
       delete h1_ttag_C2[it->first];
       delete h1_ttag_tau21_wta[it->first];
       delete h1_ttag_tau32_wta[it->first];
       delete h1_ttag_sumPseudoMv2c10ib70[it->first];
       delete h1_ttag_sumPseudoMv2c10ib77[it->first];
       delete h1_ttag_sumPseudoMv2c10ib85[it->first];
       delete h1_J2ib77i_n[it->first];
       delete h1_Jtl1ib77_n[it->first];
       delete h2_hadTopPt_ttbarPt[it->first];
       delete h2_hadTopPt_ttbarPt_u[it->first];
       delete h2_hadTopPt_ttbarPt_c[it->first];
       delete h2_nJversusnB70[it->first];
       delete h2_nJversusnB70_nMC[it->first];
       delete h1_hadTopPt[it->first];
       delete h1_loosetJ_n[it->first];
       delete h1_loosetJ_pt[it->first];
       delete h1_loosetJ_eta[it->first];
       delete h1_loosetJ_phi[it->first];
       delete h1_loosetJ_m[it->first];
       delete h1_loosetJ_dRlepton[it->first];
       delete h1_tighttJ_n[it->first];
       delete h1_tighttJ_pt[it->first];
       delete h1_tighttJ_eta[it->first];
       delete h1_tighttJ_phi[it->first];
       delete h1_tighttJ_m[it->first];
       delete h1_j_n[it->first];
       delete h1_j_pt[it->first];
       delete h1_j_eta[it->first];
       delete h1_j_phi[it->first];
       delete h1_j_nOutsideHnT[it->first];
       delete h1_b70j_n[it->first];
       delete h1_b70j_pt[it->first];
       delete h1_b70j_eta[it->first];
       delete h1_b70j_phi[it->first];
       delete h1_b77j_n[it->first];
       delete h1_b77j_pt[it->first];
       delete h1_b77j_eta[it->first];
       delete h1_b77j_phi[it->first];
       delete h1_b85j_n[it->first];
       delete h1_b85j_pt[it->first];
       delete h1_b85j_eta[it->first];
       delete h1_b85j_phi[it->first];
       delete h1_b77j_nOutsideHnT[it->first];
       delete h1_b85j_nOutsideHnT[it->first];
       delete h1_sumMv2c10_ob77_tNHtag[it->first];
       delete h1_sumMv2c10_ob85_tNHtag[it->first];
       delete h1_jOutOfLoosetJ_n[it->first];
       delete h1_jOutOfLoosetJ_pt[it->first];
       delete h1_jOutOfLoosetJ_eta[it->first];
       delete h1_jOutOfLoosetJ_phi[it->first];
       delete h1_b70jOutOfLoosetJ_n[it->first];
       delete h1_b70jOutOfLoosetJ_pt[it->first];
       delete h1_b70jOutOfLoosetJ_eta[it->first];
       delete h1_b70jOutOfLoosetJ_phi[it->first];
       delete h1_el_n  [it->first];
       delete h1_el_n_cutflow  [it->first];
       delete h1_el_pt[it->first];
       delete h1_el_eta[it->first];
       delete h1_el_phi[it->first];
       delete h1_mu_n  [it->first];
       delete h1_mu_pt[it->first];
       delete h1_mu_eta[it->first];
       delete h1_mu_phi[it->first];
       delete h1_met_met[it->first];
       delete h1_met_phi[it->first];
       delete h1_all[it->first];
       delete h1_dR_ttagHtag[it->first];
       delete h1_TopHFFilterFlag[it->first];
       delete h1_ClassifBDTOutput_withReco_basic_binned[it->first];
       delete h1_ClassifBDTOutput_withReco_basic[it->first];

    } //for it

   //delete h1_cutFlowYield;
   delete h1_lumi;
   if ( fsaveBoostedCutflow ) {
      delete h2_cutFlowValidationBoostedYield_elmu;
      delete h2_cutFlowValidationBoostedStat_elmu;
   }
   if ( fsaveResolvedCutflow ) {
      delete h2_cutFlowValidationResolvedYield_elmu;
      delete h2_cutFlowValidationResolvedStat_elmu;
   }

   fFileOutHisto->Close();
   //if (fProcess != "data") fFileOutHisto_tr->Close();
}

void cutFlow::FinalizeFitTree(){
       fFileOutFitVarRegion->cd();
       m_treeFitVarRegion->Write();
                                         
       fFileOutFitVarRegion->Close();
}

void cutFlow::FinalizeTMVATree(){
    for ( map<TString, bool>::iterator it = fselection.begin(); it != fselection.end(); ++it) {

       if (m_tmvaReader.find(it->first) != m_tmvaReader.end() ) delete m_tmvaReader[it->first];

       fFileOutTree[it->first]->cd();
       m_treeForTMVA[it->first]->Write();
       //delete m_treeForTMVA[it->first];
       fFileOutTree[it->first]->Close();

       fFileOutResolvedMinusSel[it->first]->cd();
       m_treeResolvedMinusSel[it->first]->Write();
       //delete m_treeResolvedMinusSel[it->first];
       fFileOutResolvedMinusSel[it->first]->Close();

    } //for it
}

Bool_t cutFlow::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void cutFlow::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t cutFlow::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}


#include "crossSections.C"
#include "processNormalisation.C"
#include "branchAddress.C"
#include "tmvaReaders.C"

#endif // #ifdef cutFlow_cxx
