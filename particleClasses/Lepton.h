#ifndef DEF_LEPTON
#define DEF_LEPTON
#include <vector>
#include "TLorentzVector.h"

class Lepton {
   
   public:
   Lepton(TLorentzVector* v4);
   TLorentzVector* v4() const;
   ~Lepton();
   
   private:
   TLorentzVector* m_v4; 
};
#endif
