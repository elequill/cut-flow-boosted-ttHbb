#ifndef DEF_JET
#define DEF_JET
#include <vector>
#include "TLorentzVector.h"
#include "FatJet.h"

class FatJet;

class Jet {
   
   public:
   Jet(TLorentzVector* v4, float mv2c10);
   
   void set_closestFatJet  (FatJet* closestFatJet  );
   void set_closestLooseTop(FatJet* closestLooseTop);
   void set_closestLooseTop_noElOver(FatJet* closestLooseTop_noElOver);
   void set_closestTightTop(FatJet* closestTightTop);
   void set_mv2c10(float& mv2c10);

   TLorentzVector* v4() const;
   FatJet* closestFatJet  () const ;
   FatJet* closestLooseTop() const ;
   FatJet* closestLooseTop_noElOver() const ;
   FatJet* closestTightTop() const ;
   float mv2c10() const;
   ~Jet();
   private:
   TLorentzVector* m_v4; 
   FatJet* m_closestFatJet  ;
   FatJet* m_closestLooseTop;
   FatJet* m_closestLooseTop_noElOver;
   FatJet* m_closestTightTop;
   float m_mv2c10;
};
#endif
