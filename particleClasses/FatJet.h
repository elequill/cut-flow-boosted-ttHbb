#ifndef DEF_FATJET
#define DEF_FATJET
#include <vector>
#include "TLorentzVector.h"
#include "Lepton.h"
#include "Jet.h"

class Jet;

class FatJet {
   
   public:
   FatJet(TLorentzVector* v4);
   void set_lepton         (Lepton* lepton       );
   void set_njOutsideJ     (int   njOutsideJ    );
   void set_nb77jOutsideJ  (int   nb77jOutsideJ );
   void set_nb70jOutsideJ  (int   nb70jOutsideJ );
   void set_njInsideJ      (int   njInsideJ     );
   void set_nb85jInsideJ   (int   nb85jInsideJ  );
   void set_nb77jInsideJ   (int   nb77jInsideJ  );
   void add_mv2c10ib85     (double   mv2c10ib85  );
   void set_sumMv2c10ib85   (double   sumMv2c10ib85  );
   void set_sumMv2c10ib77   (double   sumMv2c10ib77  );
   void set_sumMv2c10ib70   (double   sumMv2c10ib70  );
   void set_sumPseudoMv2c10ib85   (int   sumPseudoMv2c10ib85  );
   void set_sumPseudoMv2c10ib77   (int   sumPseudoMv2c10ib77  );
   void set_sumPseudoMv2c10ib70   (int   sumPseudoMv2c10ib70  );
   void set_nb70jInsideJ   (int   nb70jInsideJ  );
   void set_nnb85jInsideJ   (int   nnb85jInsideJ  );
   void set_nnb77jInsideJ   (int   nnb77jInsideJ  );
   void set_nnb70jInsideJ   (int   nnb70jInsideJ  );
   void set_sd12           (float& sd12          );
   void set_sd23           (float& sd23          );
   void set_tau21          (float& tau21         );
   void set_tau32          (float& tau32         );
   void set_tau21_wta      (float& tau21_wta     );
   void set_tau32_wta      (float& tau32_wta     );
   void set_D2             (float& D2            );
   void set_C2             (float& C2            );
   void set_topTag         (bool  topTag         );
   void set_bosonTag       (bool  bosonTag       );
   void set_topTag_loose   (bool  topTag_loose   );
   void set_defTopTag      (int  defTopTag   );
   void set_is2ib70i                   (bool is2ib70i                   );
   void set_istl1ib70eOutOfJ2ib70i        (bool istl1ib70eOutOfJ2ib70i        );
   void set_istl1ib70e1inb70iOutOfJ2ib70i (bool istl1ib70e1inb70iOutOfJ2ib70i );
   void set_istl1ib70eOrttOutOfJ2ib70i (bool istl1ib70eOrttOutOfJ2ib70i );
   void set_is2ib77i                   (bool is2ib77i                   );
   void set_istl1ib77eOutOfJ2ib77i        (bool istl1ib77eOutOfJ2ib77i        );
   void set_istl1ib77e1inb77iOutOfJ2ib77i (bool istl1ib77e1inb77iOutOfJ2ib77i );
   void set_is1ib70em100               (bool is1ib70em100               );
   void set_ism140OutOfJ1ib70em100     (bool ism140OutOfJ1ib70em100     );
   void set_bosonTag_loose (bool  bosonTag_loose );
   void set_truthmatch     (int   truthmatch     );
   void set_closestBJet    (Jet*  closestBJet    );
   void set_closestOutUJet (Jet*  closestOutUJet );
   void set_closestUJet    (Jet*  closestUJet );
   void set_subleadPtUJet  (Jet*  subleadPtUJet );
   void set_leadPtUJet     (Jet*  leadPtUJet );

   TLorentzVector* v4() const;
   Lepton* lepton() const;
   int   njOutsideJ() ;
   int   nb77jOutsideJ() ;
   int   nb70jOutsideJ() ;
   int   njInsideJ() ;
   int   nb85jInsideJ() ;
   int   nb77jInsideJ() ;
   int   nb70jInsideJ() ;
   vector<double> mv2c10ib85() ;
   double   sumMv2c10ib85() ;
   double   sumMv2c10ib77() ;
   double   sumMv2c10ib70() ;
   int   sumPseudoMv2c10ib85() ;
   int   sumPseudoMv2c10ib77() ;
   int   sumPseudoMv2c10ib70() ;
   int   nnb85jInsideJ() ;
   int   nnb77jInsideJ() ;
   int   nnb70jInsideJ() ;
   float sd12() const;
   float sd23() const;
   float tau21() const;
   float tau32() const;
   float tau21_wta() const;
   float tau32_wta() const;
   float D2() const;
   float C2() const;
   bool  topTag() const;
   bool  bosonTag() const;
   bool  topTag_loose() const;
   int   defTopTag() const;
   bool  is2ib70i                  () const;
   bool  istl1ib70eOutOfJ2ib70i        () const;
   bool  istl1ib70e1inb70iOutOfJ2ib70i () const;
   bool  istl1ib70eOrttOutOfJ2ib70i() const;
   bool  is2ib77i                  () const;
   bool  istl1ib77eOutOfJ2ib77i        () const;
   bool  istl1ib77e1inb77iOutOfJ2ib77i () const;
   bool  is1ib70em100              () const;
   bool  ism140OutOfJ1ib70em100    () const;
   bool  bosonTag_loose() const;
   int   truthmatch() const;
   Jet*  closestBJet() const; 
   Jet*  closestOutUJet() const; 
   Jet*  closestUJet() const; 
   Jet*  subleadPtUJet() const; 
   Jet*  leadPtUJet() const; 

   ~FatJet();
   
   private:
   TLorentzVector* m_v4; 
   Lepton* m_lepton;
   int   m_njOutsideJ;
   int   m_nb77jOutsideJ;
   int   m_nb70jOutsideJ;
   int   m_njInsideJ;
   int   m_nb85jInsideJ;
   int   m_nb77jInsideJ;
   int   m_nb70jInsideJ;
   vector<double>   m_mv2c10ib85;
   double   m_sumMv2c10ib85;
   double   m_sumMv2c10ib77;
   double   m_sumMv2c10ib70;
   int   m_sumPseudoMv2c10ib85;
   int   m_sumPseudoMv2c10ib77;
   int   m_sumPseudoMv2c10ib70;
   int   m_nnb85jInsideJ;
   int   m_nnb77jInsideJ;
   int   m_nnb70jInsideJ;
   float m_sd12;
   float m_sd23;
   float m_tau21;
   float m_tau32;
   float m_tau21_wta;
   float m_tau32_wta;
   float m_D2;
   float m_C2;
   bool  m_topTag;
   bool  m_bosonTag;
   bool  m_topTag_loose;
   int   m_defTopTag;   // 0 if not top loose, 2 if top tight, else 1
   bool  m_is2ib70i;
   bool  m_istl1ib70eOutOfJ2ib70i        ;
   bool  m_istl1ib70e1inb70iOutOfJ2ib70i ;
   bool  m_istl1ib70eOrttOutOfJ2ib70i;
   bool  m_is2ib77i;
   bool  m_istl1ib77eOutOfJ2ib77i        ;
   bool  m_istl1ib77e1inb77iOutOfJ2ib77i ;
   bool  m_is1ib70em100;
   bool  m_ism140OutOfJ1ib70em100;
   bool  m_bosonTag_loose;
   int   m_truthmatch;
   Jet*  m_closestBJet;
   Jet*  m_closestOutUJet;
   Jet*  m_closestUJet;
   Jet*  m_subleadPtUJet;
   Jet*  m_leadPtUJet;

};
#endif
