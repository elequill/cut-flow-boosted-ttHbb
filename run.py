#! /usr/bin/python

import sys
import os

from ROOT import *
gROOT.SetBatch(1)

path = "/afs/in2p3.fr/home/e/elequill/public/ttH/cut-flow-boosted-ttHbb"
os.chdir(path)

gROOT.ProcessLine(".L myMain.C+")

production               = sys.argv[1]  ## "phys-higgs.02042303.1l_sys"
sample                   = sys.argv[2]  ## "ttbartest"
treeName                 = sys.argv[3]  ## "nominal_Loose"
## dataset year
year2016                 = True
year2015                 = True
## lepton channels
electron                 = True
muon                     = True
## preselections
boosted                  = True
resolved                 = True
## saved root files
saveHistos               = False
saveBoostedCutflow       = False   ## only
saveResolvedCutflow      = False   ## for nominal_Loose
saveTreeForTMVA          = False   ## tree
saveTreeForFit           = True

myMain(production, sample, treeName, year2016, year2015, electron, muon, boosted, resolved, 
       saveHistos, saveBoostedCutflow, saveResolvedCutflow, saveTreeForTMVA, saveTreeForFit)
